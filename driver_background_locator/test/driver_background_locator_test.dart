import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:driver_background_locator/driver_background_locator.dart';

void main() {
  const MethodChannel channel = MethodChannel('driver_background_locator');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });
}
