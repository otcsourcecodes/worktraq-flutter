#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint driver_background_locator.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'driver_background_locator'
  s.version          = '0.0.1'
  s.summary          = 'A new flutter plugin project.'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'http://outthinkcoders.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'nkp.otc@gmail.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.ios.deployment_target = '8.0'

end
