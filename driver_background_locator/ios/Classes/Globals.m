//
//  Constants.m
//  background_locator
//
//  Created by Nand Kishor Patidar on 19/08/2020.
//

#import "Globals.h"

@implementation Globals

NSString *const kCallbackDispatcherKey = @"driver_callback_dispatcher_handle_key";
NSString *const kCallbackKey = @"driver_callback_handle_key";
NSString *const kInitCallbackKey = @"driver_init_callback_handle_key";
NSString *const kInitDataCallbackKey = @"driver_init_data_callback_key";
NSString *const kDisposeCallbackKey = @"driver_dispose_callback_handle_key";
NSString *const kDistanceFilterKey = @"driver_distance_filter_key";

NSString *const kChannelId = @"com.driverbackgroundlocator/driver_locator_plugin";
NSString *const kBackgroundChannelId = @"com.driverbackgroundlocator/driver_locator_plugin_background";

NSString *const kMethodServiceInitialized = @"DriverLocatorService.initialized";
NSString *const kMethodPluginInitializeService = @"DriverLocatorPlugin.initializeService";
NSString *const kMethodPluginRegisterLocationUpdate = @"DriverLocatorPlugin.registerLocationUpdate";
NSString *const kMethodPluginUnRegisterLocationUpdate = @"DriverLocatorPlugin.unRegisterLocationUpdate";
NSString *const kMethodPluginIsRegisteredLocationUpdate = @"DriverLocatorPlugin.isRegisterLocationUpdate";
NSString *const kMethodPluginIsServiceRunning = @"DriverLocatorPlugin.isServiceRunning";
NSString *const kMethodPluginUpdateNotification = @"DriverLocatorPlugin.updateNotification";

NSString *const kArgLatitude = @"latitude_driver";
NSString *const kArgLongitude = @"longitude_driver";
NSString *const kArgAccuracy = @"accuracy_driver";
NSString *const kArgAltitude = @"altitude_driver";
NSString *const kArgSpeed = @"speed_driver";
NSString *const kArgSpeedAccuracy = @"speed_accuracy_driver";
NSString *const kArgHeading = @"heading_driver";
NSString *const kArgTime = @"time_driver";
NSString *const kArgCallback = @"callback_driver";
NSString *const kArgInitCallback = @"initCallback_driver";
NSString *const kArgInitDataCallback = @"initDataCallback_driver";
NSString *const kArgDisposeCallback = @"disposeCallback_driver";
NSString *const kArgLocation = @"location_driver";
NSString *const kArgSettings = @"settings_driver";
NSString *const kArgCallbackDispatcher = @"callbackDispatcher_driver";

NSString *const kSettingsAccuracy = @"settings_accuracy_driver";
NSString *const kSettingsDistanceFilter = @"settings_distanceFilter_driver";

NSString *const kBCMSendLocation = @"DRIVER_BCM_SEND_LOCATION";
NSString *const kBCMInit = @"DRIVER_BCM_INIT";
NSString *const kBCMDispose = @"DRIVER_BCM_DISPOSE";

@end
