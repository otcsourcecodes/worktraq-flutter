//
//  PreferencesManager.h
//  driver_background_locator
//
//  Created by Nand Kishor Patidar on 19/08/2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PreferencesManager : NSObject

+ (int64_t)getCallbackDispatcherHandle;
+ (void)setCallbackDispatcherHandle:(int64_t)handle;
+ (int64_t)getCallbackHandle:(NSString *)key;
+ (void)setCallbackHandle:(int64_t)handle key:(NSString *)key;
+ (void)saveDistanceFilter:(double) distance;
+ (double)getDistanceFilter;

@end

NS_ASSUME_NONNULL_END
