//
//  MethodCallHelperDriver.h
//  driver_background_locator
//
//  Created by Nand Kishor Patidar on 19/08/2020.
//

#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

@protocol MethodCallHelperDriverDelegate <NSObject>
- (void) startLocatorService:(int64_t) callbackDispatcher;
- (void) setInitialized;
- (void)registerLocator:(int64_t)callback
           initCallback:(int64_t)initCallback
  initialDataDictionary:(NSDictionary *_Nullable)initialDataDictionary
        disposeCallback:(int64_t)disposeCallback
               settings:(NSDictionary *_Nonnull)settings;
- (void) removeLocator;
- (BOOL) isLocatorRegistered;
- (BOOL) isServiceRunning;
- (void) setServiceRunning:(BOOL)value;

@end

NS_ASSUME_NONNULL_BEGIN

@interface MethodCallHelperDriver : NSObject

- (void)handleMethodCall:(FlutterMethodCall *)call
                  result:(FlutterResult)result
                delegate:(id <MethodCallHelperDriverDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
