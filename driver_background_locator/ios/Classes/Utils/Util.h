//
//  Util.h
//  driver_background_locator
//
//  Created by Nand Kishor Patidar on 19/08/2020.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Util : NSObject

+ (CLLocationAccuracy) getAccuracy:(long)key;
+ (NSDictionary<NSString*,NSNumber*>*) getLocationMap:(CLLocation *)location;

@end

NS_ASSUME_NONNULL_END
