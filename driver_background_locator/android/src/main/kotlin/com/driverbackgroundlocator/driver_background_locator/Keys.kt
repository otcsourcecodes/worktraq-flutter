package com.driverbackgroundlocator.driver_background_locator

class Keys {
    companion object {
        @JvmStatic
        val SHARED_PREFERENCES_KEY = "DRIVER_SHARED_PREFERENCES_KEY"

        @JvmStatic
        val CALLBACK_DISPATCHER_HANDLE_KEY = "DRIVER_CALLBACK_DISPATCHER_HANDLE_KEY"
        @JvmStatic
        val CALLBACK_HANDLE_KEY = "DRIVER_CALLBACK_HANDLE_KEY"
        @JvmStatic
        val NOTIFICATION_CALLBACK_HANDLE_KEY = "DRIVER_NOTIFICATION_CALLBACK_HANDLE_KEY"
        @JvmStatic
        val INIT_CALLBACK_HANDLE_KEY = "DRIVER_INIT_CALLBACK_HANDLE_KEY"
        @JvmStatic
        val INIT_DATA_CALLBACK_KEY = "DRIVER_INIT_DATA_CALLBACK_KEY"
        @JvmStatic
        val DISPOSE_CALLBACK_HANDLE_KEY = "DRIVER_DISPOSE_CALLBACK_HANDLE_KEY"
        @JvmStatic
        val CHANNEL_ID = "com.driverbackgroundlocator/driver_locator_plugin"
        @JvmStatic
        val BACKGROUND_CHANNEL_ID = "com.driverbackgroundlocator/driver_locator_plugin_background"

        @JvmStatic
        val METHOD_SERVICE_INITIALIZED = "DriverLocatorService.initialized"
        @JvmStatic
        val METHOD_PLUGIN_INITIALIZE_SERVICE = "DriverLocatorPlugin.initializeService"
        @JvmStatic
        val METHOD_PLUGIN_REGISTER_LOCATION_UPDATE = "DriverLocatorPlugin.registerLocationUpdate"
        @JvmStatic
        val METHOD_PLUGIN_UN_REGISTER_LOCATION_UPDATE = "DriverLocatorPlugin.unRegisterLocationUpdate"
        @JvmStatic
        val METHOD_PLUGIN_IS_REGISTER_LOCATION_UPDATE = "DriverLocatorPlugin.isRegisterLocationUpdate"
        @JvmStatic
        val METHOD_PLUGIN_IS_SERVICE_RUNNING = "DriverLocatorPlugin.isServiceRunning"
        @JvmStatic
        val METHOD_PLUGIN_UPDATE_NOTIFICATION = "DriverLocatorPlugin.updateNotification"

        @JvmStatic
        val ARG_IS_MOCKED = "is_mocked_driver"
        @JvmStatic
        val ARG_LATITUDE = "latitude_driver"
        @JvmStatic
        val ARG_LONGITUDE = "longitude_driver"
        @JvmStatic
        val ARG_ACCURACY = "accuracy_driver"
        @JvmStatic
        val ARG_ALTITUDE = "altitude_driver"
        @JvmStatic
        val ARG_SPEED = "speed_driver"
        @JvmStatic
        val ARG_SPEED_ACCURACY = "speed_accuracy_driver"
        @JvmStatic
        val ARG_HEADING = "heading_driver"
        @JvmStatic
        val ARG_TIME = "time_driver"
        @JvmStatic
        val ARG_CALLBACK = "callback_driver"
        @JvmStatic
        val ARG_NOTIFICATION_CALLBACK = "notificationCallback_driver"
        @JvmStatic
        val ARG_INIT_CALLBACK = "initCallback_driver"
        @JvmStatic
        val ARG_INIT_DATA_CALLBACK = "initDataCallback_driver"
        @JvmStatic
        val ARG_DISPOSE_CALLBACK = "disposeCallback_driver"
        @JvmStatic
        val ARG_LOCATION = "location_driver"
        @JvmStatic
        val ARG_SETTINGS = "settings_driver"
        @JvmStatic
        val ARG_CALLBACK_DISPATCHER = "callbackDispatcher_driver"


        @JvmStatic
        val SETTINGS_ACCURACY = "settings_accuracy_driver"
        @JvmStatic
        val SETTINGS_INTERVAL = "settings_interval_driver"
        @JvmStatic
        val SETTINGS_DISTANCE_FILTER = "settings_distanceFilter_driver"
        @JvmStatic
        val SETTINGS_ANDROID_NOTIFICATION_CHANNEL_NAME = "settings_android_notificationChannelName_driver"
        @JvmStatic
        val SETTINGS_ANDROID_NOTIFICATION_TITLE = "settings_android_notificationTitle_driver"
        @JvmStatic
        val SETTINGS_ANDROID_NOTIFICATION_MSG = "settings_android_notificationMsg_driver"
        @JvmStatic
        val SETTINGS_ANDROID_NOTIFICATION_BIG_MSG = "settings_android_notificationBigMsg_driver"
        @JvmStatic
        val SETTINGS_ANDROID_NOTIFICATION_ICON = "settings_android_notificationIcon_driver"
        @JvmStatic
        val SETTINGS_ANDROID_NOTIFICATION_ICON_COLOR = "settings_android_notificationIconColor_driver"
        @JvmStatic
        val SETTINGS_ANDROID_WAKE_LOCK_TIME = "settings_android_wakeLockTime_driver"

        @JvmStatic
        val BCM_SEND_LOCATION = "DRIVER_BCM_SEND_LOCATION"
        @JvmStatic
        val BCM_NOTIFICATION_CLICK = "DRIVER_BCM_NOTIFICATION_CLICK"
        @JvmStatic
        val BCM_INIT = "DRIVER_BCM_INIT"
        @JvmStatic
        val BCM_DISPOSE = "DRIVER_BCM_DISPOSE"

        @JvmStatic
        val NOTIFICATION_ACTION = "com.driverbackgroundlocator.driver_background_locator.notification"
    }
}