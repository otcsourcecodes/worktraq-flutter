package com.driverbackgroundlocator.driver_background_locator

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import io.flutter.view.FlutterMain

class DriverLocatorBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        FlutterMain.ensureInitializationComplete(context, null)
        DriverLocatorService.enqueueWork(context, intent)
    }
}