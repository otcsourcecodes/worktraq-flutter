package com.driverbackgroundlocator.driver_background_locator

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class DriverBootBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == "android.intent.action.BOOT_COMPLETED") {
            DriverBackgroundLocatorPlugin.registerAfterBoot(context)
        }
    }
}