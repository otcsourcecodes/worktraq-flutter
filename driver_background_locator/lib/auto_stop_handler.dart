import 'package:driver_background_locator/driver_background_locator.dart';
import 'package:flutter/material.dart';

class AutoStopHandler extends WidgetsBindingObserver {
  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
        await DriverBackgroundLocator.unRegisterLocationUpdateDriver();
        break;
      case AppLifecycleState.resumed:
        break;
    }
  }
}
