class Keys {
  static const String CHANNEL_ID = 'com.driverbackgroundlocator/driver_locator_plugin';
  static const String BACKGROUND_CHANNEL_ID =
      'com.driverbackgroundlocator/driver_locator_plugin_background';

  static const String METHOD_SERVICE_INITIALIZED = 'DriverLocatorService.initialized';
  static const String METHOD_PLUGIN_INITIALIZE_SERVICE =
      'DriverLocatorPlugin.initializeService';
  static const String METHOD_PLUGIN_REGISTER_LOCATION_UPDATE =
      'DriverLocatorPlugin.registerLocationUpdate';
  static const String METHOD_PLUGIN_UN_REGISTER_LOCATION_UPDATE =
      'DriverLocatorPlugin.unRegisterLocationUpdate';
  static const String METHOD_PLUGIN_IS_REGISTER_LOCATION_UPDATE =
      'DriverLocatorPlugin.isRegisterLocationUpdate';
  static const String METHOD_PLUGIN_IS_SERVICE_RUNNING =
      'DriverLocatorPlugin.isServiceRunning';
  static const String METHOD_PLUGIN_UPDATE_NOTIFICATION =
      'DriverLocatorPlugin.updateNotification';

  static const String ARG_IS_MOCKED = 'is_mocked_driver';
  static const String ARG_LATITUDE = 'latitude_driver';
  static const String ARG_LONGITUDE = 'longitude_driver';
  static const String ARG_ALTITUDE = 'altitude_driver';
  static const String ARG_ACCURACY = 'accuracy_driver';
  static const String ARG_INTERVAL = 'interval_driver';
  static const String ARG_DISTANCE_FILTER = 'distanceFilter_driver';
  static const String ARG_NOTIFICATION_CHANNEL_NAME = 'notificationChannelName_driver';
  static const String ARG_NOTIFICATION_TITLE = 'notificationTitle_driver';
  static const String ARG_NOTIFICATION_MSG = 'notificationTitle_driver';
  static const String ARG_NOTIFICATION_ICON = 'notificationIcon_driver';
  static const String ARG_NOTIFICATION_ICON_COLOR = 'notificationIconColor_driver';
  static const String ARG_WAKE_LOCK_TIME = 'wakeLockTime_driver';
  static const String ARG_AUTO_STOP = 'autoStop_driver';
  static const String ARG_SPEED = 'speed_driver';
  static const String ARG_SPEED_ACCURACY = 'speed_accuracy_driver';
  static const String ARG_HEADING = 'heading_driver';
  static const String ARG_TIME = 'time_driver';
  static const String ARG_CALLBACK = 'callback_driver';
  static const String ARG_NOTIFICATION_CALLBACK = 'notificationCallback_driver';
  static const String ARG_INIT_CALLBACK = 'initCallback_driver';
  static const String ARG_INIT_DATA_CALLBACK = 'initDataCallback_driver';
  static const String ARG_DISPOSE_CALLBACK = 'disposeCallback_driver';
  static const String ARG_LOCATION = 'location_driver';
  static const String ARG_SETTINGS = 'settings_driver';
  static const String ARG_CALLBACK_DISPATCHER = 'callbackDispatcher_driver';

  static const String SETTINGS_ACCURACY = 'settings_accuracy_driver';
  static const String SETTINGS_INTERVAL = 'settings_interval_driver';
  static const String SETTINGS_DISTANCE_FILTER = 'settings_distanceFilter_driver';
  static const String SETTINGS_AUTO_STOP = 'settings_autoStop_driver';
  static const String SETTINGS_ANDROID_NOTIFICATION_CHANNEL_NAME =
      'settings_android_notificationChannelName_driver';
  static const String SETTINGS_ANDROID_NOTIFICATION_TITLE =
      'settings_android_notificationTitle_driver';
  static const String SETTINGS_ANDROID_NOTIFICATION_MSG =
      'settings_android_notificationMsg_driver';
  static const String SETTINGS_ANDROID_NOTIFICATION_BIG_MSG =
      'settings_android_notificationBigMsg_driver';
  static const String SETTINGS_ANDROID_NOTIFICATION_ICON =
      'settings_android_notificationIcon_driver';
  static const String SETTINGS_ANDROID_NOTIFICATION_ICON_COLOR =
      'settings_android_notificationIconColor_driver';
  static const String SETTINGS_ANDROID_WAKE_LOCK_TIME =
      'settings_android_wakeLockTime_driver';

  static const String BCM_SEND_LOCATION = 'DRIVER_BCM_SEND_LOCATION';
  static const String BCM_NOTIFICATION_CLICK = 'DRIVER_BCM_NOTIFICATION_CLICK';
  static const String BCM_INIT = 'DRIVER_BCM_INIT';
  static const String BCM_DISPOSE = 'DRIVER_BCM_DISPOSE';
}
