
import 'dart:async';
import 'dart:ui';

import 'package:driver_background_locator/auto_stop_handler.dart';
import 'package:driver_background_locator/callback_dispatcher.dart';
import 'package:driver_background_locator/keys.dart';
import 'package:driver_background_locator/location_dto.dart';
import 'package:driver_background_locator/location_settings.dart';
import 'package:driver_background_locator/settings/android_settings.dart';
import 'package:driver_background_locator/settings/ios_settings.dart';
import 'package:driver_background_locator/utils/settings_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DriverBackgroundLocator {
  static const MethodChannel _channel = const MethodChannel(Keys.CHANNEL_ID);

  static Future<void> initializeDriver() async {
    final CallbackHandle callback =
    PluginUtilities.getCallbackHandle(callbackDispatcher);
    await _channel.invokeMethod(Keys.METHOD_PLUGIN_INITIALIZE_SERVICE,
        {Keys.ARG_CALLBACK_DISPATCHER: callback.toRawHandle()});
  }

  static Future<void> registerLocationUpdateDriver(
      void Function(LocationDto) callback,
      {void Function(Map<String, dynamic>) initCallback,
        Map<String, dynamic> initDataCallback = const {},
        void Function() disposeCallback,
        bool autoStop,
        AndroidSettings androidSettings = const AndroidSettings(),
        IOSSettings iosSettings = const IOSSettings(),
        LocationSettings settings}) async {
    final _settings = settings ?? LocationSettings();
    if (_settings.autoStop) {
      WidgetsBinding.instance.addObserver(AutoStopHandler());
    }

    if (autoStop) {
      WidgetsBinding.instance.addObserver(AutoStopHandler());
    }

    final args = SettingsUtil.getArgumentsMap(
        callback: callback,
        initCallback: initCallback,
        initDataCallback: initDataCallback,
        disposeCallback: disposeCallback,
        androidSettings: androidSettings,
        iosSettings: iosSettings);

    await _channel.invokeMethod(Keys.METHOD_PLUGIN_REGISTER_LOCATION_UPDATE, args);
  }

  static Future<void> unRegisterLocationUpdateDriver() async {
    await _channel.invokeMethod(Keys.METHOD_PLUGIN_UN_REGISTER_LOCATION_UPDATE);
  }

  static Future<bool> isRegisterLocationUpdateDriver() async {
    return await _channel
        .invokeMethod<bool>(Keys.METHOD_PLUGIN_IS_REGISTER_LOCATION_UPDATE);
  }

  static Future<bool> isServiceRunningDriver() async {
    return await _channel
        .invokeMethod<bool>(Keys.METHOD_PLUGIN_IS_SERVICE_RUNNING);
  }

  static Future<void> updateNotificationTextDriver(
      {String title, String msg, String bigMsg}) async {
    final Map<String, dynamic> arg = {};

    if (title != null) {
      arg[Keys.SETTINGS_ANDROID_NOTIFICATION_TITLE] = title;
    }

    if (msg != null) {
      arg[Keys.SETTINGS_ANDROID_NOTIFICATION_MSG] = msg;
    }

    if (bigMsg != null) {
      arg[Keys.SETTINGS_ANDROID_NOTIFICATION_BIG_MSG] = bigMsg;
    }

    await _channel.invokeMethod(Keys.METHOD_PLUGIN_UPDATE_NOTIFICATION, arg);
  }
}
