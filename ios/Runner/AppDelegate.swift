
import UIKit
import Flutter
import background_locator
import driver_background_locator
import path_provider
import GoogleMaps

func registerPlugins(registry: FlutterPluginRegistry) -> () {
    if (!registry.hasPlugin("BackgroundLocatorPlugin")) {
        GeneratedPluginRegistrant.register(with: registry)
    }
}

func registerPluginsDriver(registry: FlutterPluginRegistry) -> () {
    if (!registry.hasPlugin("DriverBackgroundLocatorPlugin")) {
        GeneratedPluginRegistrant.register(with: registry)
    }
}

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]!
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyCXgbtCQh9Yf3cxCEO_VGsxKJMdoKEzcSg")
    GeneratedPluginRegistrant.register(with: self)
    BackgroundLocatorPlugin.setPluginRegistrantCallback(registerPlugins)
    DriverBackgroundLocatorPlugin.setPluginRegistrantCallback(registerPluginsDriver)

    registerOtherPlugins()

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }

  func registerOtherPlugins() {
      if !hasPlugin("io.flutter.plugins.pathprovider") {
        FLTPathProviderPlugin.register(with: registrar(forPlugin: "io.flutter.plugins.pathprovider")!)
      }
  }
}
