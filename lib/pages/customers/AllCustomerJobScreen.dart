import 'dart:io';

import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/pages/common/login/LoginScreen.dart';
import 'package:worktraq/pages/common/login/bloc/LoginBloc.dart';
import 'package:worktraq/pages/common/login/bloc/LoginState.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:path_provider/path_provider.dart';

import 'CompleteCustomerJobList.dart';
import 'OpenCustomerJobList.dart';

class AllCustomerJobScreen extends StatefulWidget {
  @override
  _AllCustomerJobScreenState createState() => _AllCustomerJobScreenState();
}

class _AllCustomerJobScreenState extends State<AllCustomerJobScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void logoutAction(String choice) async{
    print("logout");
    AppSharedPreferences.clearUserData();
    DatabaseHelper().clearAllData();

    Directory directory = await getExternalStorageDirectory();
    String path = directory.path;

    final dir = Directory('$path/${Constants.SIGNATURE_IMAGE_DIRECTORY}');
    if(dir.existsSync())
      dir.deleteSync(recursive: true);

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) => BlocProvider<LoginBloc>(
              create: (context) => LoginBloc(LoginState.empty()),
              child: LoginScreen()
          )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(AppTheme.primaryDarkColor);
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: Color(0xFFECECEC),
        body: ListView(
          children: [
            Container(
              color: AppTheme.primaryColor,
              child: Flex(
                direction: Axis.vertical,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right:  2 * SizeConfig.heightMultiplier, left:  2 * SizeConfig.heightMultiplier),
                        child: Text(
                          "All Jobs",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 2.5 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Container(
                        child: PopupMenuButton(
                            icon: Image.asset(
                              "${Constants.IMAGE_PATH}more.png",
                              height: 18,
                              width: 18,
                            ),
                            onSelected: logoutAction,
                            itemBuilder: (BuildContext context){
                              return Constants.choices.map((String choice) {
                                return PopupMenuItem<String>(
                                    value: choice,
                                    height: 15,
                                    textStyle: TextStyle(
                                        fontSize: 2.5 * SizeConfig.textMultiplier,
                                        color: Colors.black
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 82.0, top: 5.0, bottom: 5.0),
                                      child: Text(
                                          choice
                                      ),
                                    )
                                );
                              }).toList();
                            }
                        ),
                      )
                    ],
                  ),
                  PreferredSize(
                    preferredSize: null,
                    child: TabBar(
                      controller: _tabController,
                      labelStyle: TextStyle(
                          fontSize: 2 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.w600
                      ),
                      indicator: UnderlineTabIndicator(
                        borderSide: BorderSide(width: 3.0,
                            color: Colors.white
                        ),
                      ),
                      unselectedLabelColor: Color(0xFF788D9F),
                      tabs: [
                        Tab(text: "IN-PROGRESS"),
                        Tab(text: "COMPLETED JOBS"),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height:  SizeConfig.isPortrait ? ((100-17.5) * SizeConfig.heightMultiplier) : ((100-35) * SizeConfig.widthMultiplier),
              child: TabBarView(
                controller: _tabController,
                //physics: NeverScrollableScrollPhysics(),
                children: [
                  OpenCustomerJobList(),
                  CompleteCustomerJobList()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
