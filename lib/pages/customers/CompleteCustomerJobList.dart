import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/models/AfterWork.dart';
import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/models/BeforeWork.dart';
import 'package:worktraq/models/JobReport.dart';
import 'package:worktraq/models/JobResponse.dart';
import 'package:worktraq/models/Jobs.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/services/DriverTrackingHelper.dart';
import 'package:worktraq/services/JobTrackingHelper.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/CustomDialog.dart';
import 'package:worktraq/utils/DateTimeUtility.dart';
import 'package:worktraq/utils/ImageUtils.dart';
import 'package:worktraq/utils/ResponseHandler.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:worktraq/widgets/LoadingScreen.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

import 'CustomerJobDetailScreen.dart';


class CompleteCustomerJobList extends StatefulWidget {
  @override
  _CompleteCustomerJobListState createState() => _CompleteCustomerJobListState();
}

class _CompleteCustomerJobListState extends State<CompleteCustomerJobList> {

  List<Jobs> completedJobList;
  List<Jobs> newCompletedJobList = List();
  final GlobalKey _globalKey = GlobalKey<State>();
  DatabaseHelper databaseHelper = DatabaseHelper();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void didChangeDependencies() async{

    getCompletedJobsService();

    super.didChangeDependencies();
  }

  Future<void> getCompletedJobsService() async {
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet) {
      bool isFirstTimeLaunchComplete = await AppSharedPreferences.isFirstTimeDataSaveInLocalComplete();

      if(isFirstTimeLaunchComplete == null)
        isFirstTimeLaunchComplete = false;

      if(isFirstTimeLaunchComplete){
        Future.delayed(Duration.zero, () {
          Navigator.of(context).push(
              PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
              )
          );
        });
      } else{
        Future.delayed(Duration.zero, () {
          Navigator.of(context).push(
              PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey, title: "Please wait...")
              )
          );
        });
      }

      String authToken = await AppSharedPreferences.getAuthToken();
      print("authToken $authToken");

      NetworkConfig.restClientInstance().getCompleteJob(authToken).then((result){
        if(result.status == "success"){
          if(isFirstTimeLaunchComplete)
            Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();

          JobResponse jobResponse = result;
          if (!mounted) return;
          setState(() {
            completedJobList = jobResponse.jobs;
          });

          //print("${completedJobList.toString()}");

          if(!isFirstTimeLaunchComplete){
            Future.delayed(Duration(milliseconds: completedJobList.length * 7000), (){
              Navigator.of(_globalKey.currentContext, rootNavigator: true).pop();
            });
          }

          if(completedJobList != null && completedJobList.length > 0){
            newCompletedJobList.clear();
            for ( int i = 0; i < completedJobList.length; i++ ){
              Jobs job = completedJobList[i];

              job.jobTypeQuestionsString = job.jobTypeQuestions.toString();
              saveLocalDb(job);
              int finalI = i;

              Future.delayed(Duration(milliseconds: 2000), (){
                databaseHelper.isLocalImageUpdated(job.jobId).then((isLocalImageUpdated){
                  //print("isLocalImageUpdated : $isLocalImageUpdated");
                  if(isLocalImageUpdated){
                    JobReport jobReport = job.jobReport;
                    if (jobReport != null){
                      BeforeWork beforeWork = jobReport.beforeWork;
                      if(beforeWork != null){
                        if(beforeWork.driverSignature != null){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_DRIVER_SIGNATURE, beforeWork.driverSignature);
                        }
                        List<String> workImages = beforeWork.workImage;
                        if(workImages != null && workImages.length > 0){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_IMAGE1, workImages[0]);
                        }

                        if(workImages != null && workImages.length > 1){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_IMAGE2, workImages[1]);
                        }

                        if(workImages != null && workImages.length > 2){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_IMAGE3, workImages[2]);
                        }
                      }

                      AfterWork afterWork = jobReport.afterWork;
                      if(afterWork != null && afterWork != null){
                        if(afterWork.customerSignature != null){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_CUSTOMER_SIGNATURE, afterWork.customerSignature);
                        }
                        List<String> workImages = afterWork.workImage;
                        if(workImages != null && workImages.length > 0){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_IMAGE1, workImages[0]);
                        }

                        if(workImages != null && workImages.length > 1){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_IMAGE2, workImages[1]);
                        }

                        if(workImages != null && workImages.length > 2){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_IMAGE3, workImages[2]);
                        }
                      }
                    }

                    if( finalI == completedJobList.length-1 )
                      AppSharedPreferences.setFirstTimeDataSaveInLocalComplete(true);

                    if (job.geoFencingUrl!= null) {
                      _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_GEO_FENCING_URL, job.geoFencingUrl);
                    }
                  }
                });
              });

              if (job.jobStatus == "0" || job.jobStatus == "1") {
                newCompletedJobList.add(job);
              }
            }
          }
        }else{
          Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
          showErrorDialog(context,  'Error', result.message);
        }
      }, onError: (error){
        print(error);
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
      }).catchError((Object obj){
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        switch (obj.runtimeType) {
          case DioError:
            final res = (obj as DioError).response;
            // Here's the sample to get the failed response error code and message
            //print("${res.data.toString()} ");
            if(res.data != null) {
              AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res.data);
              print("${res.data.toString()} ");
              if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                ResponseHandler.handleResponse(context, authFailedResponse);
              }
            } else {
              NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
              showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
            }
            break;
          default:
            NetworkConfig.logger.e("Got error : ${obj.toString()}");
            break;
        }
      });
    } else {
      getCompletedJobListFromLocalDb();
    }
  }

  void saveLocalDb(Jobs job) async{
    DatabaseHelper databaseHelper = DatabaseHelper();
    if (await databaseHelper.isJobExist(job.jobId)) {
      if(await databaseHelper.isUpdateRequired(job.jobId, job.jobStatus))
        await databaseHelper.updateJob(job);
      else
        return;
    } else
      await databaseHelper.addJob(job);
  }

  void _downloadFileAndSavePathDb(String jobId, String jobStatus, String whereImageName, String imageUrl) async {
    //print("imageUrl : $imageUrl");
    downloadImage(imageUrl).then((localImagePath){
      //print("_downloadFileAndSavePathDb : $localImagePath");
      if(localImagePath != null){
        databaseHelper.isUpdateRequiredFilePath(jobId, jobStatus, whereImageName, localImagePath).then((isUpdateRequiredFilePath){
          if(!isUpdateRequiredFilePath){
            databaseHelper.updateImagePath(jobId, whereImageName, localImagePath);
          }
        });
      }
    });
  }

  void getCompletedJobListFromLocalDb() {
    JobTrackingHelper().onStop();
    DriverTrackingHelper().onStop();
    Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );
    });

    Future.delayed(Duration(milliseconds: 2000), () async{
      if(completedJobList != null)
        completedJobList.clear();

      DatabaseHelper databaseHelper = DatabaseHelper();
      String userId = await AppSharedPreferences.getId();
      print("user id : $userId");
      databaseHelper.getCompleteJobsListFromLocal(userId).then((jobList) {
        setState(() {
          completedJobList = jobList;
          //print("local completedJobList : ${completedJobList.toString()}");
        });
      });

      Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        key: _scaffoldKey,
        body: _completeProjectList()
    );
  }

  Widget _completeProjectList(){
    return (completedJobList != null && completedJobList.length > 0) ? ListView.builder(
      itemCount: completedJobList != null ? completedJobList.length : 0,
      padding: EdgeInsets.symmetric(vertical: 1 * SizeConfig.heightMultiplier, horizontal: 10.0),
      itemBuilder: (context, index) {
        return _listCard(completedJobList[index]);
      },
    ) : Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: const EdgeInsets.only(top: 15.0),
        child: Text(
          "No Record",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 22.0,
              color: AppTheme.colorText,
              fontWeight: FontWeight.w200
          ),
        ),
      ),
    );
  }

  Widget _listCard(Jobs job){
    JobReport jobReport = job?.jobReport;
    BeforeWork beforeWork = jobReport?.beforeWork;
    AfterWork afterWork = jobReport?.afterWork;
    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CustomerJobDetailScreen(id: job.jobId)
          ),
        );
      },
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        color: Colors.white,
        elevation: 5,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.transparent, width: 0),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          constraints: BoxConstraints(maxHeight: 12.7 * SizeConfig.heightMultiplier),
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(
                color: AppTheme.colorCompleteJobStatus,
                width: 3.0,
              ),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      (job.jobName != null) ? job.jobName : "NA",
                      style: TextStyle(
                        fontSize: 2.4 * SizeConfig.textMultiplier,
                      ),
                      maxLines: 1,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 7.0),
                    Text(
                      (job.customerName != null) ? job.customerName : "NA",
                      style: TextStyle(
                          color: AppTheme.colorTextGrey,
                          fontSize: 1.7 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 4.5),
                    Text(
                      (job.jobType != null) ? job.jobType : "NA",
                      style: TextStyle(
                          color: AppTheme.colorTextGrey,
                          fontSize: 1.7 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(height: 3.0),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "${Constants.IMAGE_PATH}calendar.png",
                        height: 12.0,
                        width: 12.0,
                      ),
                      SizedBox(width: 5.0),
                      Text(
                        "${(beforeWork?.startDateTime != null) ? DataTimeUtility.convertTimeForCompleteJob(beforeWork?.startDateTime) : "NA"}",
                        style: TextStyle(
                          fontSize: 1.6 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal,
                          color: AppTheme.colorTextGrey,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 6.0),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "${Constants.IMAGE_PATH}calendar.png",
                        height: 12.0,
                        width: 12.0,
                      ),
                      SizedBox(width: 5.0),
                      Text(
                        "${(afterWork?.endDateTime != null) ? DataTimeUtility.convertTimeForCompleteJob(afterWork?.endDateTime) : "NA"}",
                        style: TextStyle(
                          fontSize: 1.6 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal,
                          color: AppTheme.colorTextGrey,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    job.statusShow,
                    style: TextStyle(
                        fontSize: 1.9 * SizeConfig.textMultiplier,
                        color: AppTheme.colorCompleteJobStatus,
                        fontWeight: FontWeight.normal
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

