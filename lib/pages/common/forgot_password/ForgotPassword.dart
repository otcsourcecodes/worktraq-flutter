
import 'package:flutter/material.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/CustomDialog.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:worktraq/utils/Validators.dart';
import 'package:worktraq/widgets/LoadingScreen.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final TextEditingController _emailController = TextEditingController();
  final GlobalKey _globalKey = GlobalKey<State>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  void _onFormSubmitted() async{
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet){
      forgotPasswordService();
    } else {
     // showErrorDialog(context, "Error", "No internet connection!");
      final snackBar = SnackBar(
        content: Text("No internet connection!"),
        action: SnackBarAction(
          label: 'RETRY',
          textColor: Colors.red,
          onPressed: () {
            _onFormSubmitted();
          },
        ),
      );

      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState..showSnackBar(snackBar);
    }
  }

  void forgotPasswordService(){
    String email = _emailController.text;
    if( email.length == 0 ){
      showErrorDialog(context, 'Error', 'Please enter email address.');
    } else if(!Validators.isValidEmail(_emailController.text)) {
      showErrorDialog(context, 'Error', 'Please enter valid email address.');
    } else{
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );

      NetworkConfig.restClientInstance().forgotPassword(email).then((result){
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        if(result.status == "success"){
          String message = result.message;
          //print("${message}");
          showErrorDialog(context,  'Success', message);
        }else{
          showErrorDialog(context,  'Error', result.message);
        }
      }).catchError((onError){
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        NetworkConfig.logger.e(onError.toString());
        showErrorDialog(context, "Server Error", onError.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          key: _scaffoldKey,
          body: SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  constraints: BoxConstraints(maxHeight: (SizeConfig.screenWidth > 450) ? 98 * SizeConfig.heightMultiplier : 96.15 * SizeConfig.heightMultiplier),
                  color: AppTheme.backgroundAppColor,
                ),
                Padding(
                  padding: EdgeInsets.all(2.2*SizeConfig.heightMultiplier),
                  child: InkWell(
                    onTap:() {
                      Navigator.of(context).pop();
                    },
                    child: Image.asset(
                      "${Constants.IMAGE_PATH}back.png",
                      height: 4.8*SizeConfig.imageSizeMultiplier,
                      width: 4.8*SizeConfig.imageSizeMultiplier,
                      color: Colors.black,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    margin: EdgeInsets.only(top: 12 * SizeConfig.heightMultiplier, left: 7.7*SizeConfig.widthMultiplier, right: 7.7*SizeConfig.widthMultiplier),
                    child: Image.asset(
                      "${Constants.IMAGE_PATH}worktraq_logo_stacked2.png",
                      //height: 14 * SizeConfig.heightMultiplier,
                      width: 75 * SizeConfig.widthMultiplier*0.8,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 48 * SizeConfig.heightMultiplier),
                    Text(
                      "Forgot your password?",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 2.5 * SizeConfig.textMultiplier
                      ),
                    ),
                    SizedBox(height: 5 * SizeConfig.heightMultiplier),
                    Container(
                      margin: EdgeInsets.only(top: 3.6*SizeConfig.heightMultiplier, right: 4.8*SizeConfig.widthMultiplier, left: 4.8*SizeConfig.widthMultiplier),
                      child: Form(
                          child: Column(
                            children: <Widget>[
                              Material(
                                elevation: 5,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(4.3*SizeConfig.heightMultiplier),
                                ),
                                child: TextFormField(
                                  controller: _emailController,
                                  decoration: new InputDecoration(
                                      border: new OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(4.3*SizeConfig.heightMultiplier),
                                        ),
                                        borderSide: BorderSide(
                                          width: 0,
                                          style: BorderStyle.none,
                                        ),
                                      ),
                                      isDense: true,
                                      contentPadding: EdgeInsets.symmetric(vertical: ((100*SizeConfig.widthMultiplier) > 450) ? 20.0 : 0.0),
                                      filled: true,
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.only(left: 1.2*SizeConfig.widthMultiplier, right: 1.2*SizeConfig.widthMultiplier),
                                        child: Image.asset(
                                          "${Constants.IMAGE_PATH}email.png",
                                          scale: 3.5,
                                          //alignment: Alignment.centerLeft,
                                        ),
                                      ),
                                      hintStyle: new TextStyle(color: Color(0xFF777777)),
                                      hintText: "Email address",
                                      fillColor: Color(0xFFFFFFFF)
                                  ),
                                  autocorrect: false,
                                ),
                              ),
                              SizedBox(height: 5 * SizeConfig.heightMultiplier),
                              Material(
                                elevation: 10,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(4.3*SizeConfig.heightMultiplier),
                                ),
                                child: Container(
                                  height: 6.5 * SizeConfig.heightMultiplier,
                                  child: GestureDetector(
                                    onTap: _onFormSubmitted,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Color(0xFF2D6087), //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                                          style: BorderStyle.solid,
                                          width: 1.0,
                                        ),
                                        color: Color(0xFF2D6087), //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                                        borderRadius: BorderRadius.circular(4.3*SizeConfig.heightMultiplier),
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Center(
                                            child: Text(
                                              "Send",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 2 * SizeConfig.textMultiplier,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 1,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                    )
                  ],
                )
              ],
            ),
          )
      ),
    );
  }
}
