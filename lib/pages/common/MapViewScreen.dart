import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:worktraq/models/Jobs.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapViewScreen extends StatefulWidget {
  final Jobs job;
  final bool isLocalData;

  const MapViewScreen({Key key, this.job, this.isLocalData}) : super(key: key);

  @override
  _MapViewScreenState createState() => _MapViewScreenState();
}

class _MapViewScreenState extends State<MapViewScreen> {
  GoogleMapController controller;
  Polygon polygon;
  PolygonId polygonId = PolygonId("polygon_id_1");
  Map<PolygonId, Polygon> polygons = <PolygonId, Polygon>{};
  Marker marker;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId markerId = MarkerId("marker_id_1");

  void _onMapCreated(GoogleMapController controller) {
    this.controller = controller;
  }

  double currentLatitude = 0.0;
  double currentLongitude = 0.0;

  List<LatLng> _polygonPointList = List();
  String polygonColor;
  bool isOnline = false;
  String geoFencingUrl;

  @override
  initState() {
    super.initState();

    if(widget.job != null) {
      /*print("Jobs nkp : ${widget.job.toJson()}");
      print("latitude: ${widget.job.latitude} longitude : ${widget.job.longitude}");*/
      //initData();
      setState(() {
          currentLatitude = double.parse(widget.job.latitude);
          currentLongitude = double.parse(widget.job.longitude);

          String points = widget.job.points;
          points = points.substring(0, points.length - 1);
          var pointArray = points.split("|");

          if (widget.job.polygonColor != null) {
            polygonColor = (widget.job.polygonColor).substring(1, (widget.job.polygonColor).length);
            polygonColor = "0x75$polygonColor";
          }

          //print(pointArray);
          for (int i = 0; i < pointArray.length; i++) {
            var point = pointArray[i].split(",");
            LatLng location = LatLng(double.parse(point[0]), double.parse(point[1]));
            _polygonPointList.add(location);
            //print("i = ${pointArray[i]}");
          }
      });
    }
    //getCurrentLocation();
    //showMap();
  }

  @override
  void didChangeDependencies() async {
    bool isInternet = await ConnectivityUtility().isInternet();
    setState(() {
      if (isInternet) {
        isOnline = true;
      } else {
        isOnline = false;
        geoFencingUrl = widget.job.geoFencingUrl;
      }
    });

    super.didChangeDependencies();
  }

  void initData() async{
    bool isInternet = await ConnectivityUtility().isInternet();
    setState(() {
      if (isInternet) {

        isOnline = true;
        currentLatitude = double.parse(widget.job.latitude);
        currentLongitude = double.parse(widget.job.longitude);

        String points = widget.job.points;
        points = points.substring(0, points.length - 1);
        var pointArray = points.split("|");

        if (widget.job.polygonColor != null) {
          polygonColor = (widget.job.polygonColor).substring(1, (widget.job.polygonColor).length);
          polygonColor = "0x75$polygonColor";
        }

        print(pointArray);
        for (int i = 0; i < pointArray.length; i++) {
          var point = pointArray[i].split(",");
          LatLng location = LatLng(double.parse(point[0]), double.parse(point[1]));
          _polygonPointList.add(location);
          print("i = ${pointArray[i]}");
        }
      } else {
        isOnline = false;
        geoFencingUrl = widget.job.geoFencingUrl;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

/*
  void getCurrentLocation(){
    location.requestPermission().then((granted){
      if(granted != null){
        location.onLocationChanged.listen((locationData) {
          if(locationData != null){
            currentLatitude = locationData.latitude;
            currentLongitude = locationData.longitude;

            print(" nkp longitude : $currentLongitude latitude : $currentLatitude");
          }
        });
      }
    });
  }*/

  Widget geoFencingImage(){
    if( widget.job != null && geoFencingUrl != null && geoFencingUrl.length > 0){
      return Image.file(
        File(geoFencingUrl),
        fit: BoxFit.fill,
        errorBuilder: (context, error, stacktrace){
          return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
        });
    } else{
      return  Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
    }
  }

  @override
  Widget build(BuildContext context) {
    polygon = Polygon(
      polygonId: polygonId,
      consumeTapEvents: true,
      strokeColor: (widget.job.polygonColor != null) ? Color(int.parse(polygonColor)) : AppTheme.primaryColor,
      strokeWidth: 3,
      fillColor: (widget.job.polygonColor != null) ? Color(int.parse(polygonColor)) : AppTheme.primaryColor,
      points: _polygonPointList,
      onTap: () {},
    );

    marker = Marker(
      markerId: markerId,
      position: LatLng(currentLatitude, currentLongitude),
      infoWindow: InfoWindow(
        title: widget.job.address
      )
    );

    setState(() {
      polygons[polygonId] = polygon;
      markers[markerId] = marker;
    });

    return SafeArea(
      top: true,
      child: Scaffold(
        body: Column(
          children: [
            Container(
              color: AppTheme.primaryColor,
              child: Flex(
                direction: Axis.vertical,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    padding: EdgeInsets.only(right:  2 * SizeConfig.heightMultiplier, left:  2 * SizeConfig.heightMultiplier),
                    height: 56,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap:() {
                            Navigator.of(context).pop();
                          },
                          child: Image.asset(
                            "${Constants.IMAGE_PATH}back.png",
                            height: 18,
                            width: 18,
                          ),
                        ),
                        SizedBox(
                          width: 8 * SizeConfig.widthMultiplier,
                        ),
                        Text(
                          "Map View",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 2.5 * SizeConfig.textMultiplier
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: (100 * SizeConfig.heightMultiplier) - 84,
              child: (isOnline) ? GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: LatLng(currentLatitude, currentLongitude),
                  zoom: 16.0,
                ),
                mapType: MapType.satellite,
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                polygons: Set<Polygon>.of(polygons.values),
                markers: Set<Marker>.of(markers.values),
                onMapCreated: _onMapCreated,
              ) : geoFencingImage(),
            ),
          ],
        ),
      ),
    );/*SafeArea(
      top: true,
      child: (isOnline) ? GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(currentLatitude, currentLongitude),
          zoom: 16.0,
        ),
        mapType: MapType.satellite,
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        polygons: Set<Polygon>.of(polygons.values),
        markers: Set<Marker>.of(markers.values),
        onMapCreated: _onMapCreated,
      ) : geoFencingImage(),
    );*/
  }
}