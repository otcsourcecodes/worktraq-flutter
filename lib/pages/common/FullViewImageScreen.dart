import 'dart:io';

import 'package:worktraq/utils/Constants.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FullViewImageScreen extends StatefulWidget {
  final fullImageUrl;
  final bool isLocalData;

  const FullViewImageScreen({Key key, this.fullImageUrl, this.isLocalData}) : super(key: key);

  @override
  _FullViewImageScreenState createState() => _FullViewImageScreenState();
}

class _FullViewImageScreenState extends State<FullViewImageScreen> {
  @override
  Widget build(BuildContext context) {
    return  Stack(
      children: [
        (widget.isLocalData) ? PhotoView(
          imageProvider: (widget.fullImageUrl != null) ? FileImage(
              File(widget.fullImageUrl)
          ) : AssetImage(
              "${Constants.IMAGE_PATH}camera_icon.png"
          ),
        ) : PhotoView(
          imageProvider: (widget.fullImageUrl != null) ? NetworkImage(
              widget.fullImageUrl
          ) : AssetImage(
              "${Constants.IMAGE_PATH}camera_icon.png"
          ),
        ),
        Positioned.fill(
          top: 40,
          right: 10,
          child: Align(
            alignment: Alignment.topRight,
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: Icon(
                Icons.close,
                size: 30,
                color: Colors.white,
              ),
            ),
          ),
        )
      ],
    );
  }
}
