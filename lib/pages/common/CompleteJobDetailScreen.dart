
import 'dart:io';
import 'dart:ui';

import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/models/AfterWork.dart';
import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/models/BeforeWork.dart';
import 'package:worktraq/models/JobDetailResponse.dart';
import 'package:worktraq/models/JobTypeQuestions.dart';
import 'package:worktraq/models/Jobs.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/pages/common/MapViewScreen.dart';
import 'package:worktraq/services/DriverTrackingHelper.dart';
import 'package:worktraq/services/JobTrackingHelper.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/CustomDialog.dart';
import 'package:worktraq/utils/DateTimeUtility.dart';
import 'package:worktraq/utils/ResponseHandler.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:worktraq/widgets/LoadingScreen.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart' hide Response;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'dart:convert';

import 'package:toast/toast.dart';

import 'FullViewImageScreen.dart';

class CompleteJobDetailScreen extends StatefulWidget {
  final String id;

  const CompleteJobDetailScreen({Key key, this.id}) : super(key: key);

  @override
  _CompleteJobDetailScreenState createState() => _CompleteJobDetailScreenState();
}

class _CompleteJobDetailScreenState extends State<CompleteJobDetailScreen> with WidgetsBindingObserver {
  Jobs job;
  BeforeWork beforeWork;
  AfterWork afterWork;
  List<String> workImages, workImageAfter;
  final GlobalKey _globalKey = GlobalKey<State>();
  List<bool> _checkList;
  String _driverComment;
  String _customerComment;
  // When data comes from local database.
  bool isLocalData = true;

  @override
  void initState() {
    super.initState();
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  @override
  void didChangeDependencies() async{
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet) {
      getJobDetailByIdService();
    } else{
      getJobDetailFromLocalDb(widget.id);
    }

    super.didChangeDependencies();
  }

  Future<void> getJobDetailByIdService() async {
    Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );
    });

    String authToken = await AppSharedPreferences.getAuthToken();
    print("authToken $authToken");
    print("id ${widget.id}");

    NetworkConfig.restClientInstance().getJobDetailById(authToken, widget.id).then((result){
      if(result.status == "success"){
        JobDetailResponse jobDetailResponse = result;
        setState(() {
          job = jobDetailResponse.jobs;
          beforeWork = job.jobReport.beforeWork;
          afterWork = job.jobReport.afterWork;
          workImages = beforeWork?.workImage;
          workImageAfter = afterWork?.workImage;
          
          isLocalData = false;

          if(beforeWork?.comments != null) {
            _driverComment = beforeWork?.comments;
          }

          if(afterWork?.comments != null){
            _customerComment = afterWork?.comments;
          }

          String questionAnswer = "";
          if(job != null && job.jobTypeQuestions != null && job.jobTypeQuestions.length > 0){
            for(int i = 0; i < job.jobTypeQuestions.length; i++){
              JobTypeQuestions jobTypeQuestions = job.jobTypeQuestions[i];

              if(jobTypeQuestions != null && jobTypeQuestions.type == "checkbox"){
                var optionsJson = jsonDecode(jobTypeQuestions.options);
                List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;
                _checkList = List(optionsArray.length);
                  var options = jobTypeQuestions.answer.split(",");
                  for(int i = 0; i < optionsArray.length; i++){
                    _checkList[i] = false;
                    if(jobTypeQuestions.answer.length > 0) {
                      for (int j = 0; j < options.length; j++) {
                        if (optionsArray[i] == options[j]) {
                          _checkList[i] = true;
                        }
                      }
                    }
                  }
                //print(optionsArray);
              }
            }
          }
        });

        //print("${jobDetailResponse.toString()}");
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
      }else{
        showErrorDialog(context,  'Error', result.message);
      }
      //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
    }).catchError((Object obj){
      Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          // Here's the sample to get the failed response error code and message
          //print("${res.data.toString()} ");
          if(res?.data != null) {
            AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res.data);
            print("${res?.data.toString()} ");
            if (ResponseHandler.checkStatusCode(authFailedResponse)) {
              ResponseHandler.handleResponse(context, authFailedResponse);
            }
          } else {
            if(res != null){
              NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
              showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
            }
          }
          break;
        default:
          NetworkConfig.logger.e("Got error : ${obj.toString()}");
          break;
      }
    });
  }

  void getJobDetailFromLocalDb(String jobId) async{
    DateTime now = DateTime.now();
    String nowDate = DateFormat('dd/MM/yyyy').format(now);
    print(nowDate);

    Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );
    });

    Future.delayed(Duration(seconds: 2), () {
      JobTrackingHelper().onStop();
      DriverTrackingHelper().onStop();

      DatabaseHelper databaseHelper = DatabaseHelper();
      databaseHelper.getJobDetailByJobIdFromLocal(widget.id).then((jobs){
        setState(() {
          job = jobs;

          isLocalData = true;

          beforeWork = job.jobReport.beforeWork;
          afterWork = job.jobReport.afterWork;
          workImages = beforeWork?.workImage;
          workImageAfter = afterWork?.workImage;

          if(beforeWork?.comments != null) {
            _driverComment = beforeWork?.comments;
          }

          if(afterWork?.comments != null){
            _customerComment = afterWork?.comments;
          }

          String questionAnswer = "";
          if(job != null && job.jobTypeQuestions != null && job.jobTypeQuestions.length > 0){
            for(int i = 0; i < job.jobTypeQuestions.length; i++){
              JobTypeQuestions jobTypeQuestions = job.jobTypeQuestions[i];

              if(jobTypeQuestions != null && jobTypeQuestions.type == "checkbox"){
                var optionsJson = jsonDecode(jobTypeQuestions.options);
                List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;
                _checkList = List(optionsArray.length);
                var options = jobTypeQuestions.answer.split(",");
                for(int i = 0; i < optionsArray.length; i++){
                  _checkList[i] = false;
                  if(jobTypeQuestions.answer.length > 0) {
                    for (int j = 0; j < options.length; j++) {
                      if (optionsArray[i] == options[j]) {
                        _checkList[i] = true;
                      }
                    }
                  }
                }
                //print(optionsArray);
              }
            }
          }
        });
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
      });
    });

  }

  Widget beforeImage(int position){
    //print(finalImagePathBefore1);
    if( isLocalData && beforeWork != null && (workImages.length > (position) )){
      return InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => FullViewImageScreen(fullImageUrl: workImages[position], isLocalData: isLocalData)
          ));
        },
        child: Image.file(
          File(workImages[position]),
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => FullViewImageScreen(fullImageUrl: null)
                  ));
                },
                child: InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => FullViewImageScreen(fullImageUrl: null)
                    ));
                  },
                  child: Image.asset(
                      "${Constants.IMAGE_PATH}camera_icon.png",
                      fit: BoxFit.fill
                  ),
                )
            );
          }),
      );
    } else if( beforeWork != null && (workImages.length > (position) )){
      return InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => FullViewImageScreen(fullImageUrl: workImages[position], isLocalData: isLocalData)
          ));
        },
        child: Image.network(
            workImages[position],
            fit: BoxFit.fill,
            errorBuilder: (context, error, stacktrace){
              return InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => FullViewImageScreen(fullImageUrl: null)
                    ));
                  },
                  child: InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => FullViewImageScreen(fullImageUrl: null)
                      ));
                    },
                    child: Image.asset(
                        "${Constants.IMAGE_PATH}camera_icon.png",
                        fit: BoxFit.fill
                    ),
                  )
              );
            }),
      );
    } else{
      return  Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
    }
  }

  Widget afterImage(int position){
    //print(finalImagePathBefore1);
    if(isLocalData && afterWork != null && workImageAfter != null && workImageAfter.length > (position) ){
      return InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => FullViewImageScreen(fullImageUrl: workImageAfter[position], isLocalData: isLocalData)
          ));
        },
        child: Image.file(
        File(workImageAfter[position]),
        fit: BoxFit.fill,
        errorBuilder: (context, error, stacktrace){
          return InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => FullViewImageScreen(fullImageUrl: null)
              ));
            },
            child: Image.asset(
              "${Constants.IMAGE_PATH}camera_icon.png",
              fit: BoxFit.fill
            )
          );
        }),
      );
    } else if(afterWork != null && workImageAfter != null && workImageAfter.length > (position) ){
      return InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => FullViewImageScreen(fullImageUrl: workImageAfter[position], isLocalData: isLocalData)
          ));
        },
        child: Image.network(
            workImageAfter[position],
            fit: BoxFit.fill,
            errorBuilder: (context, error, stacktrace){
              return InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => FullViewImageScreen(fullImageUrl: null)
                    ));
                  },
                  child: Image.asset(
                      "${Constants.IMAGE_PATH}camera_icon.png",
                      fit: BoxFit.fill
                  )
              );
            }),
      );
    } else{
      return InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => FullViewImageScreen(fullImageUrl: null)
          ));
        },
        child: Image.asset(
          "${Constants.IMAGE_PATH}camera_icon.png",
          fit: BoxFit.fill
        )
      );
    }
  }

  Widget beforeSignatureImage(){
    if(isLocalData && beforeWork != null && beforeWork?.driverSignature != null && (beforeWork?.driverSignature.length > 0 )){
      return Image.file(
        File(beforeWork.driverSignature),
        fit: BoxFit.fill,
        errorBuilder: (context, error, stacktrace){
          return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
        });
    } else if(beforeWork != null && beforeWork?.driverSignature != null && (beforeWork?.driverSignature.length > 0 )){
      return Image.network(
          beforeWork.driverSignature,
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
          });
    } else{
      return  Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);;
    }
  }

  Widget afterSignatureImage(){
    if( isLocalData && afterWork != null && afterWork?.customerSignature != null && (afterWork?.customerSignature?.length > 0 )){
      return Image.file(
        File(afterWork.customerSignature),
        fit: BoxFit.fill,
        errorBuilder: (context, error, stacktrace){
          return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
        }
      );
    } else if(afterWork != null && afterWork?.customerSignature != null && (afterWork?.customerSignature?.length > 0 )){
      return Image.network(
          afterWork.customerSignature,
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
          }
      );
    } else{
      return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: Color(0xFFECECEC),
        body: Container(
          constraints: BoxConstraints(
              minHeight: 100 * SizeConfig.heightMultiplier
          ),
          child: Column(
            children: [
              Container(
                color: AppTheme.primaryColor,
                child: Flex(
                  direction: Axis.vertical,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      padding: EdgeInsets.only(right:  2 * SizeConfig.heightMultiplier, left:  2 * SizeConfig.heightMultiplier),
                      height: 56,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap:() {
                              Navigator.of(context).pop();
                            },
                            child: Image.asset(
                              "${Constants.IMAGE_PATH}back.png",
                              height: 18,
                              width: 18,
                            ),
                          ),
                          SizedBox(
                            width: 8 * SizeConfig.widthMultiplier,
                          ),
                          Text(
                            "View Job",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 2.5 * SizeConfig.textMultiplier
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  height: (100 * SizeConfig.heightMultiplier) - 84,
                  color: Color(0xFFECECEC),
                  padding: EdgeInsets.only(left: 5.0, right: 5.0,  top: 15.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            "JOB DETAILS",
                            style: TextStyle(
                                color: Color(0xFF393939),
                                fontSize: 2.0 * SizeConfig.textMultiplier
                            ),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        (job != null) ? _topCardContainer(job) : SizedBox(),
                        SizedBox(height: 15.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            "ADDRESS",
                            style: TextStyle(
                                color: Color(0xFF393939),
                                fontSize: 2.0 * SizeConfig.textMultiplier
                            ),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        (job != null) ? _addressCardContainer(job) : SizedBox(),
                        (job != null && job.jobStatus != null && job.jobStatus != "0") ? Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(height: 15.0),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                "QUESTIONS",
                                style: TextStyle(
                                    color: Color(0xFF393939),
                                    fontSize: 2.0 * SizeConfig.textMultiplier
                                ),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            (job?.jobTypeQuestions != null && job?.jobTypeQuestions.length > 0) ? _questionMainCardContainer(job) : _noQuestionCardContainer(job)
                          ],
                        ) : SizedBox(),
                        SizedBox(height: 15.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            "BEFORE WORK",
                            style: TextStyle(
                                color: Color(0xFF393939),
                                fontSize: 2.0 * SizeConfig.textMultiplier
                            ),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        (job != null) ? _beforeCardContainer(job) : SizedBox(),
                        (job != null && job.jobStatus != null && job?.jobStatus != "0") ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 15.0),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                "AFTER WORK",
                                style: TextStyle(
                                    color: Color(0xFF393939),
                                    fontSize: 2.0 * SizeConfig.textMultiplier
                                ),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            (job != null) ? _afterCardContainer(job) : SizedBox(),
                          ],
                        ) : SizedBox()
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _questionMainCardContainer(Jobs job){
    //print("length of jobTypequestions : ${job.jobTypeQuestions.length}");
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        for(int i = 0; i < job.jobTypeQuestions.length; i++)
          _questionAnswerContainer(job.jobTypeQuestions[i], i)
      ],
    );
  }

  Widget _questionAnswerContainer(JobTypeQuestions jobTypeQuestions, int index){
    return Card(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        color: Colors.white,
        elevation: 5,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.transparent, width: 0),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                "${index+1}. ${jobTypeQuestions.question}",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 1.9 * SizeConfig.textMultiplier
                ),
              ),
              SizedBox(height: 10),
              (jobTypeQuestions.type == "text") ? _textTypeQuestionContainer(jobTypeQuestions) : ((jobTypeQuestions.type == "radio") ? _radioTypeQuestionContainer(jobTypeQuestions) : (jobTypeQuestions.type == "checkbox") ? _checkBoxTypeQuestionContainer(jobTypeQuestions) : SizedBox())
            ],
          ),
        )
    );
  }
  String _currText = '';
  //List<bool> _isChecked ;//= [false,false];
  Widget _checkBoxTypeQuestionContainer(JobTypeQuestions jobTypeQuestions){
    var optionsJson = jsonDecode(jobTypeQuestions.options);
    List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;

    return Column(
      children: optionsArray.map((data){
        int index = optionsArray.indexOf(data);
        return Row(
          children: [
            Checkbox(
                value: _checkList[index],
                tristate: false,
                //activeColor: Colors.green,
                visualDensity: VisualDensity(horizontal: -4.0, vertical: -4.0),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                onChanged: (value){
                }
            ),
            Text(
              data,
              style: TextStyle(
                fontSize: 1.9 * SizeConfig.textMultiplier,
                color:  Color(0xFF9F9F9F)
              ),
            ),
          ],
        );
      }).toList(),
    );
  }

  // Declare this variable
  String selectedRadioTile;

  setSelectedRadioTile(String val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  Widget _radioTypeQuestionContainer(JobTypeQuestions jobTypeQuestions){
    var optionsJson = jsonDecode(jobTypeQuestions.options);
    List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;
    if(jobTypeQuestions.answer.length  > 0 ){
      selectedRadioTile = jobTypeQuestions.answer;
    }

    //print(optionsArray);
    return Column(
      children: optionsArray.map((data){
        int index = optionsArray.indexOf(data);
        return Row(
          children: [
            Radio(
              value: data,
              groupValue: selectedRadioTile,
              onChanged: (String val) {
              },
              //toggleable: false,
              visualDensity: VisualDensity(horizontal: -4.0, vertical: -4.0),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            ),
            Text(
              data,
              style: TextStyle(
                fontSize: 1.9 * SizeConfig.textMultiplier,
                color:  Color(0xFF9F9F9F)
              ),
            ),
          ],
        );
      }).toList(),
    );
  }

  Widget _textTypeQuestionContainer(JobTypeQuestions jobTypeQuestions){
    return TextFormField(
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF9F9F9F)),
                borderRadius: BorderRadius.all(Radius.circular(0.0))
            ),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF9F9F9F)),
                borderRadius: BorderRadius.all(Radius.circular(0.0))
            ),
            fillColor: Color(0xFFF7F7F7),
            filled: true,
            contentPadding: EdgeInsets.all(10.0)
        ),
        style: TextStyle(
          fontSize: 1.8 * SizeConfig.textMultiplier,
          color:  Color(0xFF9F9F9F)
        ),
        maxLines: 2,
        textInputAction: TextInputAction.newline,
        enabled: false,
        initialValue: (jobTypeQuestions.answer != null) ? jobTypeQuestions.answer : "NA",
    );
  }

  Widget _noQuestionCardContainer(Jobs job){
    return Card(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        color: Colors.white,
        elevation: 5,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.transparent, width: 0),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
          child: Center(
            child: Text(
              "No Questions",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontSize: 2.5 * SizeConfig.textMultiplier
              ),
            ),
          ),
        )
    );
  }

  Widget _beforeCardContainer(Jobs Job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                "Comment and Driver Signature",
                style: TextStyle(
                    fontSize: 1.8 * SizeConfig.textMultiplier,
                    color: Color(0xFF244764),
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            SizedBox(height: 15.0),
            _imageRowContainerBefore(job),
            SizedBox(height: 15.0),
            Row(
              children: [
                Expanded(
                  child:
                  Container(
                    padding: EdgeInsets.all(10.0),
                    height: 75,
                    width: 75,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xFFB3B8B7),
                      ),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child:  SingleChildScrollView(
                      scrollDirection: Axis.vertical,//.horizontal
                      child: Text(
                        _driverComment,
                        style: TextStyle(
                          fontSize: 1.8 * SizeConfig.textMultiplier,
                          color: Colors.grey
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  width: 38 * SizeConfig.widthMultiplier,
                  height: 75.0,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xFFB3B8B7)),
                      borderRadius: BorderRadius.all(Radius.circular(4.0))
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: beforeSignatureImage(),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ) ;
  }

  Widget _imageRowContainerBefore(Jobs job){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: 28 * SizeConfig.widthMultiplier,
            height: 25 * SizeConfig.widthMultiplier,
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: beforeImage(0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
              ),
              elevation: 2,
              //margin: EdgeInsets.all(5),
            ),
          ),
          Container(
            width: 28 * SizeConfig.widthMultiplier,
            height: 25 * SizeConfig.widthMultiplier,
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: beforeImage(1),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
              ),
              elevation: 2,
              //margin: EdgeInsets.all(5),
            ),
          ),
          Container(
            width: 28 * SizeConfig.widthMultiplier,
            height: 25 * SizeConfig.widthMultiplier,
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: beforeImage(2),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
              ),
              elevation: 2,
              //margin: EdgeInsets.all(5),
            ),
          ),
        ],
      ),
    );
  }

  Widget _imageRowContainerAfter(Jobs job){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            width: 28 * SizeConfig.widthMultiplier,
            height: 25 * SizeConfig.widthMultiplier,
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: afterImage(0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
              ),
              elevation: 2,
              //margin: EdgeInsets.all(5),
            ),
          ),
          Container(
            width: 28 * SizeConfig.widthMultiplier,
            height: 25 * SizeConfig.widthMultiplier,
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: afterImage(1),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
              ),
              elevation: 2,
              //margin: EdgeInsets.all(5),
            ),
          ),
          Container(
            width: 28 * SizeConfig.widthMultiplier,
            height: 25 * SizeConfig.widthMultiplier,
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: afterImage(2),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
              ),
              elevation: 2,
              //margin: EdgeInsets.all(5),
            ),
          ),
        ],
      ),
    );
  }

  Widget _afterCardContainer(Jobs Job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                "Comment and Customer Signature",
                style: TextStyle(
                    fontSize: 1.8 * SizeConfig.textMultiplier,
                    color: Color(0xFF244764),
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            SizedBox(height: 15.0),
            _imageRowContainerAfter(job),
            SizedBox(height: 15.0),
            Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    height: 75,
                    width: 75,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xFFB3B8B7),
                      ),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: Text(
                      _customerComment,
                      style: TextStyle(
                        fontSize: 1.8 * SizeConfig.textMultiplier,
                        color: Colors.grey
                      ),
                      maxLines: 3,
                    ),
                  ),/*TextFormField(
                      controller: _customerCommentController,
                      enabled: isCustomerCommentEnabled,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFB3B8B7))
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFB3B8B7))
                          ),
                          hintText: 'Comment',
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.w600
                          ),
                          contentPadding: EdgeInsets.all(15.0)
                      ),
                      style: TextStyle(
                          fontSize: 1.8 * SizeConfig.textMultiplier
                      ),
                      maxLines: 3,
                      textInputAction: TextInputAction.newline,
                  ),*/
                ),
                SizedBox(width: 10),
                Container(
                  width: 38 * SizeConfig.widthMultiplier,
                  height: 75.0,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xFFB3B8B7)),
                      borderRadius: BorderRadius.all(Radius.circular(4.0))
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: afterSignatureImage(),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ) ;
  }


  Widget _addressCardContainer(Jobs job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0, top:5.0, bottom: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                (job.address != null) ? job.address : "NA",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: AppTheme.colorTextJobDetail,
                    fontSize: 1.8 * SizeConfig.textMultiplier,
                    height: 1.3
                ),
              ),
            ),
            Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                        child: Text(
                          "${((job.street != null) || (job.street2 != null)) ? job.street +" "+ job.street2 : "NA"}",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: AppTheme.colorTextJobDetail,
                              fontSize: 1.8 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                        child: Text(
                          (job.country != null) ? job.country : "NA",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: AppTheme.colorTextJobDetail,
                              fontSize: 1.8 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                    ],
                  ),
                ),
                SizedBox(width: 20.0),
                Container(
                  width: 36 * SizeConfig.widthMultiplier,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                        child: Text(
                          "${(job.city != null) ? job.city : "NA"}${(job.state != null) ? ", " + job.state : ""}",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: AppTheme.colorTextJobDetail,
                              fontSize: 1.8 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                            child: Text(
                              (job.zip != null) ? job.zip : "NA",
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: AppTheme.colorTextJobDetail,
                                  fontSize: 1.8 * SizeConfig.textMultiplier
                              ),
                            ),
                          ),
                          Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                height: 5 * SizeConfig.heightMultiplier,
                width: 38 * SizeConfig.widthMultiplier,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MapViewScreen(job: job)
                      ),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: AppTheme.primaryColor, //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                        style: BorderStyle.solid,
                        width: 1.0,
                      ),
                      color: AppTheme.primaryColor, //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text(
                            "Map View",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 1.8 * SizeConfig.textMultiplier,
                              fontWeight: FontWeight.normal,
                              letterSpacing: 1,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _topCardContainer(Jobs job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        constraints: BoxConstraints(minHeight : 90, maxHeight: 110),
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    (job.jobName != null) ? job.jobName : "NA",
                    style: TextStyle(
                      fontSize: 2.4 * SizeConfig.textMultiplier,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 7.0),
                  Text(
                    (job.customerName != null) ? job.customerName : "NA",
                    style: TextStyle(
                        color: AppTheme.colorTextGrey,
                        fontSize: 1.8 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 4.5),
                  Text(
                    (job.jobType != null) ? job.jobType : "NA",
                    style: TextStyle(
                        color: AppTheme.colorTextGrey,
                        fontSize: 1.8 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 4.5),
                  Text(
                    (job.driverName != null) ? job.driverName : "NA",
                    style: TextStyle(
                        color: AppTheme.colorTextGrey,
                        fontSize: 1.8 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(height: 3.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "${Constants.IMAGE_PATH}calendar.png",
                      height: 12.0,
                      width: 12.0,
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      "${(job.startDate != null) ? DataTimeUtility.convertTimeForJob(job.startDate) : "NA"} ${(job.startTime!=null) ? job.startTime : "NA"}",
                      style: TextStyle(
                        fontSize: 1.6 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal,
                        color: AppTheme.colorTextGrey,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 7 * SizeConfig.heightMultiplier),
                Text(
                  job.statusShow,
                  style: TextStyle(
                      fontSize: 2 * SizeConfig.textMultiplier,
                      color: (job.jobStatus == "0") ? Color(0xFF244764) : ((job.jobStatus == "1") ? Color(0xFFD5804D) : AppTheme.colorCompleteJobStatus),
                      fontWeight: FontWeight.normal
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

