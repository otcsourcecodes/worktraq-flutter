import 'dart:io';
import 'dart:typed_data';

import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:signature/signature.dart';
import 'package:simple_permissions/simple_permissions.dart';

class SignatureScreen extends StatefulWidget {
  @override
  _SignatureScreenState createState() => _SignatureScreenState();
}

class _SignatureScreenState extends State<SignatureScreen> {
  String _platformVersion = 'Unknown';
  Permission _permission = Permission.WriteExternalStorage;

  final SignatureController _controller = SignatureController(
    penStrokeWidth: 4,
    penColor: Colors.black,
    exportBackgroundColor: Colors.white,
  );

  @override
  void initState() {
    super.initState();
    _controller.addListener(() => ""/*print("Value changed")*/);
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) =>
          Scaffold(
            body: ListView(
              children: <Widget>[
                Container(
                  color: AppTheme.primaryColor,
                  child: Flex(
                    direction: Axis.vertical,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right:  2 * SizeConfig.heightMultiplier, left:  2 * SizeConfig.heightMultiplier),
                        height: 56,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap:() {
                                setState(() => _controller.clear());
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                "Cancel",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 2.5 * SizeConfig.textMultiplier
                                ),
                              ),
                            ),
                            Text(
                              "Signature",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 2.5 * SizeConfig.textMultiplier
                              ),
                            ),
                            GestureDetector(
                              onTap: () async {
                                if (_controller.isNotEmpty) {
                                  var data = await _controller.toPngBytes();
                                  //print(data);
                                  // requesting external storage permission
                                  if(!(await checkPermission())) await requestPermission();

                                  // Use plugin [path_provider] to export image to storage
                                  //Directory directory = await getExternalStorageDirectory();
                                  Directory directory = await getApplicationDocumentsDirectory();
                                  String path = directory.path;
                                  //print(path);

                                  String signatureFileName = '$path/${Constants.SIGNATURE_IMAGE_DIRECTORY}/${DateTime.now().microsecondsSinceEpoch}.png';

                                  //print(signatureFileName);

                                  // create directory on external storage
                                  await Directory('$path/${Constants.SIGNATURE_IMAGE_DIRECTORY}').create(recursive: true).then((value){
                                    writeToFile(data.buffer.asInt8List(), signatureFileName);

                                    Navigator.pop(context, signatureFileName);
                                  });

                                  // write to storage as a filename.png
                                  //File('$path/${IMAGE_DIRECTORY}/$signatureFileName.png').writeAsBytesSync(data.buffer.asInt8List());
                                }
                              },
                              child: Text(
                                "Done",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 2.5 * SizeConfig.textMultiplier
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                //SIGNATURE CANVAS
                Signature(
                  controller: _controller,
                  height: 90 * SizeConfig.heightMultiplier,
                  backgroundColor: Colors.white,
                ),
              ],
            ),
          ),
    );
  }

  Future<void> writeToFile(Int8List int8list, String path) {
    return new File(path).writeAsBytes(int8list);
  }

  requestPermission() async {
    PermissionStatus result = await SimplePermissions.requestPermission(_permission);
    return result;
  }

  checkPermission() async {
    bool result = await SimplePermissions.checkPermission(_permission);
    return result;
  }

  getPermissionStatus() async {
    final result = await SimplePermissions.getPermissionStatus(_permission);
    print("permission status is " + result.toString());
  }

}