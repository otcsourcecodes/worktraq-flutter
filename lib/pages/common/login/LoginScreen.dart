import 'dart:isolate';
import 'dart:ui';
import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/models/Users.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/pages/common/forgot_password/ForgotPassword.dart';
import 'package:worktraq/pages/customers/AllCustomerJobScreen.dart';
import 'package:worktraq/pages/drivers/JobScreen.dart';
import 'package:worktraq/services/DriverLocationServiceRepository.dart';
import 'package:worktraq/services/DriverTrackingHelper.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/CustomDialog.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:worktraq/utils/Validators.dart';
import 'package:worktraq/widgets/LoadingScreen.dart';
import 'package:driver_background_locator/location_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

import 'bloc/LoginBloc.dart';
import 'bloc/LoginState.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _obscureText = true;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey _globalKey = GlobalKey<State>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  var db = new DatabaseHelper();
  DriverTrackingHelper driverTrackingHelper;
  ReceivePort port = ReceivePort();

  LoginBloc _loginBloc;

  bool get isPopulated => _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  String _password;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    /*_emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);*/
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Future<void> updateUI(LocationDto data) async {
    if(mounted) {
      setState(() {
        if (data != null) {
          DriverTrackingHelper.lastDriverLocation = data;

          print(("location lat ${data.latitude} long ${data.longitude}"));
          //showToast("location lat ${data.latitude} long ${data.longitude}", gravity: Toast.BOTTOM);
        }
      });
    }
  }

  void _onFormSubmitted() async{
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet) {
      loginService();
    } else {
      //showErrorDialog(context, "Error", "No internet connection!");
      final snackBar = SnackBar(
        content: Text("No internet connection!"),
        action: SnackBarAction(
          label: 'RETRY',
          textColor: Colors.red,
          onPressed: () {
           _onFormSubmitted();
          },
        ),
      );

      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState..showSnackBar(snackBar);
    }
  }

  void loginService(){
    String email = _emailController.text;
    String password = _passwordController.text;
    if( email.length == 0 ){
      showErrorDialog(context, 'Error', 'Please enter email address.');
    } else if(!Validators.isValidEmail(_emailController.text)) {
      showErrorDialog(context, 'Error', 'Please enter valid email address.');
    } else if( password.length == 0 ){
      showErrorDialog(context, 'Error', "Please enter password.");
    } else{
      //LoadingDialog.showLoadingDialog(context, _globalKey, "Loading...");

      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );

      NetworkConfig.restClientInstance().login(email, password, "1", "98765432456hcbcgfbccvbcbcbc").then((result){
        if(result.status == "success"){
          Users users = result.users;
          users.fireBaseToken = "abcd";
          users.isLogout = true;
          print("${users.toString()}");
          Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();

          db.addUser(users);
          AppSharedPreferences.createLoginSession(users);

          if(users.userType == "1") {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                    builder: (context) => AllCustomerJobScreen()
                )
            );
          } else if(users.userType == "2") {

            Future.delayed(const Duration(seconds: 2), (){
              driverTrackingHelper = DriverTrackingHelper(context: context, authToken: users.authToken);

              if (IsolateNameServer.lookupPortByName(DriverLocationServiceRepository.isolateName) != null) {
                IsolateNameServer.removePortNameMapping(DriverLocationServiceRepository.isolateName);
              }

              IsolateNameServer.registerPortWithName(port.sendPort, DriverLocationServiceRepository.isolateName);

              port.listen((dynamic data) async {
                await updateUI(data);
              },
              );
              driverTrackingHelper.initPlatformState();

              driverTrackingHelper.onStop();
              driverTrackingHelper.onStart();
            }
            );

            Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                    builder: (context) => JobScreen()
                )
            );
          }
          /*db.addUser(users).then((result) {
            if(result > 0){
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (context) => JobScreen()
                  )
              );
            } else{
              showErrorDialog(context,  'Error', "Data not inserted.");
            }
          });*/
        }else{
          Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
          showErrorDialog(context,  'Error', result.message);
        }
        //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
      }).catchError((onError){
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        NetworkConfig.logger.e(onError.toString());
        showErrorDialog(context, "Server Error", onError.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(AppTheme.primaryDarkColor);
    return SafeArea(
      top: true,
      child: Scaffold(
          key: _scaffoldKey,
          body: SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  //height: 100 * SizeConfig.heightMultiplier-28,
                  constraints: BoxConstraints(maxHeight: (SizeConfig.screenWidth > 450) ? 98 * SizeConfig.heightMultiplier : 96 * SizeConfig.heightMultiplier),
                  //constraints: BoxConstraints(minHeight: 96.15 * SizeConfig.heightMultiplier),
                  color: AppTheme.backgroundAppColor,
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    margin: EdgeInsets.only(top: 12 * SizeConfig.heightMultiplier, left: 7.7*SizeConfig.widthMultiplier, right: 7.7*SizeConfig.widthMultiplier),
                    child: Image.asset(
                      "${Constants.IMAGE_PATH}worktraq_logo_stacked2.png",
                      width: 70 * SizeConfig.widthMultiplier * 0.8,
                      //height: 30 * SizeConfig.heightMultiplier,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 45 * SizeConfig.heightMultiplier),
                    Container(
                      margin: EdgeInsets.only(top: 3.6*SizeConfig.heightMultiplier, right: 4.8*SizeConfig.widthMultiplier, left: 4.8*SizeConfig.widthMultiplier),
                      child: BlocBuilder<LoginBloc, LoginState>(
                        builder: (context, state){
                          return Form(
                            child: Column(
                              children: <Widget>[
                                Material(
                                  elevation: 5,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4.3*SizeConfig.heightMultiplier),
                                  ),
                                  child: TextFormField(
                                    controller: _emailController,
                                    decoration: new InputDecoration(
                                      border: new OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(4.3*SizeConfig.heightMultiplier),
                                        ),
                                        borderSide: BorderSide(
                                          width: 0,
                                          style: BorderStyle.none,
                                        ),
                                      ),
                                      isDense: true,
                                      //contentPadding: EdgeInsets.only(left: 40.0),
                                      filled: true,
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.only(left: 1.2*SizeConfig.widthMultiplier, right: 1.2*SizeConfig.widthMultiplier),
                                        child: Image.asset(
                                          "${Constants.IMAGE_PATH}email.png",
                                          scale: 3.5,
                                          //alignment: Alignment.centerLeft,
                                        ),
                                      ),
                                      hintStyle: new TextStyle(color: Color(0xFF777777)),
                                      hintText: "Email address",
                                      fillColor: Color(0xFFFFFFFF),
                                      contentPadding: EdgeInsets.symmetric(vertical: ((100*SizeConfig.widthMultiplier) > 450) ? 20.0 : 0.0),
                                    ),
                                    autovalidate: true,
                                    autocorrect: false,
                                  ),
                                ),
                                SizedBox(height: 2.9*SizeConfig.heightMultiplier),
                                Material(
                                  elevation: 5,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4.3*SizeConfig.heightMultiplier),
                                  ),
                                  child: TextFormField(
                                    controller: _passwordController,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(4.3*SizeConfig.heightMultiplier),
                                          ),
                                          borderSide: BorderSide(
                                            width: 0,
                                            style: BorderStyle.none,
                                          ),
                                        ),
                                        //contentPadding: EdgeInsets.only(left: 20.0),
                                      contentPadding: EdgeInsets.symmetric(vertical: ((100*SizeConfig.widthMultiplier) > 450) ? 20.0 : 0.0),
                                        filled: true,
                                        hintStyle: new TextStyle(color: Color(0xFF777777)),
                                        hintText: "Password",
                                        fillColor: Color(0xFFFFFFFF),
                                        prefixIcon: Padding(
                                          padding: EdgeInsets.only(left: 1.2*SizeConfig.widthMultiplier, right: 1.2*SizeConfig.widthMultiplier),
                                          child: Image.asset(
                                            "${Constants.IMAGE_PATH}password.png",
                                            scale: 3.5,
                                            //alignment: Alignment.centerLeft,
                                          ),
                                        ),
                                    ),
                                    obscureText: _obscureText,
                                    autovalidate: true,
                                    autocorrect: false,
                                    /*validator: (_) {
                                      return !state.isPasswordValid ? 'Invalid Password' : null;
                                    }*/
                                  ),
                                ),
                                SizedBox(height: 5 * SizeConfig.heightMultiplier),
                                Material(
                                  elevation: 5,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(4.3*SizeConfig.heightMultiplier),
                                  ),
                                  child: Container(
                                    height: 6.5 * SizeConfig.heightMultiplier,
                                    child: GestureDetector(
                                      //onTap: isLoginButtonEnabled(state) ? _onFormSubmitted : checkValidate,
                                      onTap: _onFormSubmitted/*isLoginButtonEnabled(state) ? _onFormSubmitted : null*/,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Color(0xFF2D6087), //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                                            style: BorderStyle.solid,
                                            width: 1.0,
                                          ),
                                          color: Color(0xFF2D6087), //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                                          borderRadius: BorderRadius.circular(4.3*SizeConfig.heightMultiplier),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Center(
                                              child: Text(
                                                "Login",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 2 * SizeConfig.textMultiplier,
                                                  fontWeight: FontWeight.w600,
                                                  letterSpacing: 1,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 3.5 * SizeConfig.heightMultiplier),
                                InkWell(
                                  onTap: (){
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => ForgotPassword()
                                        )
                                    );
                                  },
                                  child: Text(
                                    "Forgot Password?",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 2.2 * SizeConfig.textMultiplier
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          )
      ),
    );
  }
}
