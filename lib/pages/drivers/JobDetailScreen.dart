
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:background_locator/location_dto.dart';
import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/models/AfterWork.dart';
import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/models/BeforeWork.dart';
import 'package:worktraq/models/JobActivityResponse.dart';
import 'package:worktraq/models/JobDetailResponse.dart';
import 'package:worktraq/models/JobReport.dart';
import 'package:worktraq/models/JobTypeQuestions.dart';
import 'package:worktraq/models/Jobs.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/pages/common/MapViewScreen.dart';
import 'package:worktraq/pages/common/SignatureScreen.dart';
import 'package:worktraq/pages/drivers/JobCompletedScreen.dart';
import 'package:worktraq/services/DriverTrackingHelper.dart';
import 'package:worktraq/services/JobTrackingHelper.dart';
import 'package:worktraq/services/LocationServiceRepository.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/CustomDialog.dart';
import 'package:worktraq/utils/DateTimeUtility.dart';
import 'package:worktraq/utils/PolygonUtility.dart';
import 'package:worktraq/utils/ResponseHandler.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:worktraq/widgets/LoadingScreen.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart' hide Response;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'dart:convert';

import 'package:path/path.dart' hide context;
import 'package:async/async.dart';
import 'package:toast/toast.dart';

class JobDetailScreen extends StatefulWidget {
  final String id;

  const JobDetailScreen({Key key, this.id}) : super(key: key);
  
  @override
  _JobDetailScreenState createState() => _JobDetailScreenState();
}

class _JobDetailScreenState extends State<JobDetailScreen> with WidgetsBindingObserver {
  Jobs job;
  String jobStatus = "0";
  BeforeWork beforeWork;
  AfterWork afterWork;
  List<String> workImages, workImageAfter;
  final GlobalKey _globalKey = GlobalKey<State>();
  String imagePath = "";
  String finalImagePathBefore1 = "";
  String finalImagePathBefore2 = "";
  String finalImagePathBefore3 = "";
  String finalSignaturePathBefore = "";

  String finalImagePathAfter1 = "";
  String finalImagePathAfter2 = "";
  String finalImagePathAfter3 = "";
  String finalSignaturePathAfter = "";

  File _imageFile;
  PickedFile _pickedFile;
  final picker = ImagePicker();
  dynamic _pickImageError;
  DateTime _currentDateTime =  DateTime.now();
  TextEditingController _commentController = TextEditingController();
  TextEditingController _customerCommentController = TextEditingController();
  String buttonText = "START JOB";
  bool isStartButtonEnabled = false;
  bool isEndButtonEnabled = false;
  bool isCommentEnabled = false;
  bool isCustomerCommentEnabled = false;
  bool isProgress = false;
  bool isEnterPolygon = false;
  List<LatLng> pointList = List();
  Map<String, String> answersMap = Map();
  Location location;

  ReceivePort port = ReceivePort();
  ReceivePort portDriver = ReceivePort();
  JobTrackingHelper jobTrackingHelper;
  List<bool> _checkList;
  List<String> cbResult;
  // When data comes from local database.
  bool isLocalData = true;

  @override
  void initState() {
    super.initState();
  }

  Future<void> updateUI(LocationDto data, [List<LatLng> pointList]) async {
    if(mounted) {
      setState(() {
        if (data != null) {
          JobTrackingHelper.lastLocation = data;
          //print(("location lat ${data.latitude} long ${data.longitude}"));
          //showToast("location lat ${data.latitude} long ${data.longitude}", gravity: Toast.BOTTOM);
        }
      });
    }
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  @override
  void didChangeDependencies()async {
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet){
      getJobDetailByIdService();
    } else {
      getJobDetailFromLocalDb(widget.id);
      //showErrorDialog(context, "Error", "No internet connection!");
    }

    super.didChangeDependencies();
  }

  Future<void> trackingJobUpdate(String latitude, String longitude) async {
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet){
      String authToken = await AppSharedPreferences.getAuthToken();
   //   print("authToken $authToken");
     // print("id ${widget.id}");

      NetworkConfig.restClientInstance().trackingJobUpdate(authToken, widget.id, latitude, longitude).then((result) {
        if (result.status == "success") {
          print("Message " + result.message);
        }else{
          showErrorDialog(context,  'Error', result.message);
        }
      }).catchError((Object obj){
        switch (obj.runtimeType) {
          case DioError:
            final res = (obj as DioError).response;
            // Here's the sample to get the failed response error code and message
            //print("${res.data.toString()} ");
            if(res.data != null) {
              AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res.data);
              print("${res.data.toString()} ");
              if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                ResponseHandler.handleResponse(context, authFailedResponse);
              }
            } else {
              NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
              showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
            }
            break;
          default:
            NetworkConfig.logger.e("Got error : ${obj.toString()}");
            break;
        }
      });
    }  else {
      showErrorDialog(context, "Error", "No internet connection!");
    }
  }

  Future<void> getJobDetailByIdService() async {
    isProgress = await AppSharedPreferences.isInProgress();

    setState(() {
      if(isProgress != null)
        isProgress = isProgress;
      else
        isProgress = false;
    });

    DateTime now = DateTime.now();
    String nowDate = DateFormat('dd/MM/yyyy').format(now);
    print(nowDate);

    Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );
    });

    String authToken = await AppSharedPreferences.getAuthToken();
    print("authToken $authToken");
    print("id ${widget.id}");

    NetworkConfig.restClientInstance().getJobDetailById(authToken, widget.id).then((result){
      if(result.status == "success"){
        JobDetailResponse jobDetailResponse = result;
        setState(() {
          job = jobDetailResponse.jobs;
          beforeWork = job.jobReport.beforeWork;
          afterWork = job.jobReport.afterWork;
          workImages = beforeWork?.workImage;
          workImageAfter = afterWork?.workImage;

          isLocalData = false;
          jobStatus = job.jobStatus;

          if(job.jobStatus == "0"){
            buttonText = "START JOB";
          } else if(job.jobStatus == "1"){
            buttonText = "END JOB";
          }

          if(job.geoFencing == "1"){
            String points = job.points;
            points = points.substring(0, points.length-1);
            var pointArray = points.split("|");

            //print(pointArray);
            for(int i = 0; i < pointArray.length; i++){
              var point = pointArray[i].split(",");
              LatLng location = LatLng(double.parse(point[0]), double.parse(point[1]));
              pointList.add(location);
              //print("i = ${pointArray[i]}");
            }

            Future.delayed(const Duration(seconds: 2), (){
              location = Location();
              location.getLocation().then((location){
                //print("nkp job detail : ${location.latitude}");
                bool isPolygonStatus = false;
                if(PolygonUtility.containsLocation(LatLng(location.latitude, location.longitude), pointList, true)){
                  isPolygonStatus = true;
                } else{
                  isPolygonStatus = false;
                }
                jobTrackingHelper = JobTrackingHelper(context: context, jobId: job.jobId, authToken: authToken, points: points, isEnterPolygon: isPolygonStatus);
              }).whenComplete(() {
                if (IsolateNameServer.lookupPortByName(LocationServiceRepository.isolateName) != null) {
                  IsolateNameServer.removePortNameMapping(LocationServiceRepository.isolateName);
                }

                IsolateNameServer.registerPortWithName(port.sendPort, LocationServiceRepository.isolateName);

                port.listen((dynamic data) async {
                  await updateUI(data, pointList);
                });
                jobTrackingHelper.initPlatformState();

                jobTrackingHelper.onStop();
                if(job.jobStatus == "1"){
                  jobTrackingHelper.onStart();
                }
              });
            });
          }

          if(beforeWork?.comments != null) {
            _commentController.text = beforeWork?.comments;
            isCommentEnabled = false;
          } else{
            isCommentEnabled = true;
          }

          if(afterWork?.comments != null){
            _customerCommentController.text = afterWork?.comments;
            isCustomerCommentEnabled = false;
          } else{
            isCustomerCommentEnabled = true;
          }

          if(job != null && job.jobTypeQuestions != null && job.jobTypeQuestions.length > 0){
            for(int i = 0; i < job.jobTypeQuestions.length; i++){
              JobTypeQuestions jobTypeQuestions = job.jobTypeQuestions[i];

              if(jobTypeQuestions != null && jobTypeQuestions.type == "checkbox"){
                var optionsJson = jsonDecode(jobTypeQuestions.options);
                List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;
                _checkList = List(optionsArray.length);
                //cbResult = List(optionsArray.length);
                cbResult = List();

                //print(optionsArray);
                for(int j=0; j < optionsArray.length; j++){
                  _checkList[j] = false;
                }
              }
            }
          }
        });
        //print("${jobDetailResponse.toString()}");
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
      }else{
        showErrorDialog(context,  'Error', result.message);
      }
      //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
    }).catchError((Object obj){
      Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          // Here's the sample to get the failed response error code and message
          //print("${res.data.toString()} ");
          if(res != null){
            if(res.data != null) {
              AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res.data);
              print("${res.data.toString()} ");
              if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                ResponseHandler.handleResponse(context, authFailedResponse);
              }
            } else {
              NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
              showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
            }
          }
          break;
        default:
          NetworkConfig.logger.e("Got error : ${obj.toString()}");
          break;
      }
    });
  }

  void getJobDetailFromLocalDb(String jobId) async{
    isProgress = await AppSharedPreferences.isInProgress();

    setState(() {
      if(isProgress != null)
        isProgress = isProgress;
      else
        isProgress = false;
    });

    DateTime now = DateTime.now();
    String nowDate = DateFormat('dd/MM/yyyy').format(now);
    print(nowDate);

    Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );
    });

    Future.delayed(Duration(seconds: 2), () {
        JobTrackingHelper().onStop();
        DriverTrackingHelper().onStop();

        DatabaseHelper databaseHelper = DatabaseHelper();
        databaseHelper.getJobDetailByJobIdFromLocal(widget.id).then((jobs){
          setState(() {
            job = jobs;
            print("job local : $job");
            beforeWork = job?.jobReport.beforeWork;
            afterWork = job?.jobReport.afterWork;
            workImages = beforeWork?.workImage;
            workImageAfter = afterWork?.workImage;
            print("workImageAfter : $workImageAfter");

            isLocalData = true;

            jobStatus = job?.jobStatus;

            if(job?.jobStatus == "0"){
              buttonText = "START JOB";
            } else if(job?.jobStatus == "1"){
              buttonText = "END JOB";
            }

            if(beforeWork?.comments != null) {
              _commentController.text = beforeWork?.comments;
              isCommentEnabled = false;
            } else{
              isCommentEnabled = true;
            }

            if(afterWork?.comments != null){
              _customerCommentController.text = afterWork?.comments;
              isCustomerCommentEnabled = false;
            } else{
              isCustomerCommentEnabled = true;
            }

            if(job != null && job.jobTypeQuestions != null && job.jobTypeQuestions.length > 0){
              for(int i = 0; i < job.jobTypeQuestions.length; i++){
                JobTypeQuestions jobTypeQuestions = job.jobTypeQuestions[i];

                if(jobTypeQuestions != null && jobTypeQuestions.type == "checkbox"){
                  var optionsJson = jsonDecode(jobTypeQuestions.options);
                  List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;
                  _checkList = List(optionsArray.length);
                  //cbResult = List(optionsArray.length);
                  cbResult = List();

                  //print(optionsArray);
                  for(int j=0; j < optionsArray.length; j++){
                    _checkList[j] = false;
                  }
                }
              }
            }
          });
          Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        });
    });
  }

  void _checkButtonEnabled(){
    setState(() {
      if( _commentController.text != null && _commentController.text.length > 0
          && finalImagePathBefore1.length > 0 && finalImagePathBefore2.length > 0
          && finalImagePathBefore3.length > 0 && finalSignaturePathBefore.length > 0){
        isStartButtonEnabled = true;
      } else{
        isStartButtonEnabled = false;
      }
    });
  }

  void _checkEndButtonEnabled(){
    setState(() {
      if( _customerCommentController.text != null && _customerCommentController.text.length > 0
          && finalImagePathAfter1.length > 0 && finalImagePathAfter2.length > 0
          && finalImagePathAfter3.length > 0 && finalSignaturePathAfter.length > 0){
        /*if( answersMap != null && answersMap.length > 0 && job != null  && (answersMap.length ==job.jobTypeQuestions.length)){
          isEndButtonEnabled = true;
        }else{
          if(job.jobTypeQuestions.length > 0 )
            isEndButtonEnabled = false;
          else
            isEndButtonEnabled = true;
        }*/
        isEndButtonEnabled = true;
      } else{
        isEndButtonEnabled = false;
      }
    });
  }

  Widget beforeImage(int position){
    //print(finalImagePathBefore1);
    if( position == 0 && finalImagePathBefore1.length > 0){
      return Image.file(File(finalImagePathBefore1), fit: BoxFit.fill);
    } else if( position == 1 && finalImagePathBefore2.length > 0){
      return Image.file(File(finalImagePathBefore2), fit: BoxFit.fill);
    } else if( position == 2 && finalImagePathBefore3.length > 0){
      return Image.file(File(finalImagePathBefore3), fit: BoxFit.fill);
    } else if(isLocalData ){
      return ((beforeWork != null && (workImages.length > position )) ? Image.file(
          File(workImages[position]),
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
          }) : Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill));
    } else {
      return ((beforeWork != null && (workImages.length > position )) ? Image.network(
          workImages[position],
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
          }) : Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill));
    }
  }

  Widget afterImage(int position){
    //print(finalImagePathBefore1);
    if( position == 0 && finalImagePathAfter1.length > 0){
      return Image.file(File(finalImagePathAfter1), fit: BoxFit.fill);
    } else if( position == 1 && finalImagePathAfter2.length > 0){
      return Image.file(File(finalImagePathAfter2), fit: BoxFit.fill);
    } else if( position == 2 && finalImagePathAfter3.length > 0){
      return Image.file(File(finalImagePathAfter3), fit: BoxFit.fill);
    } else if(isLocalData){
      return ((afterWork != null && workImageAfter != null && workImageAfter.length > (position + 1) ) ? Image.file(
          File(workImages[position]),
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
          }) : Image.asset(
          "${Constants.IMAGE_PATH}camera_icon.png",
          fit: BoxFit.fill
      ));
    } else{
      return ((afterWork != null && workImageAfter != null && workImageAfter.length > (position + 1) ) ? Image.network(
          workImageAfter[position],
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Image.asset("${Constants.IMAGE_PATH}camera_icon.png", fit: BoxFit.fill);
          }) : Image.asset(
              "${Constants.IMAGE_PATH}camera_icon.png",
              fit: BoxFit.fill
          )
      );
    }
  }

  Widget beforeSignatureImage(){
    if(finalSignaturePathBefore != null && finalSignaturePathBefore.length > 0){
      return Image.file(File(finalSignaturePathBefore), fit: BoxFit.fill);
    } else if(isLocalData){
      return ((beforeWork != null && beforeWork?.driverSignature != null && (beforeWork?.driverSignature.length > 0 )) ? Image.file(
          File(beforeWork.driverSignature),
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Text(
              "Driver Signature",
              style: TextStyle(
                  fontSize: 1.8 * SizeConfig.textMultiplier,
                  color: AppTheme.colorTextGrey,
                  fontWeight: FontWeight.w500
              ),
              maxLines: 3,
            );
          }) : Text(
        "Driver Signature",
        style: TextStyle(
            fontSize: 1.8 * SizeConfig.textMultiplier,
            color: AppTheme.colorTextGrey,
            fontWeight: FontWeight.w500
        ),
        maxLines: 3,
      ));
    } else{
      return ((beforeWork != null && beforeWork?.driverSignature != null && (beforeWork?.driverSignature.length > 0 )) ? Image.network(
          beforeWork.driverSignature,
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Text(
              "Driver Signature",
              style: TextStyle(
                  fontSize: 1.8 * SizeConfig.textMultiplier,
                  color: AppTheme.colorTextGrey,
                  fontWeight: FontWeight.w500
              ),
              maxLines: 3,
            );
          }) : Text(
        "Driver Signature",
        style: TextStyle(
            fontSize: 1.8 * SizeConfig.textMultiplier,
            color: AppTheme.colorTextGrey,
            fontWeight: FontWeight.w500
        ),
        maxLines: 3,
      ));
    }
  }

  Widget afterSignatureImage(){
    if(finalSignaturePathAfter != null && finalSignaturePathAfter.length > 0){
      return Image.file(File(finalSignaturePathAfter), fit: BoxFit.fill);
    } else if(isLocalData) {
      return ((afterWork != null && afterWork?.customerSignature != null && (afterWork?.customerSignature?.length > 0 )) ? Image.file(
          File(afterWork.customerSignature),
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Text(
              "Customer Signature",
              style: TextStyle(
                  fontSize: 1.8 * SizeConfig.textMultiplier,
                  color: AppTheme.colorTextGrey,
                  fontWeight: FontWeight.w500
              ),
              maxLines: 3,
            );
          }) : Text(
        "Customer Signature",
        style: TextStyle(
            fontSize: 1.8 * SizeConfig.textMultiplier,
            color: AppTheme.colorTextGrey,
            fontWeight: FontWeight.w500
        ),
        maxLines: 3,
      ));
    } else{
      // ignore: null_aware_before_operator
      return ((afterWork != null && afterWork?.customerSignature != null && (afterWork?.customerSignature?.length > 0 )) ? Image.network(
          afterWork.customerSignature,
          fit: BoxFit.fill,
          errorBuilder: (context, error, stacktrace){
            return Text(
              "Customer Signature",
              style: TextStyle(
                  fontSize: 1.8 * SizeConfig.textMultiplier,
                  color: AppTheme.colorTextGrey,
                  fontWeight: FontWeight.w500
              ),
              maxLines: 3,
            );
          }) : Text(
        "Customer Signature",
        style: TextStyle(
            fontSize: 1.8 * SizeConfig.textMultiplier,
            color: AppTheme.colorTextGrey,
            fontWeight: FontWeight.w500
        ),
        maxLines: 3,
      ));
    }
  }

  Future<void> updateJobActivity(String action) async {
    Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );
    });

    String authToken = await AppSharedPreferences.getAuthToken();
    print("authToken $authToken");
    List<File> images = List();
    File signatureFile;
    String commentText;
    String questionAnswer = "";
    if(job.jobStatus == "0"){
      images.add(File(finalImagePathBefore1));
      images.add(File(finalImagePathBefore2));
      images.add(File(finalImagePathBefore3));
      images.add(File(finalImagePathBefore3));
      signatureFile = File(finalSignaturePathBefore);
/*      print("finalImagePathBefore1 : $finalImagePathBefore1");
      print("finalImagePathBefore2 : $finalImagePathBefore2");
      print("finalImagePathBefore3 : $finalImagePathBefore3");
      print("finalSignaturePathBefore : $finalSignaturePathBefore");*/
      commentText = _commentController.text;
    } else if(job.jobStatus == "1"){
      images.add(File(finalImagePathAfter1));
      images.add(File(finalImagePathAfter2));
      images.add(File(finalImagePathAfter3));
      images.add(File(finalImagePathAfter3));
      signatureFile = File(finalSignaturePathAfter);
      /*print("finalImagePathAfter1 : $finalImagePathAfter1");
      print("finalImagePathAfter2 : $finalImagePathAfter2");
      print("finalImagePathAfter3 : $finalImagePathAfter3");
      print("finalSignaturePathAfter : $finalSignaturePathAfter");*/

      commentText = _customerCommentController.text;

      if( job.jobTypeQuestions != null && job.jobTypeQuestions.length > 0 && (answersMap.length > 0) ){
        questionAnswer = "[";
        for(int i = 0; i < job.jobTypeQuestions.length; i++){
          JobTypeQuestions jobTypeQuestions = job.jobTypeQuestions[i];
          String questionType = jobTypeQuestions.type;
          questionAnswer = questionAnswer +  "{" + "\"questionId\":\"" + jobTypeQuestions.questionId + "\", ";
          if( questionType == "text" && answersMap.containsKey("${jobTypeQuestions.type}${jobTypeQuestions.questionId}")){
            if( i == (job.jobTypeQuestions.length -1) )
              questionAnswer = questionAnswer + "\"answer\":\"" + answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] + "\"}";
            else
              questionAnswer = questionAnswer + "\"answer\":\"" + answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] + "\"},";
          } else if( questionType == "radio"  && answersMap.containsKey("${jobTypeQuestions.type}${jobTypeQuestions.questionId}")){
            if( i == (job.jobTypeQuestions.length -1) )
              questionAnswer = questionAnswer + "\"answer\":\"" + answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] + "\"}";
            else
              questionAnswer = questionAnswer + "\"answer\":\"" + answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] + "\"},";
          } else if( questionType == "checkbox"  && answersMap.containsKey("${jobTypeQuestions.type}${jobTypeQuestions.questionId}")){
            String checkBoxOptions = answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"].replaceAll("[", "");
            checkBoxOptions = checkBoxOptions.replaceAll("]", "");
            checkBoxOptions = checkBoxOptions.replaceAll(", ", ",");
            if( i == (job.jobTypeQuestions.length -1) )
              questionAnswer = questionAnswer + "\"answer\":\"" + checkBoxOptions + "\"}";
            else
              questionAnswer = questionAnswer + "\"answer\":\"" + checkBoxOptions + "\"},";
          }
        }
        questionAnswer =  questionAnswer + "]";

        print(questionAnswer);
      }
    }

    DateTime now = DateTime.now();
    String nowDate = DateFormat('dd/MM/yyyy').format(now);
    print("current date : ${nowDate.toString()}");

    NetworkConfig.restClientInstance().updateJobActivity(authToken, images, signatureFile, widget.id, nowDate, action, commentText, questionAnswer).then((result){
      if(result.status == "success"){
        JobActivityResponse jobActivityResponse = result;
        DatabaseHelper().deleteJobById(widget.id);
        //print(jobActivityResponse.toString());
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        if(job.jobStatus == "0" ){
          Navigator.of(context).pop("start");
          jobTrackingHelper.onStart();
          AppSharedPreferences.setIsInProgress(true);
        } else if(job.jobStatus == "1"){
          jobTrackingHelper.onStop();
          AppSharedPreferences.setIsInProgress(false);
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => JobCompletedScreen()));
        }
      }else{
        Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        showErrorDialog(context,  'Error', result.message);
      }
      //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
    }).catchError((Object obj){
      if(!mounted) return;
      Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          if(res.data != null) {
            AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res?.data);
            print("${res.data.toString()} ");
            if (ResponseHandler.checkStatusCode(authFailedResponse)) {
              ResponseHandler.handleResponse(context, authFailedResponse);
            }
          } else {
            NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
            showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
          }
          break;
        default:
          NetworkConfig.logger.e("Got error : ${obj.toString()}");
          break;
      }
    });
  }

  Future<void> updateJobActivityLocalDb(String jobId, String jobDateTime) async {
    Jobs jobs = Jobs();
    jobs.jobId = jobId;
    JobReport jobReport = JobReport();
    String questionAnswer = "";

    if (jobStatus == "0") {
      BeforeWork beforeWork = new BeforeWork();
      jobs.jobStatus = "1";
      jobs.statusShow = "In-Progress";
      beforeWork.startDateTime = jobDateTime;
      beforeWork.comments = _commentController.text;
      if (finalSignaturePathBefore.length > 0)
        beforeWork.driverSignature = finalSignaturePathBefore;

      List<String> workImageList = List();
      if (finalImagePathBefore1.length > 0)
        workImageList.add(finalImagePathBefore1);

      if (finalImagePathBefore2.length > 0)
        workImageList.add(finalImagePathBefore2);

      if (finalImagePathBefore3.length > 0)
        workImageList.add(finalImagePathBefore3);

      beforeWork.workImage = workImageList;

      jobReport.beforeWork = beforeWork;

      jobs.jobTypeQuestionsString = null;
      jobs.jobTypeQuestionsString = null;
    } else {
      AfterWork afterWork = new AfterWork();
      jobs.jobStatus = "2";
      jobs.statusShow = "Completed";
      afterWork.endDateTime = jobDateTime;
      afterWork.comments = _commentController.text;
      if (finalSignaturePathAfter.length > 0)
        afterWork.customerSignature = finalSignaturePathAfter;

      List<String> workImageList = List();
      if (finalImagePathAfter1.length > 0)
        workImageList.add(finalImagePathAfter1);

      if (finalImagePathAfter2.length > 0)
        workImageList.add(finalImagePathAfter2);

      if (finalImagePathAfter3.length > 0)
        workImageList.add(finalImagePathAfter3);

      afterWork.workImage = workImageList;

      jobReport.afterWork = afterWork;

      if (job.jobTypeQuestions != null && job.jobTypeQuestions.length > 0 && answersMap.length > 0) {
        List<JobTypeQuestions> jobTypeQuestionsListLocal = List();
        questionAnswer = "[";
        for (int i = 0; i < job.jobTypeQuestions.length; i++) {
          JobTypeQuestions jobTypeQuestions = job.jobTypeQuestions[i];
          String questionType = jobTypeQuestions.type;
          questionAnswer = questionAnswer + "{" + "\"questionId\":\"" +
              jobTypeQuestions.questionId + "\", ";
          if (questionType == "text" && answersMap.containsKey("${jobTypeQuestions.type}${jobTypeQuestions
              .questionId}")) {
            if (i == (job.jobTypeQuestions.length - 1))
              questionAnswer = questionAnswer + "\"answer\":\"" +
                  answersMap["${jobTypeQuestions.type}${jobTypeQuestions
                      .questionId}"] + "\"}";
            else
              questionAnswer = questionAnswer + "\"answer\":\"" +
                  answersMap["${jobTypeQuestions.type}${jobTypeQuestions
                      .questionId}"] + "\"},";
            jobTypeQuestions.answer = answersMap["${jobTypeQuestions.type}${jobTypeQuestions
                .questionId}"];
          } else if (questionType == "radio" && answersMap.containsKey("${jobTypeQuestions.type}${jobTypeQuestions
              .questionId}")) {
            if (i == (job.jobTypeQuestions.length - 1))
              questionAnswer = questionAnswer + "\"answer\":\"" +
                  answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] + "\"}";
            else
              questionAnswer = questionAnswer + "\"answer\":\"" +
                  answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] + "\"},";

            jobTypeQuestions.answer = answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"];
          } else if (questionType == "checkbox" && answersMap.containsKey("${jobTypeQuestions.type}${jobTypeQuestions
              .questionId}")) {
            String checkBoxOptions = answersMap["${jobTypeQuestions
                .type}${jobTypeQuestions.questionId}"].replaceAll("[", "");
            checkBoxOptions = checkBoxOptions.replaceAll("]", "");
            checkBoxOptions = checkBoxOptions.replaceAll(", ", ",");
            if (i == (job.jobTypeQuestions.length - 1))
              questionAnswer = questionAnswer + "\"answer\":\"" + checkBoxOptions + "\"}";
            else
              questionAnswer = questionAnswer + "\"answer\":\"" + checkBoxOptions + "\"},";

            jobTypeQuestions.answer = checkBoxOptions;
          }
          jobTypeQuestionsListLocal.add(jobTypeQuestions);
        }
        questionAnswer = questionAnswer + "]";

        print(questionAnswer);
        jobs.jobTypeQuestionAnswers = questionAnswer;
        jobs.jobTypeQuestionsString = jobTypeQuestionsListLocal.toString();
      }
    }

    jobs.jobReport = jobReport;
    jobs.upd = jobDateTime;
    jobs.updatedStatus = "No";
    jobs.imageUpdatedStatus = "Yes";

    /*Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) =>
                  LoadingScreen(globalKey: _globalKey,
                      title: "Please wait, we are sending data to server.")
          )
      );
    });*/

    DatabaseHelper databaseHelper = DatabaseHelper();
    //print("job offline mode : ${jobs.toString()}");
    databaseHelper.updateJobActivity(jobs).then((updateResult){
      if (updateResult == 1) {
        if (job.jobStatus == "0") {
          AppSharedPreferences.setIsInProgress(true);

          Navigator.of(context).pop("start");
        } else if(job.jobStatus == "1") {
          AppSharedPreferences.setIsInProgress(false);
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => JobCompletedScreen()));
        }
      } else {
        showErrorDialog(context, "Error", "Does not update data.");
      }
    });
  }

  void _submitStartJob() async {
    print("stated");
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet){
      if(finalImagePathBefore1.length > 0 && finalImagePathBefore2.length > 0 && finalImagePathBefore3.length > 0 && finalSignaturePathBefore.length > 0 && _commentController.text != null && _commentController.text.length > 0) {
        updateJobActivity("start");
      } else{
        showErrorDialog(context, "Error", "Please select all required field.");
      }
    } else {
      if(finalImagePathBefore1.length > 0 && finalImagePathBefore2.length > 0 && finalImagePathBefore3.length > 0 && finalSignaturePathBefore.length > 0 && _commentController.text != null && _commentController.text.length > 0) {
        DateTime now = DateTime.now();
        String nowDate = DateFormat('yyyy-MM-dd hh:mm:ss a').format(now);
        //print("current date : ${nowDate.toString()}");
        updateJobActivityLocalDb(widget.id, nowDate);
      } else{
        showErrorDialog(context, "Error", "Please select all required field.");
      }
      //showErrorDialog(context, "Error", "No internet connection!");
    }
      //imageUploadRequest();
  }

  void _submitEndJob() async{
    print("end job");
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet){
      if(finalImagePathAfter1.length > 0 && finalImagePathAfter2.length > 0 && finalImagePathAfter3.length > 0 && finalSignaturePathAfter.length > 0 && _customerCommentController.text != null && _customerCommentController.text.length > 0) {
        updateJobActivity("end");
      } else{
        showErrorDialog(context, "Error", "Please select all required field.");
      }
    } else {
      if(finalImagePathAfter1.length > 0 && finalImagePathAfter2.length > 0 && finalImagePathAfter3.length > 0 && finalSignaturePathAfter.length > 0 && _customerCommentController.text != null && _customerCommentController.text.length > 0) {
        DateTime now = DateTime.now();
        String nowDate = DateFormat('yyyy-MM-dd hh:mm:ss a').format(now);
        //print("current date : ${nowDate.toString()}");
        updateJobActivityLocalDb(widget.id, nowDate);
      } else{
        showErrorDialog(context, "Error", "Please select all required field.");
      }
      //showErrorDialog(context, "Error", "No internet connection!");
    }
  }

  @override
  void dispose() {
    _commentController.dispose();
    _customerCommentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //print("before job : ${beforeWork?.workImage} after job : ${afterWork?.workImage}");
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: Color(0xFFECECEC),
        bottomNavigationBar: (job?.jobStatus != "2") ? RaisedButton(
          onPressed: (job?.jobStatus == "0" && isStartButtonEnabled) ? () => _submitStartJob() : (job?.jobStatus == "1" && isEndButtonEnabled) ? () =>  _submitEndJob() : null,
          color: (job?.jobStatus == "0" && isStartButtonEnabled) ? Color(0xFF62B466) : (job?.jobStatus == "1" && isEndButtonEnabled) ? Color(0xFFE2332D) : Color(0xFFBBBBBB),
          padding: EdgeInsets.symmetric(vertical: 15.0),
          disabledColor: Color(0xFFBBBBBB),
          child: Text(
            buttonText,
            style: TextStyle(
              color: Colors.white
            ),
          ),
        ) : SizedBox(),
        body: Container(
          constraints: BoxConstraints(
            minHeight: 100 * SizeConfig.heightMultiplier
          ),
          child: Column(
            children: [
              Container(
                color: AppTheme.primaryColor,
                child: Flex(
                  direction: Axis.vertical,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      padding: EdgeInsets.only(right:  2 * SizeConfig.heightMultiplier, left:  2 * SizeConfig.heightMultiplier),
                      height: 56,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          InkWell(
                           onTap:() {
                             Navigator.of(context).pop();
                           },
                            child: Image.asset(
                              "${Constants.IMAGE_PATH}back.png",
                              height: 18,
                              width: 18,
                            ),
                          ),
                          SizedBox(
                            width: 8 * SizeConfig.widthMultiplier,
                          ),
                          Text(
                            "View Job",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 2.5 * SizeConfig.textMultiplier
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  height: (100 * SizeConfig.heightMultiplier) - 84,
                  color: Color(0xFFECECEC),
                  padding: EdgeInsets.only(left: 5.0, right: 5.0,  top: 15.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            "JOB DETAILS",
                            style: TextStyle(
                              color: Color(0xFF393939),
                              fontSize: 2.0 * SizeConfig.textMultiplier
                            ),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        (job != null) ? _topCardContainer(job) : SizedBox(),
                        SizedBox(height: 15.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            "ADDRESS",
                            style: TextStyle(
                                color: Color(0xFF393939),
                                fontSize: 2.0 * SizeConfig.textMultiplier
                            ),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        (job != null) ? _addressCardContainer(job) : SizedBox(),
                        (job != null && job.jobStatus != null && job.jobStatus != "0") ? Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(height: 15.0),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                "QUESTIONS",
                                style: TextStyle(
                                    color: Color(0xFF393939),
                                    fontSize: 2.0 * SizeConfig.textMultiplier
                                ),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            (job?.jobTypeQuestions != null && job?.jobTypeQuestions.length > 0) ? _questionMainCardContainer(job) : _noQuestionCardContainer(job)
                          ],
                        ) : SizedBox(),
                        SizedBox(height: 15.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            "BEFORE WORK",
                            style: TextStyle(
                                color: Color(0xFF393939),
                                fontSize: 2.0 * SizeConfig.textMultiplier
                            ),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        (job != null) ? _beforeCardContainer(job) : SizedBox(),
                        (job != null && job.jobStatus != null && job?.jobStatus != "0") ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 15.0),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Text(
                                "AFTER WORK",
                                style: TextStyle(
                                    color: Color(0xFF393939),
                                    fontSize: 2.0 * SizeConfig.textMultiplier
                                ),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            (job != null) ? _afterCardContainer(job) : SizedBox(),
                          ],
                        ) : SizedBox()
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _questionMainCardContainer(Jobs job){
    //print("length of jobTypequestions : ${job.jobTypeQuestions.length}");
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        for(int i = 0; i < job.jobTypeQuestions.length; i++)
          _questionAnswerContainer(job.jobTypeQuestions[i], i)
      ],
    );
  }

  Widget _questionAnswerContainer(JobTypeQuestions jobTypeQuestions, int index){
    return Card(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        color: Colors.white,
        elevation: 5,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.transparent, width: 0),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                "${index+1}. ${jobTypeQuestions.question}",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 1.9 * SizeConfig.textMultiplier
                ),
              ),
              SizedBox(height: 10),
              (jobTypeQuestions.type == "text") ? _textTypeQuestionContainer(jobTypeQuestions) : ((jobTypeQuestions.type == "radio") ? _radioTypeQuestionContainer(jobTypeQuestions) : (jobTypeQuestions.type == "checkbox") ? _checkBoxTypeQuestionContainer(jobTypeQuestions) : SizedBox())
            ],
          ),
        )
    );
  }
  String _currText = '';
  //List<bool> _isChecked ;//= [false,false];
  Widget _checkBoxTypeQuestionContainer(JobTypeQuestions jobTypeQuestions){
    var optionsJson = jsonDecode(jobTypeQuestions.options);
    List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;
    return Column(
      children: optionsArray.map((data){
        int index = optionsArray.indexOf(data);
        return Row(
          children: [
            Checkbox(
              value: _checkList[index],
              //activeColor: Colors.green,
              visualDensity: VisualDensity(horizontal: -4.0, vertical: -4.0),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onChanged:(bool newValue){
                setState(() {
                  _checkList[index] = newValue;
                  if (newValue == true) {
                    _currText = data;

                    if(cbResult != null)
                      cbResult.add(data);
                  } else{
                    if(cbResult != null && cbResult.length > 0){
                      cbResult.remove(data);
                    }
                    /*if(cbResult.contains(",$data")){
                      cbResult.replaceAll(",$data", "");
                    } else if(cbResult.contains(data)){
                      cbResult.replaceAll(data, "");
                    }*/
                  }

                  if(cbResult.length > 0)
                    answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] = cbResult.toString();
                  else
                    answersMap.remove("${jobTypeQuestions.type}${jobTypeQuestions.questionId}");

                  //print("answersMap : $answersMap");

                  //_checkEndButtonEnabled();
                });
              }
            ),
            Text(
              data,
              style: TextStyle(
                fontSize: 1.9 * SizeConfig.textMultiplier
              ),
            )
          ],
        );
      }).toList(),
    );
  }

  // Declare this variable
  String selectedRadioTile;

  setSelectedRadioTile(String val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  Widget _radioTypeQuestionContainer(JobTypeQuestions jobTypeQuestions){
    var optionsJson = jsonDecode(jobTypeQuestions.options);
    List<String> optionsArray = optionsJson != null ? List.from(optionsJson) : null;

    //print(optionsArray);
    return Column(
      children: /*[
        for(int i = 0; i < optionsArray.length; i++)
          _radioOption(optionsArray[i])
      ]*/optionsArray.map((data){
        int index = optionsArray.indexOf(data);
        return Row(
          children: [
            Radio(
              value: data,
              groupValue: selectedRadioTile,
              onChanged: (String val) {
                setState(() {
                  selectedRadioTile = val;
                  answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] = val;
                });
                //_checkEndButtonEnabled();
              },
              visualDensity: VisualDensity(horizontal: -4.0, vertical: -4.0),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            ),
            Text(
              data,
              style: TextStyle(
                fontSize: 1.9 * SizeConfig.textMultiplier
              ),
            ),
          ],
        );
      }).toList(),
    );
  }
  
  Widget _radioOption(String option){
    return  Row(
      children: [
        Radio(
          value: option,
          groupValue: selectedRadioTile,
          onChanged: setSelectedRadioTile,
          visualDensity: VisualDensity(horizontal: -4.0, vertical: -4.0),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        ),
        Text(option),
      ],
    );
  }

  Widget _textTypeQuestionContainer(JobTypeQuestions jobTypeQuestions){
    return TextFormField(
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF9F9F9F)),
                borderRadius: BorderRadius.all(Radius.circular(0.0))
            ),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF9F9F9F)),
                borderRadius: BorderRadius.all(Radius.circular(0.0))
            ),
            hintText: 'Type here',
            hintStyle: TextStyle(
                fontWeight: FontWeight.w600,
                color: Color(0xFF9F9F9F)
            ),
            fillColor: Color(0xFFF7F7F7),
            filled: true,
            contentPadding: EdgeInsets.all(15.0)
        ),
        style: TextStyle(
            fontSize: 1.8 * SizeConfig.textMultiplier
        ),
        maxLines: 2,
        textInputAction: TextInputAction.newline,
        onChanged: (value){
          setState(() {
            if(value.length > 0){
              answersMap["${jobTypeQuestions.type}${jobTypeQuestions.questionId}"] = value;
            }else{
              answersMap.remove("${jobTypeQuestions.type}${jobTypeQuestions.questionId}");
            }
          });
          //_checkEndButtonEnabled();
        }
    );

  }

  Widget _noQuestionCardContainer(Jobs job){
    return Card(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        color: Colors.white,
        elevation: 5,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.transparent, width: 0),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
          child: Center(
            child: Text(
              "No Questions",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: 2.5 * SizeConfig.textMultiplier
              ),
            ),
          ),
        )
    );
  }

  Widget _beforeCardContainer(Jobs Job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                "Comment and Driver Signature",
                style: TextStyle(
                  fontSize: 1.8 * SizeConfig.textMultiplier,
                  color: Color(0xFF244764),
                  fontWeight: FontWeight.normal
                ),
              ),
            ),
            SizedBox(height: 15.0),
            _imageRowContainerBefore(job),
            SizedBox(height: 15.0),
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: _commentController,
                    enabled: isCommentEnabled,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFB3B8B7))
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFB3B8B7))
                        ),
                        hintText: 'Comment',
                        hintStyle: TextStyle(
                          fontWeight: FontWeight.w600
                        ),
                      contentPadding: EdgeInsets.all(15.0)
                    ),
                    style: TextStyle(
                      fontSize: 1.8 * SizeConfig.textMultiplier
                    ),
                    maxLines: 3,
                    textInputAction: TextInputAction.newline,
                    onChanged: (value){
                      _checkButtonEnabled();
                    }
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  width: 38 * SizeConfig.widthMultiplier,
                  height: 75.0,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xFFB3B8B7)),
                      borderRadius: BorderRadius.all(Radius.circular(4.0))
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: GestureDetector(
                      onTap: ((finalSignaturePathBefore.length > 0) || (beforeWork?.driverSignature != null && beforeWork?.driverSignature.length > 0) ) ? null : () async {
                        String result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SignatureScreen()
                          ),
                        );
                        setState(() {
                          if(result != null && result.length > 0){
                            finalSignaturePathBefore = result;
                            print("result : $result");
                            _checkButtonEnabled();
                          }
                        });
                      },
                      child: beforeSignatureImage(),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ) ;
  }

  Future<void> _pickImageDialog(BuildContext context, OnPickImageCallback onPick, int position, String action) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        //useSafeArea: false,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10)),
            content: Container(
              child: Column(
                mainAxisSize:MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 10),
                  Text(
                    'SELECT IMAGE',
                    style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.bold,
                      fontSize: 2.5 * SizeConfig.textMultiplier
                    ),
                  ),
                  SizedBox(height: 10),
                  Divider(color: Color(0x50CCCCCC), thickness: 1),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                        onTap: () async{
                          Navigator.of(context).pop();
                          _pickedFile = await picker.getImage(source: ImageSource.gallery, maxWidth: 768, maxHeight: 1024, imageQuality: 90);
                          setState(() {
                            _pickedFile = _pickedFile;
                            _imageFile = File(_pickedFile.path);
                            if(action == "before"){
                              if(position == 0) {
                                finalImagePathBefore1 = _pickedFile.path;
                              } else if(position == 1){
                                finalImagePathBefore2 = _pickedFile.path;
                              } else if(position == 2){
                                finalImagePathBefore3 = _pickedFile.path;
                              }
                              _checkButtonEnabled();
                            } else if(action == "after"){
                              if(position == 0) {
                                finalImagePathAfter1 = _pickedFile.path;
                              } else if(position == 1){
                                finalImagePathAfter2 = _pickedFile.path;
                              } else if(position == 2){
                                finalImagePathAfter3 = _pickedFile.path;
                              }
                              _checkEndButtonEnabled();
                            }
                          });
                          //print("Gallery image path : ${_imageFile.path}");
                        },
                        child: Text(
                          "Gallery",
                          style: TextStyle(
                            fontSize: 2.5 * SizeConfig.textMultiplier,
                            color: Color(0xff393939)
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          Navigator.of(context).pop();
                          _pickedFile = await picker.getImage(source: ImageSource.camera, maxWidth: 768, maxHeight: 1024, imageQuality: 90);
                          setState(() {
                            _pickedFile = _pickedFile;
                            _imageFile = File(_pickedFile.path);

                            if(action == "before"){
                              if(position == 0) {
                                finalImagePathBefore1 = _pickedFile.path;
                              } else if(position == 1){
                                finalImagePathBefore2 = _pickedFile.path;
                              } else if(position == 2){
                                finalImagePathBefore3 = _pickedFile.path;
                              }
                              _checkButtonEnabled();
                            } else if(action == "after"){
                              if(position == 0) {
                                finalImagePathAfter1 = _pickedFile.path;
                              } else if(position == 1){
                                finalImagePathAfter2 = _pickedFile.path;
                              } else if(position == 2){
                                finalImagePathAfter3 = _pickedFile.path;
                              }

                              _checkEndButtonEnabled();
                            }
                          });
                          //print("Camera image path : ${_imageFile.path}");
                        },
                        child: Text(
                          "Take Picture",
                          style: TextStyle(
                            fontSize: 2.5 * SizeConfig.textMultiplier,
                            color: Color(0xff393939)
                          ),
                        )
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  Divider(color: Color(0x50CCCCCC), thickness: 1),
                  FlatButton(
                    child: const Text(
                      'Cancel',
                      style: TextStyle(
                          color: Color(0xff393939)
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  Widget _imageRowContainerBefore(Jobs job){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () => ((finalImagePathBefore1.length > 0) || (beforeWork?.workImage != null && beforeWork?.workImage.length > 0)) ? null : (isProgress) ? showErrorDialog(context, "Error", "You can not start more than one job at a time.") : imageEventCall(0),
                child: Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: beforeImage(0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
              ),
              (finalImagePathBefore1 != null && finalImagePathBefore1.length > 0) ? Positioned(
                top: 0,
                right: 0,
                child: GestureDetector(
                  onTap: (){
                    //print('delete image from List');
                    setState(() {
                      finalImagePathBefore1 = "";
                      _checkButtonEnabled();
                    });
                  },
                  child:Image.asset(
                    "${Constants.IMAGE_PATH}delete.png",
                    height: 20,
                    width: 20,
                    fit: BoxFit.fill,
                  ),
                ),
              ) : SizedBox(),
            ],
          ),
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () => ((finalImagePathBefore2.length > 0) || (beforeWork?.workImage != null && beforeWork?.workImage.length > 0)) ? null : (isProgress) ? showErrorDialog(context, "Error", "You can not start more than one job at a time.") : imageEventCall(1),
                child: Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: beforeImage(1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
              ),
              (finalImagePathBefore2 != null && finalImagePathBefore2.length > 0) ? Positioned(
                top: 0,
                right: 0,
                child: GestureDetector(
                  onTap: (){
                    //print('delete image from List');
                    setState(() {
                      finalImagePathBefore2 = "";
                      _checkButtonEnabled();
                    });
                  },
                  child:Image.asset(
                    "${Constants.IMAGE_PATH}delete.png",
                    height: 20,
                    width: 20,
                    fit: BoxFit.fill,
                  ),
                ),
              ) : SizedBox(),
            ],
          ),
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () => ((finalImagePathBefore3.length > 0) || (beforeWork?.workImage != null && beforeWork?.workImage.length > 0)) ? null : (isProgress) ? showErrorDialog(context, "Error", "You can not start more than one job at a time.") : imageEventCall(2),
                child: Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: beforeImage(2),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
              ),
              (finalImagePathBefore3 != null && finalImagePathBefore3.length > 0) ? Positioned(
                top: 0,
                right: 0,
                child: GestureDetector(
                  onTap: (){
                    //print('delete image from List');
                    setState(() {
                      finalImagePathBefore3 = "";
                      _checkButtonEnabled();
                    });
                  },
                  child:Image.asset(
                    "${Constants.IMAGE_PATH}delete.png",
                    height: 20,
                    width: 20,
                    fit: BoxFit.fill,
                  ),
                ),
              ) : SizedBox(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _imageRowContainerAfter(Jobs job){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () => ((finalImagePathAfter1.length > 0) || (workImageAfter != null && workImageAfter.length > 0)) ? null : imageEventCallAfter(0),
                child: Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: afterImage(0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
              ),
              (finalImagePathAfter1 != null && finalImagePathAfter1.length > 0) ? Positioned(
                top: 0,
                right: 0,
                child: GestureDetector(
                  onTap: (){
                    //print('delete image from List');
                    setState(() {
                      finalImagePathAfter1 = "";
                      _checkEndButtonEnabled();
                    });
                  },
                  child:Image.asset(
                    "${Constants.IMAGE_PATH}delete.png",
                    height: 20,
                    width: 20,
                    fit: BoxFit.fill,
                  ),
                ),
              ) : SizedBox(),
            ],
          ),
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () => ((finalImagePathAfter2.length > 0) || (workImageAfter != null && workImageAfter.length > 0)) ? null : imageEventCallAfter(1),
                child: Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: afterImage(1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
              ),
              (finalImagePathAfter2 != null && finalImagePathAfter2.length > 0) ? Positioned(
                top: 0,
                right: 0,
                child: GestureDetector(
                  onTap: (){
                    //print('delete image from List');
                    setState(() {
                      finalImagePathAfter2 = "";
                      _checkEndButtonEnabled();
                    });
                  },
                  child:Image.asset(
                    "${Constants.IMAGE_PATH}delete.png",
                    height: 20,
                    width: 20,
                    fit: BoxFit.fill,
                  ),
                ),
              ) : SizedBox(),
            ],
          ),
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () => ((finalImagePathAfter3.length > 0) || (workImageAfter != null && workImageAfter.length > 0)) ? null : imageEventCallAfter(2),
                child: Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: afterImage(2),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
              ),
              (finalImagePathAfter3 != null && finalImagePathAfter3.length > 0) ? Positioned(
                top: 0,
                right: 0,
                child: GestureDetector(
                  onTap: (){
                    //print('delete image from List');
                    setState(() {
                      finalImagePathAfter3 = "";
                      _checkEndButtonEnabled();
                    });
                  },
                  child:Image.asset(
                    "${Constants.IMAGE_PATH}delete.png",
                    height: 20,
                    width: 20,
                    fit: BoxFit.fill,
                  ),
                ),
              ) : SizedBox(),
            ],
          ),
        ],
      ),
    );
  }

  void imageEventCall(int position){
    _pickImageDialog(context, (double maxWidth, double maxHeight, int quality) async {
      try {
        _pickedFile = await picker.getImage(
            source: ImageSource.camera,
            maxWidth: maxWidth,
            maxHeight: maxHeight,
            imageQuality: quality
        );
        print(_pickedFile.path);
        _imageFile = File(_pickedFile.path);
      } catch (e) {
        _pickImageError = e;
        print("pick image error : ${_pickImageError}");
      }
    }, position, "before");
  }

  void imageEventCallAfter(int position){
    _pickImageDialog(context, (double maxWidth, double maxHeight, int quality) async {
      try {
        _pickedFile = await picker.getImage(
            source: ImageSource.camera,
            maxWidth: maxWidth,
            maxHeight: maxHeight,
            imageQuality: quality
        );
        print(_pickedFile.path);
        _imageFile = File(_pickedFile.path);
      } catch (e) {
        _pickImageError = e;
        print("pick image error : ${_pickImageError}");
      }
    }, position, "after");
  }

  Widget _afterCardContainer(Jobs Job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                "Comment and Customer Signature",
                style: TextStyle(
                    fontSize: 1.8 * SizeConfig.textMultiplier,
                    color: Color(0xFF244764),
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            SizedBox(height: 15.0),
            _imageRowContainerAfter(job),
            SizedBox(height: 15.0),
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                      controller: _customerCommentController,
                      enabled: isCustomerCommentEnabled,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFB3B8B7))
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFB3B8B7))
                          ),
                          hintText: 'Comment',
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.w600
                          ),
                          contentPadding: EdgeInsets.all(15.0)
                      ),
                      style: TextStyle(
                          fontSize: 1.8 * SizeConfig.textMultiplier
                      ),
                      maxLines: 3,
                      textInputAction: TextInputAction.newline,
                      onChanged: (value){
                        _checkEndButtonEnabled();
                      }
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  width: 38 * SizeConfig.widthMultiplier,
                  height: 75.0,
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0xFFB3B8B7)),
                      borderRadius: BorderRadius.all(Radius.circular(4.0))
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: GestureDetector(
                      onTap: ((finalSignaturePathAfter.length > 0) || (afterWork?.customerSignature != null && afterWork?.customerSignature.length > 0) ) ? null : () async {
                        String result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SignatureScreen()
                          ),
                        );

                        setState(() {
                          if(result != null && result.length > 0){
                            finalSignaturePathAfter = result;
                            _checkEndButtonEnabled();
                            print("result : $result");
                          }
                        });
                      },
                      child: afterSignatureImage(),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ) ;
  }


  Widget _addressCardContainer(Jobs job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0, top:5.0, bottom: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                (job.address != null) ? job.address : "NA",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: AppTheme.colorTextJobDetail,
                  fontSize: 1.8 * SizeConfig.textMultiplier,
                  height: 1.3
                ),
              ),
            ),
            Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                        child: Text(
                          "${((job.street != null) || (job.street2 != null)) ? job.street +" "+ job.street2 : "NA"}",
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: AppTheme.colorTextJobDetail,
                            fontSize: 1.8 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                        child: Text(
                          (job.country != null) ? job.country : "NA",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: AppTheme.colorTextJobDetail,
                              fontSize: 1.8 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                    ],
                  ),
                ),
                SizedBox(width: 20.0),
                Container(
                  width: 36 * SizeConfig.widthMultiplier,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                        child: Text(
                          "${(job.city != null) ? job.city : "NA"}${(job.state != null) ? ", " + job.state : ""}",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: AppTheme.colorTextJobDetail,
                              fontSize: 1.8 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                            child: Text(
                              (job.zip != null) ? job.zip : "NA",
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: AppTheme.colorTextJobDetail,
                                  fontSize: 1.8 * SizeConfig.textMultiplier
                              ),
                            ),
                          ),
                          Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                height: 5 * SizeConfig.heightMultiplier,
                width: 38 * SizeConfig.widthMultiplier,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MapViewScreen(job: job, isLocalData: isLocalData)
                      ),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: AppTheme.primaryColor, //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                        style: BorderStyle.solid,
                        width: 1.0,
                      ),
                      color: AppTheme.primaryColor, //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text(
                            "Map View",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 1.8 * SizeConfig.textMultiplier,
                              fontWeight: FontWeight.normal,
                              letterSpacing: 1,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _topCardContainer(Jobs job){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        constraints: BoxConstraints(maxHeight: 15.7 * SizeConfig.heightMultiplier),
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    (job.jobName != null) ? job.jobName : "NA",
                    style: TextStyle(
                      fontSize: 2.4 * SizeConfig.textMultiplier,
                      fontWeight: FontWeight.w600
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 7.0),
                  Text(
                    (job.customerName != null) ? job.customerName : "NA",
                    style: TextStyle(
                        color: AppTheme.colorTextGrey,
                        fontSize: 1.8 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 4.5),
                  Text(
                    (job.jobType != null) ? job.jobType : "NA",
                    style: TextStyle(
                        color: AppTheme.colorTextGrey,
                        fontSize: 1.8 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 4.5),
                  Text(
                    (job.driverName != null) ? job.driverName : "NA",
                    style: TextStyle(
                        color: AppTheme.colorTextGrey,
                        fontSize: 1.8 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(height: 3.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "${Constants.IMAGE_PATH}calendar.png",
                      height: 12.0,
                      width: 12.0,
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      "${(job.startDate != null) ? DataTimeUtility.convertTimeForJob(job.startDate) : "NA"} ${(job.startTime!=null) ? job.startTime : "NA"}",
                      style: TextStyle(
                        fontSize: 1.6 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal,
                        color: AppTheme.colorTextGrey,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 7 * SizeConfig.heightMultiplier),
                Text(
                  job.statusShow,
                  style: TextStyle(
                      fontSize: 1.9 * SizeConfig.textMultiplier,
                      color: (job.jobStatus == "0") ? Color(0xFF244764) : ((job.jobStatus == "1") ? Color(0xFFD5804D) : Color(0xFF244764)),
                      fontWeight: FontWeight.normal
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

typedef void OnPickImageCallback(double maxWidth, double maxHeight, int quality);
