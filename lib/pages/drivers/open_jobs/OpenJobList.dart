
import 'dart:io';

import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/models/AfterWork.dart';
import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/models/BeforeWork.dart';
import 'package:worktraq/models/JobReport.dart';
import 'package:worktraq/models/JobResponse.dart';
import 'package:worktraq/models/Jobs.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/pages/drivers/JobDetailScreen.dart';
import 'package:worktraq/services/DriverTrackingHelper.dart';
import 'package:worktraq/services/JobTrackingHelper.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/CustomDialog.dart';
import 'package:worktraq/utils/ImageUtils.dart';
import 'package:worktraq/utils/ResponseHandler.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:worktraq/widgets/LoadingListPage.dart';
import 'package:worktraq/widgets/LoadingScreen.dart';
import 'package:file_utils/file_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:worktraq/utils/DateTimeUtility.dart';
import 'package:dio/dio.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:image/image.dart' as IM;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class OpenJobList extends StatefulWidget {
  @override
  _OpenJobListState createState() => _OpenJobListState();
}

class _OpenJobListState extends State<OpenJobList>{
  List<Jobs> openJobList;
  List<Jobs> newOpenJobList = List();
  final GlobalKey _globalKey = GlobalKey<State>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  DatabaseHelper databaseHelper = DatabaseHelper();
  BuildContext context;
  bool isLoadingList = true;

  @override
  void didChangeDependencies() {
    getOpenJobsService();
    super.didChangeDependencies();
  }

  Future<void> getOpenJobsService() async {
    bool isInternet = await ConnectivityUtility().isInternet();
    if(isInternet) {
      bool isFirstTimeLaunch = await AppSharedPreferences.isFirstTimeDataSaveInLocal();

      if(isFirstTimeLaunch == null)
        isFirstTimeLaunch = false;

      setState(() {
        isLoadingList = true;
      });

      if(isFirstTimeLaunch){
        Future.delayed(Duration.zero, () {
          Navigator.of(context).push(
              PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
              )
          );
        });
      } else{
        Future.delayed(Duration.zero, () {
          Navigator.of(context).push(
              PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey, title: "Please wait...")
              )
          );
        });
      }

      String authToken = await AppSharedPreferences.getAuthToken();
      print("authToken $authToken");
      bool isProgress = await AppSharedPreferences.isInProgress();
      if(isProgress == null)
        isProgress = false;

      NetworkConfig.restClientInstance().getAllJobs(authToken).then((result){
        if(result.status == "success") {
          //print("isFirstTimeLaunch : $isFirstTimeLaunch");
          Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();

          JobResponse jobResponse = result;
          if (!mounted) return;
          setState(() {
            openJobList = jobResponse.jobs;
            isLoadingList = false;
          });

          //print("length : ${openJobList.length} ${openJobList.toString()}");

          if(!isFirstTimeLaunch){
            Future.delayed(Duration(milliseconds: openJobList.length * 3000), (){
              if(!mounted) return;
                Navigator.of(_globalKey?.currentContext, rootNavigator: true).pop();
            });
          }

          if(openJobList != null && openJobList.length > 0){
            newOpenJobList.clear();
            for ( int i = 0; i < openJobList.length; i++ ){
              Jobs job = openJobList[i];
              if (isInternet){
                job.updatedStatus = "Yes";
              } else {
                job.updatedStatus = "No";
              }

              if(job.jobStatus == "1"){
                if (!isProgress){
                  AppSharedPreferences.setIsInProgress(true);
                }
              }

              job.jobTypeQuestionsString = job.jobTypeQuestions.toString();
              saveLocalDb(job);
              int finalI = i;

               Future.delayed(Duration(milliseconds: 2000), (){
                databaseHelper.isLocalImageUpdated(job.jobId).then((isLocalImageUpdated){
                  //print("isLocalImageUpdated : $isLocalImageUpdated");
                  if(isLocalImageUpdated){
                    JobReport jobReport = job.jobReport;
                    if (jobReport != null){
                      BeforeWork beforeWork = jobReport.beforeWork;
                      if(beforeWork != null){
                        if(beforeWork.driverSignature != null){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_DRIVER_SIGNATURE, beforeWork.driverSignature);
                        }
                        List<String> workImages = beforeWork.workImage;
                        if(workImages != null && workImages.length > 0){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_IMAGE1, workImages[0]);
                        }

                        if(workImages != null && workImages.length > 1){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_IMAGE2, workImages[1]);
                        }

                        if(workImages != null && workImages.length > 2){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_BEFORE_WORK_IMAGE3, workImages[2]);
                        }
                      }

                      AfterWork afterWork = jobReport.afterWork;
                      if(afterWork != null){
                        if(afterWork.customerSignature != null){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_CUSTOMER_SIGNATURE, afterWork.customerSignature);
                        }
                        List<String> workImages = afterWork.workImage;
                        if(workImages != null && workImages.length > 0){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_IMAGE1, workImages[0]);
                        }

                        if(workImages != null && workImages.length > 1){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_IMAGE2, workImages[1]);
                        }

                        if(workImages != null && workImages.length > 2){
                          _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_AFTER_WORK_IMAGE3, workImages[2]);
                        }
                      }
                    }

                    if( finalI == openJobList.length-1 )
                      AppSharedPreferences.setFirstTimeDataSaveInLocal(true);

                    if (job.geoFencingUrl!= null) {
                      _downloadFileAndSavePathDb(job.jobId, job.jobStatus, DatabaseHelper.JOB_GEO_FENCING_URL, job.geoFencingUrl);
                    }
                  }
                });
              });

              if (job.jobStatus == "0" || job.jobStatus == "1") {
                newOpenJobList.add(job);
              }
            }
          }
        }else{
          if(!mounted) return;
            Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();

          setState(() {
            isLoadingList = false;
          });

          showErrorDialog(context,  'Error', result.message);
          //print("result nkp : status - ${result.status} Message - ${result.message}");
        }
        //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
      }).catchError((Object obj){
        if(!mounted) return;
          Navigator.of(_globalKey.currentContext, rootNavigator: true).pop();

        setState(() {
          isLoadingList = false;
        });
        switch (obj.runtimeType) {
          case DioError:
            final res = (obj as DioError).response;
            if(res?.data != null) {
              AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res?.data);

              print("${res?.data.toString()} ");
              if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                ResponseHandler.handleResponse(context, authFailedResponse);
              }
            } else {
              if(res != null){
                NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
                showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
              }
            }
            break;
          default:
            NetworkConfig.logger.e("Got error : ${obj.toString()}");
            break;
        }
      });
    }  else {
      getOpenJobListFromLocalDb();
    }
  }

  void _downloadFileAndSavePathDb(String jobId, String jobStatus, String whereImageName, String imageUrl) async {
    downloadImage(imageUrl).then((localImagePath){
      //print("_downloadFileAndSavePathDb : $localImagePath");
      if(localImagePath != null){
        databaseHelper.isUpdateRequiredFilePath(jobId, jobStatus, whereImageName, localImagePath).then((isUpdateRequiredFilePath){
          if(!isUpdateRequiredFilePath){
            databaseHelper.updateImagePath(jobId, whereImageName, localImagePath);
          }
        });
      }
    });
  }

  void getOpenJobListFromLocalDb() {
    JobTrackingHelper().onStop();
    DriverTrackingHelper().onStop();
    setState(() {
      isLoadingList = true;
    });
    Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey,)
          )
      );
    });

    Future.delayed(Duration(milliseconds: 2000), () async{
      if(openJobList != null)
        openJobList.clear();

      DatabaseHelper databaseHelper = DatabaseHelper();
      String userId = await AppSharedPreferences.getId();
      print("user id : $userId");
      databaseHelper.getOpenJobsListFromLocal(userId).then((jobList) {
        if(!mounted) return;

        setState(() {
          openJobList = jobList;
          isLoadingList = false;
          //print("local openJobList : ${openJobList.toString()}");
        });
      });

      Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      this.context = context;
    });
    return Scaffold(
      backgroundColor: Colors.transparent,
      key: _scaffoldKey,
      body: isLoadingList ? LoadingListPage() : _openProjectList(),
    );
  }

  Widget _openProjectList(){
    return (openJobList != null && openJobList.length > 0) ? ListView.builder(
      itemCount: openJobList != null ? openJobList.length : 0,
      padding: EdgeInsets.symmetric(vertical: 1 * SizeConfig.heightMultiplier, horizontal: 10.0),
      itemBuilder: (context, index) {
        return _listCard(openJobList[index]);
      },
    ) : Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: const EdgeInsets.only(top: 15.0),
        child: Text(
          "No Record",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 22.0,
            color: AppTheme.colorText,
            fontWeight: FontWeight.w200
          ),
        ),
      ),
    );
  }

  Widget _listCard(Jobs job){
    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => JobDetailScreen(id: job?.jobId)
          ),
        ).then((value){
          if(value == "start")
            getOpenJobsService();

          if (!mounted) return;
            setState(() {});
        });
      },
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        color: Colors.white,
        elevation: 5,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.transparent, width: 0),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          constraints: BoxConstraints(maxHeight: 15.7 * SizeConfig.heightMultiplier),
          padding: EdgeInsets.symmetric(horizontal: 2.43 * SizeConfig.widthMultiplier, vertical: 1.463 * SizeConfig.heightMultiplier),
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(
                color: (job.jobStatus == "0") ? Color(0xFF244764) : ((job.jobStatus == "1") ? Color(0xFFD5804D) : Color(0xFF244764)),
                width: 3.0,
              ),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      (job.jobName != null) ? job.jobName : "NA",
                      style: TextStyle(
                        fontSize: 2.4 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.w600
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 0.9 * SizeConfig.heightMultiplier),
                    Text(
                      (job.customerName != null) ? job.customerName : "NA",
                      style: TextStyle(
                        color: AppTheme.colorTextGrey,
                        fontSize: 1.7 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.normal
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                    Text(
                      (job.jobType != null) ? job.jobType : "NA",
                      style: TextStyle(
                          color: AppTheme.colorTextGrey,
                          fontSize: 1.7 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 0.65 * SizeConfig.heightMultiplier),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          "${Constants.IMAGE_PATH}distance.png",
                          height: 4 * SizeConfig.imageSizeMultiplier,
                          width: 4 * SizeConfig.imageSizeMultiplier,
                        ),
                        SizedBox(width: 1.9 * SizeConfig.widthMultiplier),
                        Text(
                          "${(job.distance != null) ? job.distance : "0.0"} Miles",
                          style: TextStyle(
                            color: AppTheme.colorTextGrey,
                            fontSize: 1.9 * SizeConfig.textMultiplier,
                            fontWeight: FontWeight.normal
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(height: 0.4 * SizeConfig.heightMultiplier),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "${Constants.IMAGE_PATH}calendar.png",
                        height: 2.7 * SizeConfig.imageSizeMultiplier,
                        width: 2.7 * SizeConfig.imageSizeMultiplier,
                      ),
                      SizedBox(width: 1.2 * SizeConfig.widthMultiplier),
                      Text(
                        "${(job.startDate != null) ? DataTimeUtility.convertTimeForJob(job.startDate) : "NA"} ${(job.startTime!=null) ? job.startTime : "NA"}",
                        style: TextStyle(
                          fontSize: 1.6 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal,
                          color: AppTheme.colorTextGrey,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 0.9 * SizeConfig.heightMultiplier),
                  Row(
                    children: [
                      Text(
                        "Priority - ",
                        style: TextStyle(
                          color: AppTheme.colorTextGrey,
                          fontSize: 1.9 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal
                        ),
                      ),
                      Text(
                        (job.priority!=null) ? job.priority : "NA",
                        style: TextStyle(
                          color: (job.workPriority == "0") ? AppTheme.colorPriorityLow : ((job.workPriority == "1") ? AppTheme.colorPriorityMedium : (job.workPriority == "2") ? AppTheme.colorPriorityHigh : AppTheme.colorPriorityLow),
                          fontSize: 1.9 * SizeConfig.textMultiplier,
                          fontWeight: FontWeight.normal
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 3.5 * SizeConfig.heightMultiplier),
                  Text(
                    job.statusShow,
                    style: TextStyle(
                      fontSize: 1.9 * SizeConfig.textMultiplier,
                      color: (job.jobStatus == "0") ? Color(0xFF244764) : ((job.jobStatus == "1") ? Color(0xFFD5804D) : Color(0xFF244764)),
                      fontWeight: FontWeight.normal
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void saveLocalDb(Jobs job) async{
    DatabaseHelper databaseHelper = DatabaseHelper();
    if (await databaseHelper.isJobExist(job.jobId)) {
      if(await databaseHelper.isUpdateRequired(job.jobId, job.jobStatus))
        await databaseHelper.updateJob(job);
      else
        return;
    } else
      await databaseHelper.addJob(job);
  }

}
