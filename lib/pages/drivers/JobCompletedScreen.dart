import 'package:worktraq/pages/drivers/JobScreen.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class JobCompletedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      left: true,
      right: true,
      child: Scaffold(
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 60, right: 60, bottom: 40),
                  child: Image.asset(
                    "${Constants.IMAGE_PATH}job_completed.png"
                  ),
                ),
                RichText(
                  text: TextSpan(
                      text: 'JOB',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold
                      ),
                      children: <TextSpan>[
                        TextSpan(text: ' COMPLETED',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 24,
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ]
                  ),
                ),
                Container(
                  height: 40,
                  margin: EdgeInsets.only(top: 70, left: 60, right: 60),
                  child: GestureDetector(
                    onTap:(){
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => JobScreen()
                            ),
                            (route) => false
                        );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: AppTheme.colorViewNewJob,
                          style: BorderStyle.solid,
                          width: 1.0,
                        ),
                        color: AppTheme.colorViewNewJob,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Center(
                            child: Text(
                              "View New Jobs",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
      ),
    );
  }
}
