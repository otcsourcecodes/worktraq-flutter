import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/models/AfterWork.dart';
import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/models/BeforeWork.dart';
import 'package:worktraq/models/JobActivityResponse.dart';
import 'package:worktraq/models/JobReport.dart';
import 'package:worktraq/models/Jobs.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/pages/common/login/LoginScreen.dart';
import 'package:worktraq/pages/common/login/bloc/LoginBloc.dart';
import 'package:worktraq/pages/common/login/bloc/LoginState.dart';
import 'package:worktraq/pages/drivers/complete_jobs/CompleteJobList.dart';
import 'package:worktraq/pages/drivers/open_jobs/OpenJobList.dart';
import 'package:worktraq/services/DriverTrackingHelper.dart';
import 'package:worktraq/services/JobTrackingHelper.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/CustomDialog.dart';
import 'package:worktraq/utils/ResponseHandler.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:connectivity/connectivity.dart';
import 'package:driver_background_locator/location_dto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:path_provider/path_provider.dart';
import 'package:worktraq/widgets/LoadingScreen.dart';

class JobScreen extends StatefulWidget {
  @override
  _JobScreenState createState() => _JobScreenState();
}

class _JobScreenState extends State<JobScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  DriverTrackingHelper driverTrackingHelper;
  JobTrackingHelper jobTrackingHelper;
  final GlobalKey _globalKey = GlobalKey<State>();
  ReceivePort port = ReceivePort();
  bool isSync = false;
  Future<void> updateUI(LocationDto data) async {
    if(mounted) {
      setState(() {
        if (data != null) {
          DriverTrackingHelper.lastDriverLocation = data;

          print(("location lat ${data.latitude} long ${data.longitude}"));
          //showToast("location lat ${data.latitude} long ${data.longitude}", gravity: Toast.BOTTOM);
        }
      });
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }


  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    String networkStatus;
    String authToken = await AppSharedPreferences.getAuthToken();

    driverTrackingHelper = DriverTrackingHelper(context: context, authToken: authToken);
    if( (result == ConnectivityResult.wifi) || (result == ConnectivityResult.mobile) ){
      networkStatus = "Online";
      /*Future.delayed(const Duration(seconds: 2), () {
          if (IsolateNameServer.lookupPortByName(DriverLocationServiceRepository.isolateName) != null) {
            IsolateNameServer.removePortNameMapping(DriverLocationServiceRepository.isolateName);
          }

          IsolateNameServer.registerPortWithName(port.sendPort, DriverLocationServiceRepository.isolateName);

          port.listen((dynamic data) async {
            await updateUI(data);
          });
          driverTrackingHelper.initPlatformState();

          driverTrackingHelper.onStop();
          driverTrackingHelper.onStart();
        });*/
      DatabaseHelper databaseHelper = DatabaseHelper();
      print("sync data ${await databaseHelper.dbSyncCount()}");
      //Toast.makeText(context, "sync data " +databaseHandler.dbSyncCount(), Toast.LENGTH_SHORT).show();
      if(await databaseHelper.dbSyncCount() > 0) {
        setState(() {
          isSync = true;
        });
        syncDataFromLocalDbToServerDb(databaseHelper);
      } else{
        setState(() {
          isSync = false;
        });
      }
    } else if(result == ConnectivityResult.none){
      networkStatus = "Offline";
      driverTrackingHelper.onStop();
      JobTrackingHelper().onStop();
    } else{
      networkStatus = "Network failed";
      driverTrackingHelper.onStop();
      JobTrackingHelper().onStop();
    }

    print("networkStatus : $networkStatus");
  }

  void logoutAction(String choice) async{
    print("logout");
    AppSharedPreferences.clearUserData();
    DatabaseHelper().clearAllData();
    JobTrackingHelper().onStop();
    DriverTrackingHelper().onStop();

    Directory directory;
    if(Platform.isAndroid)
      directory = await getExternalStorageDirectory();
    else
      directory = await getApplicationDocumentsDirectory();

    String path = directory.path;

    final dir = Directory('$path/${Constants.SIGNATURE_IMAGE_DIRECTORY}');
    if(dir.existsSync())
      dir.deleteSync(recursive: true);

    final dirLocal = Directory('$path/${Constants.LOCAL_FILE_STORAGE_DIRECTORY}');
    if(dirLocal.existsSync())
      dirLocal.deleteSync(recursive: true);

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) => BlocProvider<LoginBloc>(
              create: (context) => LoginBloc(LoginState.empty()),
              child: LoginScreen()
          )
      ),
    );
  }

  Future<void> syncDataFromLocalDbToServerDb(DatabaseHelper databaseHelper) async{
    List<Jobs> jobList = await databaseHelper.getJobListForUploadOnServerFromLocal();
      Future.delayed(Duration.zero, () {
      Navigator.of(context).push(
          PageRouteBuilder(
              opaque: false,
              pageBuilder: (BuildContext context, _, __) => LoadingScreen(globalKey: _globalKey, title: "Please wait, we are sending data to server.")
          )
      );
    });
    String authToken = await AppSharedPreferences.getAuthToken();
    bool isProgress = await AppSharedPreferences.isInProgress();
    //print("local db : authToken : $authToken");
    if (jobList != null && jobList.length > 0){
      for (Jobs job in jobList){
        //print("local job : ${job.toString()}");
        JobReport jobReport = job.jobReport;

        if(jobReport != null){
          if(job.jobStatus == "1"){
            BeforeWork beforeWork = jobReport.beforeWork;
            if(beforeWork != null){
              List<File> images = List();
              File signatureFile;

              images.add(File(beforeWork.workImage[0]));
              images.add(File(beforeWork.workImage[1]));
              images.add(File(beforeWork.workImage[2]));
              images.add(File(beforeWork.workImage[2]));
              signatureFile = File(beforeWork.driverSignature);

              NetworkConfig.restClientInstance().updateJobActivity(authToken, images, signatureFile, job.jobId, beforeWork.startDateTime, "start", beforeWork.comments, "").then((result){
                if(!mounted) return;
                Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
                setState(() {
                  isSync = false;
                });
                if(result.status == "success"){
                  JobActivityResponse jobActivityResponse = result;
                  print(jobActivityResponse.toString());
                }else{
                  print("Error : status - ${result.status} Message - ${result.message}");
                  //showErrorDialog(context,  'Error', result.message);
                }
                //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
              })./*catchError((onError){
                print("onError : ${onError.toString()}");
              });*/catchError((Object obj){
                if(!mounted) return;
                Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
                switch (obj.runtimeType) {
                  case DioError:
                    final res = (obj as DioError).response;
                    print("res: ${res?.toString()}");
                    if(res != null && res?.data != null) {
                      AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res?.data);
                      //print("${res?.data.toString()} ");
                      if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                        ResponseHandler.handleResponse(context, authFailedResponse);
                      }
                    } else {
                      NetworkConfig.logger.e("Got error : ${res?.statusCode} -> ${res?.statusMessage} -> ${res?.data}");
                      //showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
                    }
                    break;
                  default:
                    NetworkConfig.logger.e("Got error : ${obj.toString()}");
                    break;
                }
              });
            }
          } else if(job.jobStatus == "2"){
            BeforeWork beforeWork = jobReport.beforeWork;
            AfterWork afterWork = jobReport.afterWork;

            if(beforeWork != null){
              List<File> images = List();
              File signatureFile;

              images.add(File(beforeWork.workImage[0]));
              images.add(File(beforeWork.workImage[1]));
              images.add(File(beforeWork.workImage[2]));
              images.add(File(beforeWork.workImage[2]));
              signatureFile = File(beforeWork.driverSignature);

              NetworkConfig.restClientInstance().updateJobActivity(authToken, images, signatureFile, job.jobId, beforeWork.startDateTime, "start", beforeWork.comments, "").then((result){
                if(result.status == "success"){
                  JobActivityResponse jobActivityResponse = result;
                  //print(jobActivityResponse.toString());
                  if(afterWork != null){
                    List<File> afterImages = List();
                    File customerSignatureFile;

                    afterImages.add(File(afterWork.workImage[0]));
                    afterImages.add(File(afterWork.workImage[1]));
                    afterImages.add(File(afterWork.workImage[2]));
                    afterImages.add(File(afterWork.workImage[2]));
                    customerSignatureFile = File(afterWork.customerSignature);

                    NetworkConfig.restClientInstance().updateJobActivity(authToken, afterImages, customerSignatureFile, job.jobId, afterWork.endDateTime, "end", afterWork.comments, (job.jobTypeQuestionAnswers != null) ? job.jobTypeQuestionAnswers : "").then((result) async{
                      Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
                      setState(() {
                        isSync = false;
                      });
                      if( isProgress )
                        AppSharedPreferences.setIsInProgress(false);

                      if(result.status == "success"){
                        JobActivityResponse jobActivityResponse = result;
                        print(jobActivityResponse.toString());
                        Directory directory;
                        if(Platform.isAndroid)
                          directory = await getExternalStorageDirectory();
                        else
                          directory = await getApplicationDocumentsDirectory();

                        String path = directory.path;

                        final dirLocal = Directory('$path/${Constants.LOCAL_FILE_STORAGE_DIRECTORY}');
                        if(dirLocal.existsSync())
                          dirLocal.deleteSync(recursive: true);

                        int deleteJobResult = await databaseHelper.deleteJobById(job.jobId);

                        if(deleteJobResult == 1){
                        }
                      }else{
                        print("Error : status - ${result.status} Message - ${result.message}");
                        //showErrorDialog(context,  'Error', result.message);
                      }
                      //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
                    })./*catchError((onError){
                      if( isProgress )
                        AppSharedPreferences.setIsInProgress(false);
                      print("onError: ${onError.toString()}");
                    });*/catchError((Object obj){
                      setState(() {
                        isSync = false;
                      });
                      switch (obj.runtimeType) {
                        case DioError:
                          final res = (obj as DioError).response;
                          print("res : ${res?.toString()}");
                          if(res?.data != null) {
                            print("${res?.data.toString()} ");
                            AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res?.data);

                            if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                              ResponseHandler.handleResponse(context, authFailedResponse);
                            }
                          } else {
                            NetworkConfig.logger.e("Got error : ${res?.statusCode} -> ${res?.statusMessage} -> ${res?.data}");
                            showErrorDialog(context, "Server Error", "${res.statusCode} -> ${res.statusMessage}");
                          }
                          break;
                        default:
                          NetworkConfig.logger.e("Got error : ${obj.toString()}");
                          showErrorDialog(context, "Error", "${obj.toString()}");
                          break;
                      }
                    });
                  }
                }else{
                  Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
                  setState(() {
                    isSync = false;
                  });
                  print("Error : status - ${result.status} Message - ${result.message}");
                  //showErrorDialog(context,  'Error', result.message);
                }
                //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
              })./*catchError((onError){
                if( isProgress )
                  AppSharedPreferences.setIsInProgress(false);
                print("onError : ${onError?.toString()}");
              });*/catchError((Object obj){
                //if(!mounted) return;
                Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
                setState(() {
                  isSync = false;
                });
                switch (obj.runtimeType) {
                  case DioError:
                    final res = (obj as DioError).response;
                    if(res!= null && res?.data != null) {
                      print("res ${res?.data.toString()}");
                      AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res?.data);

                      if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                        ResponseHandler.handleResponse(context, authFailedResponse);
                      }
                    }
                    break;
                  default:
                    NetworkConfig.logger.e("Got error : ${obj.toString()}");
                    break;
                }
              });
            }
          }
        } else {
          Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
        }
      }
    } else {
      Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    //print("height ${SizeConfig.heightMultiplier} width ${SizeConfig.widthMultiplier}");
    FlutterStatusbarcolor.setStatusBarColor(AppTheme.primaryDarkColor);
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: Color(0xFFECECEC),
        body: ListView(
          children: [
            Container(
              color: AppTheme.primaryColor,
              child: Flex(
                direction: Axis.vertical,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right:  2 * SizeConfig.heightMultiplier, left:  2 * SizeConfig.heightMultiplier),
                        child: Text(
                          "All Jobs",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 2.5 * SizeConfig.textMultiplier
                          ),
                        ),
                      ),
                      Container(
                        child: PopupMenuButton(
                            icon: Image.asset(
                              "${Constants.IMAGE_PATH}more.png",
                              height: 18,
                              width: 18,
                            ),
                            onSelected: logoutAction,
                            itemBuilder: (BuildContext context){
                              return Constants.choices.map((String choice) {
                                return PopupMenuItem<String>(
                                    value: choice,
                                    height: 15,
                                    textStyle: TextStyle(
                                        fontSize: 2.5 * SizeConfig.textMultiplier,
                                        color: Colors.black
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 82.0, top: 5.0, bottom: 5.0),
                                      child: Text(
                                          choice
                                      ),
                                    )
                                );
                              }).toList();
                            }
                        ),
                      )
                    ],
                  ),
                  PreferredSize(
                    preferredSize: null,
                    child: TabBar(
                      controller: _tabController,
                      labelStyle: TextStyle(
                        fontSize: 2 * SizeConfig.textMultiplier,
                        fontWeight: FontWeight.w600
                      ),
                      indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(width: 3.0,
                            color: Colors.white
                          ),
                      ),
                      unselectedLabelColor: Color(0xFF788D9F),
                      tabs: [
                        Tab(text: "OPEN JOBS"),
                        Tab(text: "COMPLETED JOBS"),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height:  SizeConfig.isPortrait ? ((100-17.5) * SizeConfig.heightMultiplier) : ((100-35) * SizeConfig.widthMultiplier),
              child: TabBarView(
                controller: _tabController,
                //physics: NeverScrollableScrollPhysics(),
                children: [
                  OpenJobList(),
                  CompleteJobList()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}