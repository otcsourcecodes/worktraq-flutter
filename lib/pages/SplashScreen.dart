import 'dart:async';
import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:worktraq/models/Users.dart';
import 'package:worktraq/pages/customers/AllCustomerJobScreen.dart';
import 'package:worktraq/pages/drivers/JobScreen.dart';
import 'package:worktraq/services/DriverLocationServiceRepository.dart';
import 'package:worktraq/services/DriverTrackingHelper.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/ConnectivityUtility.dart';
import 'package:worktraq/utils/Constants.dart';
import 'package:worktraq/utils/SizeConfig.dart';
import 'package:driver_background_locator/location_dto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

import 'common/login/LoginScreen.dart';
import 'common/login/bloc/LoginBloc.dart';
import 'common/login/bloc/LoginState.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  DriverTrackingHelper driverTrackingHelper;
  ReceivePort port = ReceivePort();
  String nameKey = "_key_name";

  startTime() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationPage);
  }

  Future<void> updateUI(LocationDto data) async {
    if(mounted) {
      setState(() {
        if (data != null) {
          DriverTrackingHelper.lastDriverLocation = data;

          print(("location lat ${data.latitude} long ${data.longitude}"));
          //showToast("location lat ${data.latitude} long ${data.longitude}", gravity: Toast.BOTTOM);
        }
      });
    }
  }

  void navigationPage() async{
    bool isLoggedIn = await AppSharedPreferences.isUserLoggedIn();
    String userType = await AppSharedPreferences.getUserType();
    String authToken = await AppSharedPreferences.getAuthToken();

    print("isLogin $isLoggedIn");
    print("userType $userType");

    if (isLoggedIn != null && isLoggedIn) {
      if(userType != null && userType == "1"){
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => AllCustomerJobScreen()
          ),
        );
      } else if(userType != null && userType == "2"){
        Future.delayed(const Duration(seconds: 2), () async{
          driverTrackingHelper = DriverTrackingHelper(context: context, authToken: authToken);
          bool isInternet = await ConnectivityUtility().isInternet();
          if(isInternet){
            if (IsolateNameServer.lookupPortByName(DriverLocationServiceRepository.isolateName) != null) {
              IsolateNameServer.removePortNameMapping(DriverLocationServiceRepository.isolateName);
            }

            IsolateNameServer.registerPortWithName(port.sendPort, DriverLocationServiceRepository.isolateName);

            port.listen((dynamic data) async {
              await updateUI(data);
            });

            driverTrackingHelper.initPlatformState();

            driverTrackingHelper.onStop();
            driverTrackingHelper.onStart();
          } else{
            driverTrackingHelper.onStop();
          }
        });

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => JobScreen()
          ),
        );
      }
    } else{
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => BlocProvider<LoginBloc>(
                create: (context) => LoginBloc(LoginState.empty()),
                child: LoginScreen()
            )
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    startTime();
    /*const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler(
          (MethodCall methodcall) async {
        if (methodcall.method == 'getAll') {
          return {"flutter." + nameKey: "[ No Name Saved ]"};
        }
        return null;
      },
    );*/
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(AppTheme.primaryDarkColor);
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Center(
          child: Image.asset(
            "${Constants.IMAGE_PATH}worktraq_logo_stacked2.png",
                width: 70 * SizeConfig.widthMultiplier,
            ),
          ),
        )
    );
  }
}
