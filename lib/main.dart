import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:worktraq/pages/SplashScreen.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/SizeConfig.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'Worktraq',
              theme: AppTheme.lightTheme,
              home: SplashScreen(),
            );
          },
        );
      },
    );;
  }
}