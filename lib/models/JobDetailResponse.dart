import 'Jobs.dart';

class JobDetailResponse {
  String status;
  String message;
  Jobs jobs;

  JobDetailResponse({this.status, this.message, this.jobs});

  JobDetailResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    jobs = json['jobs'] != null ? new Jobs.fromJson(json['jobs']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.jobs != null) {
      data['jobs'] = this.jobs.toJson();
    }
    return data;
  }
}
