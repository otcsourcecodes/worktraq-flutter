class OtherInfo {
  String driverMetaId;
  String userId;
  String emergencyPersonName;
  String emergencyPersonNumber;
  String dob;
  String doh;
  String licenseNumber;
  String licenseExpiryDate;
  String address;
  String street;
  String street2;
  String city;
  String state;
  String zip;
  String country;
  String latitude;
  String longitude;
  String customerMetaId;
  String creditHoldStatus;
  String billAddress;
  String billStreet;
  String billStreet2;
  String billCity;
  String billState;
  String billZip;
  String billCountry;
  String billLatitude;
  String billLongitude;

  OtherInfo(
      {this.driverMetaId,
        this.userId,
        this.emergencyPersonName,
        this.emergencyPersonNumber,
        this.dob,
        this.doh,
        this.licenseNumber,
        this.licenseExpiryDate,
        this.address,
        this.street,
        this.street2,
        this.city,
        this.state,
        this.zip,
        this.country,
        this.latitude,
        this.longitude,
        this.customerMetaId,
        this.creditHoldStatus,
        this.billAddress,
        this.billStreet,
        this.billStreet2,
        this.billCity,
        this.billState,
        this.billZip,
        this.billCountry,
        this.billLatitude,
        this.billLongitude});

  OtherInfo.fromJson(Map<String, dynamic> json) {
    driverMetaId =  json['driverMetaId'] != null ? json['driverMetaId'] : null;
    userId = json['userId'];
    emergencyPersonName = json['emergencyPersonName'] != null ? json['emergencyPersonName'] : null;
    emergencyPersonNumber = json['emergencyPersonNumber'] != null ? json['emergencyPersonNumber'] : null;
    dob = json['dob'] != null ? json['dob'] : null;
    doh = json['doh'] != null ? json['doh'] : null;
    licenseNumber = json['licenseNumber'] != null ? json['licenseNumber'] : null;
    licenseExpiryDate = json['licenseExpiryDate'] != null ? json['licenseExpiryDate'] : null;
    address = json['address'];
    street = json['street'];
    street2 = json['street2'];
    city = json['city'];
    state = json['state'];
    zip = json['zip'];
    country = json['country'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    customerMetaId = json['customerMetaId'] != null ? json['customerMetaId'] : null;
    creditHoldStatus = json['creditHoldStatus'] != null ? json['creditHoldStatus'] : null;
    billAddress = json['billAddress'] != null ? json['billAddress'] : null;
    billStreet = json['billStreet'] != null ? json['billStreet'] : null;
    billStreet2 = json['billStreet2'] != null ? json['billStreet2'] : null;
    billCity = json['billCity'] != null ? json['billCity'] : null;
    billState = json['billState']  != null ? json['billState'] : null;
    billZip = json['billZip'] != null ? json['billZip'] : null;
    billCountry = json['billCountry'] != null ? json['billCountry'] : null;
    billLatitude = json['billLatitude'] != null ? json['billLatitude'] : null;
    billLongitude = json['billLongitude'] != null ? json['billLongitude'] : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.driverMetaId != null) {
      data['driverMetaId'] = this.driverMetaId;
    }
    if(this.userId != null) {
      data['userId'] = this.userId;
    }
    if(this.emergencyPersonName != null) {
      data['emergencyPersonName'] = this.emergencyPersonName;
    }
    if(this.emergencyPersonNumber != null) {
      data['emergencyPersonNumber'] = this.emergencyPersonNumber;
    }
    if(this.dob != null) {
      data['dob'] = this.dob;
    }
    if(this.doh != null) {
      data['doh'] = this.doh;
    }
    if(this.licenseNumber != null) {
      data['licenseNumber'] = this.licenseNumber;
    }
    if(this.licenseExpiryDate != null) {
      data['licenseExpiryDate'] = this.licenseExpiryDate;
    }
    data['address'] = this.address;
    data['street'] = this.street;
    data['street2'] = this.street2;
    data['city'] = this.city;
    data['state'] = this.state;
    data['zip'] = this.zip;
    data['country'] = this.country;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;

    if (this.customerMetaId != null) {
      data['customerMetaId'] = this.customerMetaId;
    }
    if(this.creditHoldStatus != null)
      data['creditHoldStatus'] = this.creditHoldStatus;

    if(this.billAddress != null)
      data['billAddress'] = this.billAddress;

    if(this.billStreet != null)
      data['billStreet'] = this.billStreet;

    if(this.billStreet2 != null)
      data['billStreet2'] = this.billStreet2;

    if(this.billCity != null)
      data['billCity'] = this.billCity;

    if(this.billState != null)
      data['billState'] = this.billState;

    if(this.billZip != null)
      data['billZip'] = this.billZip;

    if(this.billCountry != null)
      data['billCountry'] = this.billCountry;

    if(this.billLatitude != null)
      data['billLatitude'] = this.billLatitude;

    if(this.billLongitude != null)
      data['billLongitude'] = this.billLongitude;
    return data;
  }

  @override
  String toString() {
    return "{\"driverMetaId\":\"" + ((driverMetaId != null) ? driverMetaId : "") + "\"," +
        "\"userId\":\"" + userId + "\"," +
        "\"emergencyPersonName\":\"" + ((emergencyPersonName != null) ? emergencyPersonName : "") + "\"," +
        "\"emergencyPersonNumber\":\"" + ((emergencyPersonNumber != null) ? emergencyPersonNumber : "") + "\"," +
        "\"dob\":\"" + ((dob != null) ? dob : "") + "\"," +
        "\"doh\":\"" + ((doh != null) ? doh : "") + "\"," +
        "\"licenseNumber\":\"" + ((licenseNumber != null) ? licenseNumber : "") + "\"," +
        "\"licenseExpiryDate\":\"" + ((licenseExpiryDate != null) ? licenseExpiryDate : "") + "\"," +
        "\"address\":\"" + address + "\"," +
        "\"street\":\"" + street + "\"," +
        "\"street2\":\"" + street2 + "\"," +
        "\"city\":\"" + city + "\"," +
        "\"state\":\"" + state + "\"," +
        "\"zip\":\"" + zip + "\"," +
        "\"country\":\"" + country + "\"," +
        "\"latitude\":\"" + latitude + "\"," +
        "\"longitude\":\"" + longitude + "\"," +
        "\"customerMetaId\":\"" + ((customerMetaId != null) ? customerMetaId : "") + "\"," +
        "\"creditHoldStatus\":\"" + ((creditHoldStatus != null) ? creditHoldStatus : "") + "\"," +
        "\"billAddress\":\"" + ((billAddress != null) ? billAddress : "") + "\"," +
        "\"billStreet\":\"" + ((billStreet != null) ? billStreet : "") + "\"," +
        "\"billStreet2\":\"" + ((billStreet2 != null) ? billStreet2 : "") + "\"," +
        "\"billCity\":\"" + ((billCity != null) ? billCity : "") + "\"," +
        "\"billState\":\"" + ((billState != null) ? billState : "") + "\"," +
        "\"billZip\":\"" + ((billZip != null) ? billZip : "") + "\"," +
        "\"billCountry\":\"" + ((billCountry != null) ? billCountry : "") + "\"," +
        "\"billLatitude\":\"" + ((billLatitude != null) ? billLatitude : "") + "\"," +
        "\"billLongitude\":\"" + ((billLongitude != null) ? billLongitude : "") + "\"}";
  }

}