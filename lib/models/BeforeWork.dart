class BeforeWork {
  String startDateTime;
  String comments;
  String driverSignature;
  List<String> workImage;

  BeforeWork(
      {this.startDateTime,
        this.comments,
        this.driverSignature,
        this.workImage});

  BeforeWork.fromJson(Map<String, dynamic> json) {
    startDateTime = (json['startDateTime'] != null) ? json['startDateTime'] : null;
    comments = (json['comments'] != null) ? json['comments'] : null;
    driverSignature = (json['driverSignature'] != null) ? json['driverSignature'] : null;
    workImage = (json['workImage'] != null) ? json['workImage'].cast<String>() : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(this.startDateTime != null)
      data['startDateTime'] = this.startDateTime;

    if(this.comments != null)
      data['comments'] = this.comments;

    if(this.driverSignature != null)
      data['driverSignature'] = this.driverSignature;

    if(this.workImage != null && this.workImage.length > 0)
      data['workImage'] = this.workImage;

    return data;
  }

  String toString() {
    String workImageString = "[";
    if ( workImage!= null && workImage.length > 0){
      for (int i = 0; i < workImage.length; i++){
        if(i == 0)
          workImageString = workImageString + workImage[i];
        else
          workImageString = workImageString + ", " + workImage[i];
      }
    }
    workImageString = workImageString + "]";

    return "{\"startDateTime\":\"" + ((startDateTime != null) ? startDateTime : "") + "\"," +
        "\"comments\":\"" + ((comments != null) ? comments : "") + "\"," +
        "\"driverSignature\":\"" + ((driverSignature != null) ? driverSignature : "") + "\"," +
        "\"workImage\":\"" + ((workImageString != null) ? workImageString : "[]") + "\"}";
  }
}
