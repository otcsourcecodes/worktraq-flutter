import 'GeoTimeDuration.dart';
import 'JobReport.dart';
import 'JobTypeQuestions.dart';

class Jobs {
  String jobId;
  String jobName;
  String jobTypeId;
  String driverId;
  String customerName;
  String jobType;
  String driverName;
  String customerId;
  String jobStatus;
  String startDate;
  String startTime;
  String statusShow;
  String address;
  String street;
  String street2;
  String city;
  String state;
  String zip;
  String country;
  String latitude;
  String longitude;
  JobReport jobReport;
  String geoFencing;
  String points;
  String polygonColor;
  String crd;
  String upd;
  String priority;
  String workPriority;
  String distance;
  String generatePdf;
  String timeDuration;
  GeoTimeDuration geoTimeDuration;
  String geoFencingUrl;
  List<JobTypeQuestions> jobTypeQuestions;
  String jobTypeQuestionsString;
  String jobTypeQuestionAnswers;
  String updatedStatus;
  String imageUpdatedStatus;
  bool isLocalDb;

  Jobs(
      {this.jobId,
        this.jobName,
        this.jobTypeId,
        this.driverId,
        this.customerName,
        this.jobType,
        this.driverName,
        this.customerId,
        this.jobStatus,
        this.startDate,
        this.startTime,
        this.statusShow,
        this.address,
        this.street,
        this.street2,
        this.city,
        this.state,
        this.zip,
        this.country,
        this.latitude,
        this.longitude,
        this.jobReport,
        this.geoFencing,
        this.points,
        this.polygonColor,
        this.crd,
        this.upd,
        this.priority,
        this.workPriority,
        this.distance,
        this.generatePdf,
        this.timeDuration,
        this.geoTimeDuration,
        this.geoFencingUrl,
        this.jobTypeQuestions,
        this.jobTypeQuestionAnswers,
        this.jobTypeQuestionsString,
        this.imageUpdatedStatus,
        this.isLocalDb,
        this.updatedStatus
      });

  Jobs.fromJson(Map<String, dynamic> json) {
    jobId = json['jobId'];
    jobName = json['jobName'];
    jobTypeId = json['jobTypeId'];
    driverId = json['driverId'];
    customerName = json['customerName'];
    jobType = json['jobType'];
    driverName = json['driverName'];
    customerId = json['customerId'];
    jobStatus = json['jobStatus'];
    startDate = json['startDate'];
    startTime = json['startTime'];
    statusShow = json['statusShow'];
    address = json['address'];
    street = json['street'];
    street2 = json['street2'];
    city = json['city'];
    state = json['state'];
    zip = json['zip'];
    country = json['country'];
    latitude = json['latitude'] != null ? json['latitude'] : null;
    longitude = json['longitude'] != null ? json['longitude'] : null;
    jobReport = json['jobReport'] != null ? JobReport.fromJson(json['jobReport']) : null;
    geoFencing = json['geoFencing'];
    points = json['points'];
    polygonColor = json['polygonColor'];
    crd = json['crd'];
    upd = json['upd'];
    priority = json['priority'];
    workPriority = json['workPriority'];
    distance = (json['distance'] != null) ? json['distance'] : "0.0";
    generatePdf = json['generatePdf'];
    timeDuration = json['timeDuration'];
    geoTimeDuration = json['geoTimeDuration'] != null ? new GeoTimeDuration.fromJson(json['geoTimeDuration']) : null;
    geoFencingUrl = json['geoFencingUrl'];
    if (json['jobTypeQuestions'] != null) {
      jobTypeQuestions = new List<JobTypeQuestions>();
      json['jobTypeQuestions'].forEach((v) {
        jobTypeQuestions.add(JobTypeQuestions.fromJson(v));
      });
    }
    jobTypeQuestionsString = json['jobTypeQuestionsString'] != null ? jobTypeQuestionsString : null;
    jobTypeQuestionAnswers = json['jobTypeQuestionAnswers'] != null ? jobTypeQuestionAnswers : null;
    updatedStatus = json['updatedStatus'] != null ? updatedStatus : null;
    imageUpdatedStatus = json['imageUpdatedStatus'] != null ? imageUpdatedStatus : null;
    isLocalDb = json['isLocalDb'] != null ? isLocalDb : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['jobId'] = this.jobId;
    data['jobName'] = this.jobName;
    data['jobTypeId'] = this.jobTypeId;
    data['driverId'] = this.driverId;
    data['customerName'] = this.customerName;
    data['jobType'] = this.jobType;
    data['driverName'] = this.driverName;
    data['customerId'] = this.customerId;
    data['jobStatus'] = this.jobStatus;
    data['startDate'] = this.startDate;
    data['startTime'] = this.startTime;
    data['statusShow'] = this.statusShow;
    data['address'] = this.address;
    data['street'] = this.street;
    data['street2'] = this.street2;
    data['city'] = this.city;
    data['state'] = this.state;
    data['zip'] = this.zip;
    data['country'] = this.country;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    if (this.jobReport != null) {
      data['jobReport'] = this.jobReport.toJson();
    }
    data['geoFencing'] = this.geoFencing;
    data['points'] = this.points;
    data['polygonColor'] = this.polygonColor;
    data['crd'] = this.crd;
    data['upd'] = this.upd;
    data['priority'] = this.priority;
    data['workPriority'] = this.workPriority;
    if(this.distance != null) {
      data['distance'] = this.distance;
    }
    data['generatePdf'] = this.generatePdf;
    data['timeDuration'] = this.timeDuration;
    if (this.geoTimeDuration != null) {
      data['geoTimeDuration'] = this.geoTimeDuration.toJson();
    }
    data['geoFencingUrl'] = this.geoFencingUrl;
    if (this.jobTypeQuestions != null) {
      data['jobTypeQuestions'] = this.jobTypeQuestions.map((v) => v.toJson()).toList();
    }

    if (this.jobTypeQuestionAnswers != null) {
      data['jobTypeQuestionAnswers'] = this.jobTypeQuestionAnswers;
    }

    if(jobTypeQuestionsString != null) {
      data['jobTypeQuestionsString'] = this.jobTypeQuestionsString;
    }

    if(this.updatedStatus != null){
      data['updatedStatus'] = this.updatedStatus;
    }

    if(this.imageUpdatedStatus != null){
      data['imageUpdatedStatus'] = this.imageUpdatedStatus;
    }

    if(this.isLocalDb != null){
      data['isLocalDb'] = this.isLocalDb;
    }

    return data;
  }

  @override
  String toString() {
    return "\"Job\" : {\"jobId\":\"" + jobId + "\"," +
        "\"jobName\":\"" + ((jobName != null) ? jobName : "") + "\"," +
        "\"jobTypeId\":\"" + ((jobTypeId != null) ? jobTypeId : "") + "\"," +
        "\"customerId\":\"" + ((customerId != null) ? customerId : "") + "\"," +
        "\"driverId\":\"" + ((driverId != null) ? driverId : "") + "\"," +
        "\"startDate\":\"" + ((startDate != null) ? startDate : "") + "\"," +
        "\"startTime='" + ((startTime != null) ? startTime : "") + "\"," +
        "\"address\":\"" + ((address != null) ? address : "") + "\"," +
        "\"street\":\"" + ((street != null) ? street : "") + "\"," +
        "\"street2\":\"" + ((street2 != null) ? street2 : "") + "\"," +
        "\"city\":\"" + ((city != null) ? city : "") + "\"," +
        "\"state\":\"" + ((state != null) ? state : "") + "\"," +
        "\"zip\":\"" + ((zip != null) ? zip : "") + "\"," +
        "\"country\":\"" + ((country != null) ? country : "") + "\"," +
        "\"latitude\":\"" + ((latitude != null) ? latitude : "") + "\"," +
        "\"longitude\":\"" + ((longitude != null) ? longitude : "") + "\"," +
        "\"jobReport\":\"" + ((jobReport != null)  ? jobReport.toString() : "") + "\"," +
        "\"jobStatus\":\"" + ((jobStatus != null) ? jobStatus : "") + "\"," +
        "\"crd\":\"" + ((crd != null) ? crd : "") + "\"," +
        "\"upd\":\"" + ((upd != null) ? upd : "") + "\"," +
        "\"priority\":\"" + ((priority != null) ? priority : "") + "\"," +
        "\"workPriority\":\"" + ((workPriority != null) ? workPriority : "") + "\"," +
        "\"distance\":\"" + ((distance != null) ? distance : "") + "\"," +
        "\"customerName\":\"" + ((customerName != null) ? customerName : "") + "\"," +
        "\"jobType\":\"" + ((jobType != null) ? jobType : "") + "\"," +
        "\"driverName\":\"" + ((driverName != null) ? driverName : "") + "\"," +
        "\"jobStatus\":\"" + ((jobStatus != null) ? jobStatus : "") + "\"," +
        "\"statusShow\":\"" + ((statusShow != null) ? statusShow : "") + "\"," +
        "\"geoFencing\":\"" + ((geoFencing != null) ? geoFencing : "") + "\"," +
        "\"points\":\"" + ((points != null) ? points : "") + "\"," +
        "\"polygonColor\":\"" + ((polygonColor != null) ? polygonColor : "") + "\"," +
        "\"geoFencingUrl\":\"" + ((geoFencingUrl != null) ? geoFencingUrl : "") + "\"," +
        "\"generatePdf\":\"" + ((generatePdf != null) ? generatePdf : "") + "\"," +
        /*"\"jobTypeQuestionsString\":\"" + jobTypeQuestionsString + "\"," +
        "\"jobTypeQuestionAnswers\":\"" + jobTypeQuestionAnswers + "\"," +
        "\"updatedStatus\":\"" + updatedStatus + "\"," +
        "\"imageUpdatedStatus\":\"" + imageUpdatedStatus + "\"," +
        "\"isLocalDb\":\"" + isLocalDb + "\"," +*/
        "}";
  }
}