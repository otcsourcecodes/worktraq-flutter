import 'Users.dart';

class LoginResponse {
  String status;
  String message;
  Users users;

  LoginResponse({this.status, this.message, this.users});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    users = json['users'] != null ? new Users.fromJson(json['users']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.users != null) {
      data['users'] = this.users.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return "{\"users\":" + ((users != null) ? users.toString() : "") + "," +
        "\"status\":\"" + status + "\"," +
        "\"message\":" + message + "}";
  }
}