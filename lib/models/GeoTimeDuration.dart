import 'Timinig.dart';

class GeoTimeDuration {
  List<Timinig> timinig;
  String total;

  GeoTimeDuration({this.timinig, this.total});

  GeoTimeDuration.fromJson(Map<String, dynamic> json) {
    if (json['timinig'] != null) {
      timinig = new List<Timinig>();
      json['timinig'].forEach((v) {
        timinig.add(new Timinig.fromJson(v));
      });
    }
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.timinig != null) {
      data['timinig'] = this.timinig.map((v) => v.toJson()).toList();
    }
    data['total'] = this.total;
    return data;
  }
}
