import 'AfterWork.dart';
import 'BeforeWork.dart';

class JobReport {
  BeforeWork beforeWork;
  AfterWork afterWork;

  JobReport({this.beforeWork, this.afterWork});

  JobReport.fromJson(Map<String, dynamic> json) {
    beforeWork = json['beforeWork'] != null ? new BeforeWork.fromJson(json['beforeWork']) : null;
    afterWork = json['afterWork'] != null && json['afterWork'] != {} ? new AfterWork.fromJson(json['afterWork']) : AfterWork();
    //afterWork = json['afterWork'] != null && json['afterWork'] != {} ? new AfterWork.fromJson(json['afterWork']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.beforeWork != null) {
      data['beforeWork'] = this.beforeWork.toJson();
    }
    if (this.afterWork != null) {
      data['afterWork'] = this.afterWork.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return "{\"beforeWork\":\"" + (( beforeWork != null ) ? beforeWork?.toString() : "") + "\"," +
        "\"afterWork\":\"" + ((afterWork != null) ? afterWork.toString() : "")  + "\"}";
  }
}
