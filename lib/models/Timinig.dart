class Timinig {
  String jobId;
  String inDateTime;
  String outDateTime;
  String timeDuration;
  String startTime;
  String endTime;

  Timinig(
      {this.jobId,
        this.inDateTime,
        this.outDateTime,
        this.timeDuration,
        this.startTime,
        this.endTime});

  Timinig.fromJson(Map<String, dynamic> json) {
    jobId = json['jobId'];
    inDateTime = json['inDateTime'];
    outDateTime = json['outDateTime'];
    timeDuration = json['timeDuration'];
    startTime = json['startTime'];
    endTime = json['endTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['jobId'] = this.jobId;
    data['inDateTime'] = this.inDateTime;
    data['outDateTime'] = this.outDateTime;
    data['timeDuration'] = this.timeDuration;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    return data;
  }
}