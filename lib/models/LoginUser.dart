
class LoginUser {
  int userId;
  String email;
  String token;
  String loginType;

  LoginUser({this.userId, this.email, this.token, this.loginType});

  LoginUser.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    email = json['email'];
    token = json['token'];
    loginType = json['loginType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['email'] = this.email;
    data['token'] = this.token;
    data['loginType'] = this.loginType;
    return data;
  }
}

