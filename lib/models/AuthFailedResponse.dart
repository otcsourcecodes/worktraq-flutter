class AuthFailedResponse {
  String message;
  String authToken;
  int responseCode;
  int isActive;

  AuthFailedResponse(
      {this.message, this.authToken, this.responseCode, this.isActive});

  AuthFailedResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    authToken = json['authToken'];
    responseCode = json['responseCode'];
    isActive = json['isActive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['authToken'] = this.authToken;
    data['responseCode'] = this.responseCode;
    data['isActive'] = this.isActive;
    return data;
  }
}