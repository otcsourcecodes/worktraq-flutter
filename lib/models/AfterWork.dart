class AfterWork {
  String endDateTime;
  String comments;
  String customerSignature;
  List<String> workImage;

  AfterWork({this.endDateTime,
    this.comments,
    this.customerSignature,
    this.workImage
  });

  AfterWork.fromJson(Map<String, dynamic> json) {
    endDateTime = (json['endDateTime'] != null) ? json['endDateTime'] : null;
    comments = (json['comments'] != null) ? json['comments'] : null;
    customerSignature = (json['customerSignature'] != null) ? json['customerSignature'] : null;
    workImage = (json['workImage'] != null) ? json['workImage'].cast<String>() : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(this.endDateTime != null)
      data['endDateTime'] = this.endDateTime;

    if(this.comments != null)
      data['comments'] = this.comments;

    if(this.customerSignature != null)
      data['driverSignature'] = this.customerSignature;

    if(this.workImage != null && this.workImage.length > 0)
      data['workImage'] = this.workImage;

    return data;
  }

  @override
  String toString() {
    String workImageString = "[";
    if (workImage!= null && workImage.length > 0){
      for (int i = 0; i < workImage.length; i++){
        if(i == 0)
          workImageString = workImageString + workImage[i];
        else
          workImageString = workImageString + ", " + workImage[i];
      }
    }
    workImageString = workImageString + "]";

    return "{\"endDateTime\":\"" + ((endDateTime != null) ? endDateTime : "") + "\"," +
        "\"comments\":\"" + ((comments != null) ? comments : "") + "\"," +
        "\"customerSignature\":\"" + ((customerSignature != null) ? customerSignature : "") + "\"," +
        "\"workImage\":\"" + ((workImageString != null) ? workImageString : "[]") + "\"}";
  }
}
