import 'OtherInfo.dart';

class Users {
  String id;
  String userId;
  String fullName;
  String email;
  String authToken;
  String userType;
  String profileImage;
  String userRole;
  String fireBaseToken;
  bool isLogout;
  OtherInfo otherInfo;

  Users(
      {this.id,
        this.userId,
        this.fullName,
        this.email,
        this.authToken,
        this.userType,
        this.profileImage,
        this.userRole,
        /*this.fireBaseToken,
        this.isLogout,*/
        this.otherInfo});

  Users.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    fullName = json['fullName'];
    email = json['email'];
    authToken = json['authToken'];
    userType = json['userType'];
    profileImage = json['profileImage'];
    userRole = json['userRole'];
    fireBaseToken = json['fireBaseToken'] != null ? json['fireBaseToken'] : null;
    isLogout = json['isLogout'] != null ? json['isLogout'] : null;
    otherInfo = json['otherInfo'] != null ? new OtherInfo.fromJson(json['otherInfo']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['fullName'] = this.fullName;
    data['email'] = this.email;
    data['authToken'] = this.authToken;
    data['userType'] = this.userType;
    data['profileImage'] = this.profileImage;
    data['userRole'] = this.userRole;

    if (this.fireBaseToken != null)
      data['fireBaseToken'] = this.fireBaseToken;

    if (this.isLogout != null)
      data['isLogout'] = this.isLogout;

    if (this.otherInfo != null) {
      data['otherInfo'] = this.otherInfo.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return "{\"id\":\"" + id + "\"," +
        "\"userId\":\"" + userId + "\"," +
        "\"fullName\":\"" + fullName + "\"," +
        "\"email\":\"" + email + "\"," +
        "\"authToken\":\"" + authToken + "\"," +
        "\"userType\":\"" + userType + "\"," +
        "\"profileImage\":\"" + profileImage + "\"," +
        "\"userRole\":\"" + userRole + "\"," +
        "\"userRole\":\"" + userRole + "\"," +
        "\"fireBaseToken\":\"" + (( fireBaseToken != null ) ? fireBaseToken : "") + "\"," +
        "\"otherInfo\":\"" + ((otherInfo != null) ? otherInfo.toString() : "") + "\"," +
        "\"isLogout\":\"" + ((isLogout != null) ? isLogout.toString() : "") + "\"}";
  }
}