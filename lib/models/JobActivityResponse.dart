import 'package:json_annotation/json_annotation.dart';

class JobActivityResponse {
  String status;
  String message;

  JobActivityResponse({this.status, this.message});

  JobActivityResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }

  @override
  String toString() {
    return "{\"status\":\"" + status + "\"," +
        "\"message\":\"" + message + "\"}";
  }
}
