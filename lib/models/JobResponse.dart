import 'Jobs.dart';

class JobResponse {
  String status;
  String message;
  String authToken;
  int responseCode;
  int isActive;
  List<Jobs> jobs;

  JobResponse({this.status,
        this.message,
        this.authToken,
        this.responseCode,
        this.isActive,
        this.jobs});

  JobResponse.fromJson(Map<String, dynamic> json) {
    if(json['status'] != null){
      status = json['status'];
    }

    message = json['message'];

    authToken = (json['authToken'] != null) ?  json['authToken'] : null;

    if( json['responseCode'] != null ) {
      responseCode = json['responseCode'];
    }

    if( json['isActive'] != null ) {
      isActive = json['isActive'];
    }

    if (json['jobs'] != null) {
      jobs = new List<Jobs>();
      json['jobs'].forEach((v) {
        jobs.add(new Jobs.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(this.status != null) {
      data['status'] = this.status;
    }
    data['message'] = this.message;
    if(this.authToken != null) {
      data['authToken'] = this.authToken;
    }
    if(this.responseCode != null) {
      data['responseCode'] = this.responseCode;
    }
    if(this.isActive != null) {
      data['isActive'] = this.isActive;
    }
    if (this.jobs != null) {
      data['jobs'] = this.jobs.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  String toString() {
    return super.toString();
  }
}
