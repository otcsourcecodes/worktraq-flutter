class JobTypeQuestions {
  String questionId;
  String question;
  String type;
  String options;
  String answerId;
  String answer;

  JobTypeQuestions(
      {this.questionId,
        this.question,
        this.type,
        this.options,
        this.answerId,
        this.answer
      });

  JobTypeQuestions.fromJson(Map<String, dynamic> json) {
    questionId = json['questionId'];
    question = json['question'];
    type = json['type'];
    options = json['options'];
    answerId = json['answerId'];
    answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['questionId'] = this.questionId;
    data['question'] = this.question;
    data['type'] = this.type;
    data['options'] = this.options;
    data['answerId'] = this.answerId;
    data['answer'] = this.answer;
    return data;
  }

  @override
  String toString() {
    return "{\"questionId\":\"" + questionId + "\"," +
        "\"question\":\"" + question + "\"," +
        "\"type\":\"" + type + "\"," +
        "\"options\":\"" + options.replaceAll("\"", "\\\"") + "\"," +
        "\"answerId\":\"" + answerId + "\"," +
        "\"answer\":\"" + answer + "\"}";
  }
}