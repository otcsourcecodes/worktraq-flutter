import 'dart:io';


import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:worktraq/models/ForgotResponse.dart';
import 'package:worktraq/models/JobActivityResponse.dart';
import 'package:worktraq/models/JobDetailResponse.dart';
import 'package:worktraq/models/JobResponse.dart';
import 'package:worktraq/models/JobTrackResponse.dart';
import 'package:worktraq/models/LoginResponse.dart';

part 'RestClient.g.dart';

//@RestApi(baseUrl: "http://eloiapp.com/api/v1/")
//@RestApi(baseUrl: "https://outthinkcoders.com/cgrobinsons/apiv1/") // testing
@RestApi(baseUrl: "http://worktraq.lucidgrp.co.uk/apiv1/") //live
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST("/api/login")
  Future<LoginResponse> login(@Part(name: "email") String email, @Part(name: "password") String password, @Part(name: "deviceType") String deviceType, @Part(name: "deviceToken") String deviceToken );

  @POST("/api/forgotPassword")
  Future<ForgotResponse> forgotPassword(@Part(name: "email") String email);

  /*@GET("/jobs/assignJobs")
  Future<JobResponse> getAllJobs(@Header("authToken") String authToken);
*/
  @GET("/jobs/nearByJobs")
  Future<JobResponse> getAllJobs(@Header("authToken") String authToken);

/*  @GET("/jobs/nearByJobs")
  Future<AuthFailedResponse> getJobList(@Header("authToken") String authToken);*/

  @GET("/jobs/completeJobs")
  Future<JobResponse> getCompleteJob(@Header("authToken") String authToken);

  @POST("/jobs/jobDetail")
  Future<JobDetailResponse> getJobDetailById( @Header("authToken") String authToken, @Part(name: "jobId") id);

  @POST("/jobs/jobActivity")
  @MultiPart()
  Future<JobActivityResponse> updateJobActivity(
      @Header("authToken") String authToken,
      @Part(fileName: "workImage[]", name: "workImage[]") List<File> workImages,
      @Part(fileName: "signature") File signature,
      @Part(name: "jobId") String jobId,
      @Part(name: "jobDateTime") String jobDateTime,
      @Part(name: "jobStatus") String jobStatus,
      @Part(name: "comments") String comments,
      @Part(name: "questionAnswer") String questionAnswer
  );

  @POST("/jobs/jobTracking")
  Future<JobTrackResponse> trackingJobUpdate(@Header("authToken") String authToken, @Part(name: "jobId") String jobId, @Part(name: "latitude") String latitude, @Part(name: "longitude") String longitude);

  @POST("/jobs/driverTracking")
  Future<JobTrackResponse> driverTrackingUpdate(@Header("authToken") String authToken, @Part(name: "latitude") String latitude, @Part(name: "longitude") String longitude);
}