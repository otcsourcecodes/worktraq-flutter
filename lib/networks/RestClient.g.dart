// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RestClient.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    //this.baseUrl ??= 'https://outthinkcoders.com/cgrobinsons/apiv1/';
    this.baseUrl ??= 'http://worktraq.lucidgrp.co.uk/apiv1/';
  }

  final Dio _dio;

  String baseUrl;

  @override
  login(email, password, deviceType, deviceToken) async {
    ArgumentError.checkNotNull(email, 'email');
    ArgumentError.checkNotNull(password, 'password');
    ArgumentError.checkNotNull(deviceType, 'deviceType');
    ArgumentError.checkNotNull(deviceToken, 'deviceToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (email != null) {
      _data.fields.add(MapEntry('email', email));
    }
    if (password != null) {
      _data.fields.add(MapEntry('password', password));
    }
    if (deviceType != null) {
      _data.fields.add(MapEntry('deviceType', deviceType));
    }
    if (deviceToken != null) {
      _data.fields.add(MapEntry('deviceToken', deviceToken));
    }
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/login',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    //print(_data.toString());
    //print(_result.data);
    final value = LoginResponse.fromJson(_result.data);
    return value;
  }

  @override
  forgotPassword(email) async {
    ArgumentError.checkNotNull(email, 'email');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (email != null) {
      _data.fields.add(MapEntry('email', email));
    }
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/api/forgotPassword',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ForgotResponse.fromJson(_result.data);
    return value;
  }

  @override
  getAllJobs(authToken) async {
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/jobs/nearByJobs',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{r'authToken': authToken},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = JobResponse.fromJson(_result.data);
    //print(value.toJson());
    return value;
  }

  @override
  getCompleteJob(authToken) async {
    ArgumentError.checkNotNull(authToken, 'authToken');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/jobs/completeJobs',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{r'authToken': authToken},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = JobResponse.fromJson(_result.data);
    return value;
  }

  @override
  getJobDetailById(authToken, id) async {
    ArgumentError.checkNotNull(authToken, 'authToken');
    ArgumentError.checkNotNull(id, 'id');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    _data.fields.add(MapEntry('jobId', id));
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/jobs/jobDetail',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{r'authToken': authToken},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = JobDetailResponse.fromJson(_result.data);
    return value;
  }

  @override
  updateJobActivity(authToken, workImages, signature, jobId, jobDateTime,
      jobStatus, comments, questionAnswer) async {
    ArgumentError.checkNotNull(authToken, 'authToken');
    ArgumentError.checkNotNull(workImages, 'workImages');
    ArgumentError.checkNotNull(signature, 'signature');
    ArgumentError.checkNotNull(jobId, 'jobId');
    ArgumentError.checkNotNull(jobDateTime, 'jobDateTime');
    ArgumentError.checkNotNull(jobStatus, 'jobStatus');
    ArgumentError.checkNotNull(comments, 'comments');
    ArgumentError.checkNotNull(questionAnswer, 'questionAnswer');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    _data.files.addAll(workImages?.map((i) => MapEntry(
        'workImage[]',
        MultipartFile.fromFileSync(
          i.path,
          filename: i.path.split(Platform.pathSeparator).last,
        ))));
    _data.files.add(MapEntry('signature',
        MultipartFile.fromFileSync(signature.path, filename: 'signature')));
    if (jobId != null) {
      _data.fields.add(MapEntry('jobId', jobId));
    }
    if (jobDateTime != null) {
      _data.fields.add(MapEntry('jobDateTime', jobDateTime));
    }
    if (jobStatus != null) {
      _data.fields.add(MapEntry('jobStatus', jobStatus));
    }
    if (comments != null) {
      _data.fields.add(MapEntry('comments', comments));
    }
    if (questionAnswer != null) {
      _data.fields.add(MapEntry('questionAnswer', questionAnswer));
    }
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/jobs/jobActivity',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{r'authToken': authToken},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = JobActivityResponse.fromJson(_result.data);
    return value;
  }

  @override
  trackingJobUpdate(authToken, jobId, latitude, longitude) async {
    ArgumentError.checkNotNull(authToken, 'authToken');
    ArgumentError.checkNotNull(jobId, 'jobId');
    ArgumentError.checkNotNull(latitude, 'latitude');
    ArgumentError.checkNotNull(longitude, 'longitude');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (jobId != null) {
      _data.fields.add(MapEntry('jobId', jobId));
    }
    if (latitude != null) {
      _data.fields.add(MapEntry('latitude', latitude));
    }
    if (longitude != null) {
      _data.fields.add(MapEntry('longitude', longitude));
    }
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/jobs/jobTracking',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{r'authToken': authToken},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = JobTrackResponse.fromJson(_result.data);
    return value;
  }

  @override
  driverTrackingUpdate(authToken, latitude, longitude) async {
    ArgumentError.checkNotNull(authToken, 'authToken');
    ArgumentError.checkNotNull(latitude, 'latitude');
    ArgumentError.checkNotNull(longitude, 'longitude');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (latitude != null) {
      _data.fields.add(MapEntry('latitude', latitude));
    }
    if (longitude != null) {
      _data.fields.add(MapEntry('longitude', longitude));
    }
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/jobs/driverTracking',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{r'authToken': authToken},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = JobTrackResponse.fromJson(_result.data);
    return value;
  }
}
