import 'package:logger/logger.dart';
import 'package:dio/dio.dart';
import 'RestClient.dart';

class NetworkConfig{
  static final logger = Logger();

  static RestClient restClientInstance(){
    final dio = Dio();
    /*dio.options.connectTimeout = 100000;
    dio.options.receiveTimeout = 100000;*/
    dio.options.sendTimeout = 100000;
    dio.options.headers["Accept"] = "application/json";
    //dio.options.headers["authToken"] = "dea85962b87d5cdf6d9d1af69822161493e1cc17";
    //dio.options.headers["Authorization"] = "Bearer ${AppSharedPreferences.getLoginUser().then((value) => value.token)}";
    final restClient = RestClient(dio);
    //print("after");

    return restClient;
  }
}