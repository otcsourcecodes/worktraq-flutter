import 'dart:math' as math;

import 'package:google_maps_flutter/google_maps_flutter.dart';

class PolygonUtility {
  /**
   * Computes whether the given point lies inside the specified polygon.
   * The polygon is always cosidered closed, regardless of whether the last point equals
   * the first or not.
   * Inside is defined as not containing the South Pole -- the South Pole is always outside.
   * The polygon is formed of great circle segments if geodesic is true, and of rhumb
   * (loxodromic) segments otherwise.
   */
  static bool containsLocation(LatLng point, List<LatLng> polygon, bool geodesic) {
    final int size = polygon.length;
    if (size == 0) {
      return false;
    }
    double lat3 = toRadians(point.latitude);
    double lng3 = toRadians(point.longitude);
    LatLng prev = polygon[size - 1];
    double lat1 = toRadians(prev.latitude);
    double lng1 = toRadians(prev.longitude);
    int nIntersect = 0;
    for (LatLng point2 in polygon) {
      double dLng3 = wrap(lng3 - lng1, - math.pi, math.pi);
      // Special case: point equal to vertex is inside.
      if (lat3 == lat1 && dLng3 == 0) {
        return true;
      }
      double lat2 = toRadians(point2.latitude);
      double lng2 = toRadians(point2.longitude);
      // Offset longitudes by -lng1.
      if (intersects(lat1, lat2, wrap(lng2 - lng1, -math.pi, math.pi), lat3, dLng3, geodesic)) {
        ++nIntersect;
      }
      lat1 = lat2;
      lng1 = lng2;
    }
    return (nIntersect & 1) != 0;
  }

  static double toRadians(double angdeg) {
    return angdeg / 180.0 * math.pi;
  }

  /**
   * Wraps the given value into the inclusive-exclusive interval between min and max.
   * @param n   The value to wrap.
   * @param min The minimum.
   * @param max The maximum.
   */
  static double wrap(double n, double min, double max) {
    return (n >= min && n < max) ? n : (mod(n - min, max - min) + min);
  }

  /**
   * Returns the non-negative remainder of x / m.
   * @param x The operand.
   * @param m The modulus.
   */
  static double mod(double x, double m) {
    return ((x % m) + m) % m;
  }

  /**
   * Computes whether the vertical segment (lat3, lng3) to South Pole intersects the segment
   * (lat1, lng1) to (lat2, lng2).
   * Longitudes are offset by -lng1; the implicit lng1 becomes 0.
   */
  static bool intersects(double lat1, double lat2, double lng2, double lat3, double lng3, bool geodesic) {
    // Both ends on the same side of lng3.
    if ((lng3 >= 0 && lng3 >= lng2) || (lng3 < 0 && lng3 < lng2)) {
      return false;
    }
    // Point is South Pole.
    if (lat3 <= -math.pi/2) {
      return false;
    }
    // Any segment end is a pole.
    if (lat1 <= -math.pi/2 || lat2 <= -math.pi/2 || lat1 >= math.pi/2 || lat2 >= math.pi/2) {
      return false;
    }
    if (lng2 <= -math.pi) {
      return false;
    }
    double linearLat = (lat1 * (lng2 - lng3) + lat2 * lng3) / lng2;
    // Northern hemisphere and point under lat-lng line.
    if (lat1 >= 0 && lat2 >= 0 && lat3 < linearLat) {
      return false;
    }
    // Southern hemisphere and point above lat-lng line.
    if (lat1 <= 0 && lat2 <= 0 && lat3 >= linearLat) {
      return true;
    }
    // North Pole.
    if (lat3 >= math.pi/2) {
      return true;
    }
    // Compare lat3 with latitude on the GC/Rhumb segment corresponding to lng3.
    // Compare through a strictly-increasing function (tan() or mercator()) as convenient.
    return geodesic ?
    math.tan(lat3) >= tanLatGC(lat1, lat2, lng2, lng3) :
    mercator(lat3) >= mercatorLatRhumb(lat1, lat2, lng2, lng3);
  }

  /**
   * Returns tan(latitude-at-lng3) on the great circle (lat1, lng1) to (lat2, lng2). lng1==0.
   * See http://williams.best.vwh.net/avform.htm .
   */
  static double tanLatGC(double lat1, double lat2, double lng2, double lng3) {
    return (math.tan(lat1) * math.sin(lng2 - lng3) + math.tan(lat2) * math.sin(lng3)) / math.sin(lng2);
  }

  /**
   * Returns mercator Y corresponding to latitude.
   * See http://en.wikipedia.org/wiki/Mercator_projection .
   */
  static double mercator(double lat) {
    return math.log(math.tan(lat * 0.5 + math.pi/4));
  }

  /**
   * Returns mercator(latitude-at-lng3) on the Rhumb line (lat1, lng1) to (lat2, lng2). lng1==0.
   */
  static double mercatorLatRhumb(double lat1, double lat2, double lng2, double lng3) {
    return (mercator(lat1) * (lng2 - lng3) + mercator(lat2) * lng3) / lng2;
  }
}
