
import 'package:intl/intl.dart';

class DataTimeUtility{

  static DateFormat dateJobFormat = DateFormat("yyyy-MM-dd hh:mm:ss");
  static DateFormat displayDateJobFormat = DateFormat("dd MMM yyyy");

  static DateFormat dateCompleteJobFormat = DateFormat("yyyy-MM-dd hh:mm:ss a");
  static DateFormat displayCompleteDateJobFormat = DateFormat("dd MMM yyyy hh:mm a");

  /**
   * Convert string time for jobList
   */
  static String convertTimeForJob(String strDate) {
    String formattedDate = null;
    DateTime date = null;
    try {
      date = dateJobFormat.parse(strDate);
      formattedDate = displayDateJobFormat.format(date);
    } catch (e) {
    print("Exception : $e");
    }
    return formattedDate;
  }

  /**
   * Convert string time for complete jobList
   */
   static String convertTimeForCompleteJob(String strDate) {
    String formattedDate = null;
    DateTime date = null;
    try {
      date = dateCompleteJobFormat.parse(strDate);
      formattedDate = displayCompleteDateJobFormat.format(date);
    } catch (e) {
    print("Exception :" + e);
    }
    return formattedDate;
  }
}