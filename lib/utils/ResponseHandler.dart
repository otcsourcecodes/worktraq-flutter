import 'package:worktraq/database/DatabaseHelper.dart';
import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/pages/common/login/LoginScreen.dart';
import 'package:worktraq/pages/common/login/bloc/LoginBloc.dart';
import 'package:worktraq/pages/common/login/bloc/LoginState.dart';
import 'package:worktraq/services/JobTrackingHelper.dart';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'SizeConfig.dart';

class ResponseHandler{

  static void handleResponse(final BuildContext context, AuthFailedResponse authFailedResponse) async{
    try {
      if (authFailedResponse.responseCode != null && authFailedResponse.responseCode == 300) {
        return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10)),
            title: Text(
              "Message",
              style: TextStyle(
                  fontSize: 3 * SizeConfig.textMultiplier,
                  color: Color(0xFF707070)
              ),
              textAlign: TextAlign.center,
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Divider(
                    thickness: 1,
                    height: 1,
                    color: Color(0x50CCCCCC),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                    child: Text(
                      (authFailedResponse.message != null)
                          ? authFailedResponse.message
                          : "NA",
                      style: TextStyle(
                          color: Color(0xFF707070),
                          fontSize: 2 * SizeConfig.textMultiplier
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Divider(
                    thickness: 1,
                    height: 1,
                    color: Color(0x50CCCCCC),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  FlatButton(
                    child: Text(
                      'OK',
                      textAlign: TextAlign.center,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      AppSharedPreferences.clearUserData();
                      DatabaseHelper().clearAllData();
                      JobTrackingHelper().onStop();
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BlocProvider<LoginBloc>(
                                create: (context) => LoginBloc(LoginState.empty()),
                                child: LoginScreen()
                            )
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          );
        });
      }
    }catch(onError) {
      print(onError.toString());
    }
  }

  static bool checkStatusCode(AuthFailedResponse authFailedResponse) {
    bool responseCode = false;
    try {
      if (authFailedResponse.responseCode == 300) {
        responseCode = true;
      }
    } catch (e) {
      print(e.toString());
    }
    return responseCode;
  }

}