import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:image/image.dart' as IM;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'Constants.dart';

Future<String> downloadImage(String url) async {
  await new Future.delayed(new Duration(seconds: 1));
  /*bool checkResult = await SimplePermissions.checkPermission(Permission.WriteExternalStorage);
    if (!checkResult) {
      print("permission not given.");
      var status = await SimplePermissions.requestPermission(Permission.WriteExternalStorage);
      if (status == PermissionStatus.authorized) {
        print("permission given granted.");
        var res = await saveImage(url);
        return res.path;
        //return res != null;
      }
    } else {
      print("permission already given.");*/
  var res = await saveImage(url);
  return res?.path;
  //return res != null;
  /*}
    return null;*/
}

Future<File> saveImage(String url) async {
  try {
    String path = Uri.parse(url).path;
    String fileName = p.basename(path);
    String imageFileName = p.basenameWithoutExtension(path);
    String fileExtension = p.extension(path);

    if((fileExtension.length == 0) && (fileName.length > 0 )) {
      if (fileName.contains("."))
        imageFileName = fileName + "png";
      else{
        if(fileName == "staticmap") {
          imageFileName = fileName + "_" + DateTime.now().microsecondsSinceEpoch.toString() + ".png";
        } else
          imageFileName = fileName +  ".png";
      }
    }
    //print("fileName nkp: $imageFileName fileExtension : $fileExtension");

    final file = await getImageFromNetwork(url);
    Directory dir;
    if (Platform.isAndroid) {
      dir = await getExternalStorageDirectory();
    } else {
      dir = await getApplicationDocumentsDirectory();
    }

    var localdir = await new Directory('${dir.path}${Constants.LOCAL_FILE_STORAGE_DIRECTORY}').create(recursive: true);
    IM.Image image = IM.decodeImage(file.readAsBytesSync());
    return new File('${localdir.path}/${imageFileName}')..writeAsBytesSync(IM.encodePng(image));
  } catch (e) {
    print(e);
    return null;
  }
}

Future<File> getImageFromNetwork(String url) async {
  File file = await DefaultCacheManager().getSingleFile(url);
  return file;
}