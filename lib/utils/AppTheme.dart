import 'dart:ui';

import 'package:flutter/material.dart';


import 'SizeConfig.dart';

class AppTheme {
  AppTheme._();

  static const Color appBackgroundColor = Color(0xFFFFFFFF);
  static const Color backgroundAppColor = Color(0xFFECECEC);
  static const Color topBarBackgroundColor = Color(0xFF244764);
  static const Color primaryColor = Color(0xFF244764);
  static const Color primaryDarkColor = Color(0xFF1E3B54);
  static const Color colorAccent = Color(0xFF244764);
  static const Color selectedTabBackgroundColor = Color(0xFF287FBB);
  static const Color unSelectedTabBackgroundColor = Color(0xFFFFFFFC);
  static const Color subTitleTextColor = Color(0xFF9F988F);
  static const Color gradientTopColor = Color(0xFF5B9DCB);
  static const Color gradientBottomColor = Color(0xFF3A739B);
  static const Color colorPriorityLow = Color(0xFF57889C);
  static const Color colorPriorityMedium = Color(0xFFD5804D);
  static const Color colorPriorityHigh = Color(0xFFA90329);
  static const Color colorCompleteJobStatus = Color(0xFF62B466);
  static const Color colorViewNewJob = Color(0xFF00A03F);
  static const Color colorTextGrey = Color(0xFF727272);
  static const Color colorTextJobDetail = Color(0xFF393939);
  static const Color colorDivider = Color(0xFFDDDCDC);
  static const Color colorText = Color(0xFF686565);

  static final ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: AppTheme.appBackgroundColor,
    accentColor: AppTheme.colorAccent,
    primaryColor: AppTheme.primaryColor,
    primaryColorDark: AppTheme.primaryDarkColor,
    brightness: Brightness.light,
    textTheme: lightTextTheme,
    fontFamily: 'Segoeui'
  );

  static final ThemeData darkTheme = ThemeData(
    scaffoldBackgroundColor: Colors.black,
    brightness: Brightness.dark,
    textTheme: darkTextTheme,
  );

  static final TextTheme lightTextTheme = TextTheme(
    title: _titleLight,
    subtitle: _subTitleLight,
    button: _buttonLight,
    display1: _greetingLight,
    display2: _searchLight,
    body1: _selectedTabLight,
    body2: _unSelectedTabLight,
  );

  static final TextTheme darkTextTheme = TextTheme(
    title: _titleDark,
    subtitle: _subTitleDark,
    button: _buttonDark,
    display1: _greetingDark,
    display2: _searchDark,
    body1: _selectedTabDark,
    body2: _unSelectedTabDark,
  );

  static final TextStyle _titleLight = TextStyle(
    color: Colors.black,
    fontSize: 3.5 * SizeConfig.textMultiplier,
  );

  static final TextStyle _subTitleLight = TextStyle(
    color: subTitleTextColor,
    fontSize: 2 * SizeConfig.textMultiplier,
    height: 1.5,
  );

  static final TextStyle _buttonLight = TextStyle(
    color: Colors.black,
    fontSize: 2.5 * SizeConfig.textMultiplier,
  );

  static final TextStyle _greetingLight = TextStyle(
    color: Colors.black,
    fontSize: 2.0 * SizeConfig.textMultiplier,
  );

  static final TextStyle _searchLight = TextStyle(
    color: Colors.black,
    fontSize: 2.3 * SizeConfig.textMultiplier,
  );

  static final TextStyle _selectedTabLight = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 2 * SizeConfig.textMultiplier,
  );

  static final TextStyle _unSelectedTabLight = TextStyle(
    color: Colors.grey,
    fontSize: 2 * SizeConfig.textMultiplier,
  );

  static final TextStyle _titleDark = _titleLight.copyWith(color: Colors.white);

  static final TextStyle _subTitleDark = _subTitleLight.copyWith(color: Colors.white70);

  static final TextStyle _buttonDark = _buttonLight.copyWith(color: Colors.black);

  static final TextStyle _greetingDark = _greetingLight.copyWith(color: Colors.black);

  static final TextStyle _searchDark = _searchDark.copyWith(color: Colors.black);

  static final TextStyle _selectedTabDark = _selectedTabDark.copyWith(color: Colors.white);

  static final TextStyle _unSelectedTabDark = _selectedTabDark.copyWith(color: Colors.white70);
}
