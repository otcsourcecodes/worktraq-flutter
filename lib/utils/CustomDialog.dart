
import 'package:flutter/material.dart';

import 'SizeConfig.dart';

Future<void> showErrorDialog(BuildContext context, String errorTitle, String errorMessage) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10)),
        title: Text(
          errorTitle,
          style: TextStyle(
              fontSize: 3 * SizeConfig.textMultiplier,
              color: Color(0xFF707070)
          ),
          textAlign: TextAlign.center,
        ),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Divider(
                thickness: 1,
                height: 1,
                color: Color(0x50CCCCCC),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Text(
                  errorMessage,
                  style: TextStyle(
                    color: Color(0xFF707070),
                    fontSize: 2 * SizeConfig.textMultiplier
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Divider(
                thickness: 1,
                height: 1,
                color: Color(0x50CCCCCC),
              ),
              SizedBox(
                height: 10,
              ),
              FlatButton(
                child: Text(
                  'OK',
                  textAlign: TextAlign.center,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
      );
    },
  );
}