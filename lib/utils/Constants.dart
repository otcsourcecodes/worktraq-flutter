
class Constants{
   static const String IMAGE_PATH = "assets/images/";
   static const String MESSAGE = "message";
   static const String JOB_START = "start";
   static const String JOB_END = "end";
   static const String JOB_OPEN = "open";
   static const String SIGNATURE_IMAGE_DIRECTORY = "Worktraq/signature";
   static const String LOCAL_FILE_STORAGE_DIRECTORY = "/Worktraq/localStorage";
   static const String LOCAL_PDF_FILE_STORAGE_DIRECTORY = "/Worktraq/localStorage/pdf";
   static const String LOGOUT = "Logout";
   static const API_KEY = "AIzaSyDjhKBJtoevmCuR5iD1El6cuDHTMByw9Co";

   static final String IS_USER_LOGGED_IN = "IsLoggedIn";
   static final String EMAIL = "email";
   static final String AUTH_TOKEN = "authToken";
   static final String ID = "id";
   static final String FULLNAME = "fullName";
   static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
   static final String USER_ROLE = "userRole";
   static final String USER_TYPE = "userType";
   static final String PROFILE_IMAGE = "profileImage";
   static final String IN_PROGRESS = "inProgress";
   static final String FIRST_TIME_DATA_SAVE_IN_LOCAL = "firstTimeDataSaveInLocal";
   static final String FIRST_TIME_DATA_SAVE_IN_LOCAL_COMPLETE = "firstTimeDataSaveInLocalComplete";

   static const List<String> choices = <String>[
      LOGOUT,
   ];
}