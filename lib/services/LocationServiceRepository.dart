import 'dart:async';
import 'dart:isolate';
import 'dart:math';
import 'dart:ui';

import 'package:background_locator/location_dto.dart';
import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/utils/PolygonUtility.dart';
import 'package:worktraq/utils/ResponseHandler.dart';
import 'package:dio/dio.dart' hide Response;
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


import 'FileManager.dart';

class LocationServiceRepository {
  static LocationServiceRepository _instance = LocationServiceRepository._();

  LocationServiceRepository._();

  factory LocationServiceRepository() {
    return _instance;
  }

  static const String isolateName = 'LocatorIsolate';

  int _count = -1;
  String _jobId = "";
  String _authToken = "";
  BuildContext _context;
  List<LatLng> _pointList = List();
  String _points;
  bool isEnterPolygon = false;

  Future<void> init(Map<dynamic, dynamic> params) async {
    print("***********Init callback handler");
    if (params.containsKey('countInit')) {
      dynamic tmpCount = params['countInit'];
      if (tmpCount is double) {
        _count = tmpCount.toInt();
      } else if (tmpCount is String) {
        _count = int.parse(tmpCount);
      } else if (tmpCount is int) {
        _count = tmpCount;
      } else {
        _count = -2;
      }
    } else {
      _count = 0;
    }

    if(params.containsKey("jobId")){
      dynamic jobId = params['jobId'];
      if(jobId is String){
        _jobId = jobId;
      }
    }

    if(params.containsKey("authToken")){
      dynamic authToken = params['authToken'];
      if(authToken is String){
        _authToken = authToken;
      }
    }

    if(params.containsKey("context")){
      dynamic context = params['context'];
      if(context is BuildContext){
        _context = context;
      }
    }

    if(params.containsKey("isEnterPolygon")){
      dynamic _isEnterPolygon = params['isEnterPolygon'];
      if(_isEnterPolygon is bool){
        if(_isEnterPolygon == true)
          isEnterPolygon = true;
        else
          isEnterPolygon = false;
      }
    }

    if(params.containsKey("points")){
      dynamic points = params['points'];
      if(points is String){
        _points = points;

        points = points.substring(0, points.length-1);
        var pointArray = points.split("|");

        print(pointArray);
        for(int i = 0; i < pointArray.length; i++){
          var point = pointArray[i].split(",");
          LatLng location = LatLng(double.parse(point[0]), double.parse(point[1]));
          _pointList.add(location);
          print("i = ${pointArray[i]}");
        }
      }
    }

    print("$_count");
    //await setLogLabel("start");
    final SendPort send = IsolateNameServer.lookupPortByName(isolateName);
    send?.send(null);
  }

  Future<void> dispose() async {
    print("***********Dispose callback handler");
    print("$_count");
    //await setLogLabel("end");
    final SendPort send = IsolateNameServer.lookupPortByName(isolateName);
    send?.send(null);
  }

  Future<void> callback(LocationDto locationDto) async {
    //print('$_count location in dart: ${locationDto.toString()} jobId: $_jobId authToken : $_authToken');
    //print('$_count location in dart: ${locationDto.toString()} jobId: $_jobId authToken : $_authToken pointList: $_pointList');
    if(PolygonUtility.containsLocation(LatLng(locationDto.latitude, locationDto.longitude), _pointList, true)){
      //isEnterPolygon = true;
      if(isEnterPolygon) {
        //Toast.makeText(context, "Inside Location", Toast.LENGTH_SHORT).show();
        isEnterPolygon = false;
        jobTrackingUpdate(locationDto.latitude.toString(), locationDto.longitude.toString());
        //showToast("Enter in polygon", gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
        print("Enter in polygon");
      }
    } else{
      if(!isEnterPolygon) {
        //Toast.makeText(context, "Outside Location", Toast.LENGTH_SHORT).show();
        isEnterPolygon = true;
        jobTrackingUpdate(locationDto.latitude.toString(), locationDto.longitude.toString());
        //showToast("Exit in polygon", gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
        print("Exit in polygon");
      }
    }
    //await setLogPosition(_count, locationDto);
    final SendPort send = IsolateNameServer.lookupPortByName(isolateName);
    send?.send(locationDto);
    _count++;
  }

  /*static Future<void> setLogLabel(String label) async {
    final date = DateTime.now();
    await FileManager.writeToLogFile('------------\n$label: ${formatDateLog(date)}\n------------\n');
  }

  static Future<void> setLogPosition(int count, LocationDto data) async {
    final date = DateTime.now();
    await FileManager.writeToLogFile('$count : ${formatDateLog(date)} --> ${formatLog(data)} --- isMocked: ${data.isMocked}\n');
  }*/

  static double dp(double val, int places) {
    double mod = pow(10.0, places);
    return ((val * mod).round().toDouble() / mod);
  }

  static String formatDateLog(DateTime date) {
    return date.hour.toString() +
        ":" +
        date.minute.toString() +
        ":" +
        date.second.toString();
  }

  static String formatLog(LocationDto locationDto) {
    return dp(locationDto.latitude, 4).toString() + ", " + dp(locationDto.longitude, 4).toString();
  }

  Future<void> jobTrackingUpdate(String latitude, String longitude) async {
    //SharedPreferences.setMockInitialValues({});
    if(_authToken != null){
      NetworkConfig.restClientInstance().trackingJobUpdate(_authToken, _jobId, latitude, longitude).then((result) {
        if (result.status == "success") {
          //Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
          print("Message " + result.message);
        }else{
          print("Error " + result.message);
        }
        //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
      }).catchError((Object obj){
        switch (obj.runtimeType) {
          case DioError:
            final res = (obj as DioError).response;
            // Here's the sample to get the failed response error code and message
            print("${res.data.toString()} ");
            if(res.data != null) {
              AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res.data);
              print("${res.data.toString()} ");
              if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                ResponseHandler.handleResponse(_context, authFailedResponse);
              }
            } else {
              NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
            }
            break;
          default:
            NetworkConfig.logger.e("Got error : ${obj.toString()}");
            break;
        }
      });
    }
  }
}
