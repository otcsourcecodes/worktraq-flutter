
import 'package:worktraq/services/DriverTrackingCallbackHandler.dart';
import 'package:driver_background_locator/driver_background_locator.dart';
import 'package:driver_background_locator/location_dto.dart';
import 'package:driver_background_locator/settings/android_settings.dart';
import 'package:driver_background_locator/settings/ios_settings.dart';
import 'package:driver_background_locator/settings/locator_settings.dart';
import 'package:flutter/material.dart';
import 'package:location_permissions/location_permissions.dart';

class DriverTrackingHelper{
  static bool isDriverRunning;
  static LocationDto lastDriverLocation;
  final BuildContext context;
  final String authToken;

  DriverTrackingHelper({this.context, this.authToken});

  Future<void> initPlatformState() async {
    print('Initializing...');
    await DriverBackgroundLocator.initializeDriver();
    print('Initialization done');
    final _isRunning = await DriverBackgroundLocator.isRegisterLocationUpdateDriver();
    isDriverRunning = _isRunning;
    /*setState(() {
      isRunning = _isRunning;
    });*/
    print('Driver Running ${isDriverRunning.toString()}');
  }

  void onStop() async{
    bool isPermission = await _checkLocationPermission();
    if (isPermission != null && isPermission) {
      DriverBackgroundLocator.unRegisterLocationUpdateDriver();
      isDriverRunning = false;
      /*setState(() {
        isRunning = false;
//      lastTimeLocation = null;
//      lastLocation = null;
    });*/
    }
  }

  void onStart() async {
    bool check = await _checkLocationPermission();
    if (check != null && check) {
      _startLocator();
      final _isRunning = await DriverBackgroundLocator.isServiceRunningDriver();
      isDriverRunning = _isRunning;
      lastDriverLocation = null;
      /*setState(() {
        isRunning = true;
        //lastTimeLocation = null;
        lastLocation = null;
      });*/
    } else {
      // show error
    }
  }

  Future<bool> _checkLocationPermission() async {
    final access = await LocationPermissions().checkPermissionStatus();
    switch (access) {
      case PermissionStatus.unknown:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
        Future.delayed(Duration(seconds: 4), () async{
          final permission = await LocationPermissions().requestPermissions(
            permissionLevel: LocationPermissionLevel.locationAlways,
          );
          if (permission == PermissionStatus.granted) {
            return true;
          } else {
            return false;
          }
        });
        break;
      case PermissionStatus.granted:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  void _startLocator() {
    print("nkp driver tracking start service");
    Map<String, dynamic> data;
    if(authToken != null){
      data = {'countInit': 1, 'id' : '', 'authToken' : authToken, 'context': context};
    } else{
      data = {'countInit': 1};
    }
   // print("k authTOken $authToken");
    DriverBackgroundLocator.registerLocationUpdateDriver(
        DriverTrackingCallbackHandler.callback,
        initCallback: DriverTrackingCallbackHandler.initCallback,
        initDataCallback: data,
/*
        Comment initDataCallback, so service not set init variable,
        variable stay with value of last run after unRegisterLocationUpdate
 */
        disposeCallback: DriverTrackingCallbackHandler.disposeCallback,
        iosSettings: IOSSettings(
            accuracy: LocationAccuracy.NAVIGATION, distanceFilter: 0),
        autoStop: false,
        androidSettings: AndroidSettings(
            accuracy: LocationAccuracy.NAVIGATION,
            interval: 5,
            distanceFilter: 0,
            androidNotificationSettings: AndroidNotificationSettings(
                notificationChannelName: 'Location tracking',
                notificationTitle: 'Driver Location Notification',
                notificationMsg: 'Driver location service running in the background.d',
                notificationBigMsg: '',
                notificationIcon: 'worktraq_logo',
                notificationIconColor: Colors.grey,
                notificationTapCallback:
                DriverTrackingCallbackHandler.notificationCallback
            )
          )
        );
      }
}