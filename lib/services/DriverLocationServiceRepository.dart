import 'dart:isolate';
import 'dart:math';
import 'dart:ui';
import 'dart:async';

import 'package:worktraq/models/AuthFailedResponse.dart';
import 'package:worktraq/networks/NetworkConfig.dart';
import 'package:worktraq/utils/ResponseHandler.dart';
import 'package:dio/dio.dart' hide Response;
import 'package:driver_background_locator/location_dto.dart';

import 'FileManager.dart';

class DriverLocationServiceRepository{
  static DriverLocationServiceRepository _instance = DriverLocationServiceRepository._();

  DriverLocationServiceRepository._();

  factory DriverLocationServiceRepository() {
    return _instance;
  }

  static const String isolateName = 'DriverLocatorIsolate';

  int _count = -1;
  String _authToken = "";

  Future<void> init(Map<dynamic, dynamic> params) async {
    print("***********Init callback handler");
    if (params.containsKey('countInit')) {
      dynamic tmpCount = params['countInit'];
      if (tmpCount is double) {
        _count = tmpCount.toInt();
      } else if (tmpCount is String) {
        _count = int.parse(tmpCount);
      } else if (tmpCount is int) {
        _count = tmpCount;
      } else {
        _count = -2;
      }
    } else {
      _count = 0;
    }

    if (params.containsKey('authToken')) {
      dynamic authToken = params['authToken'];
      if(authToken is String){
        _authToken = authToken.toString();
      }
      //print("nkp init authToken : ${_authToken}");
    }
    print("$_count");
    //await setLogLabel("start");
    final SendPort send = IsolateNameServer.lookupPortByName(isolateName);
    send?.send(null);
  }

  Future<void> dispose() async {
    print("***********Dispose callback handler");
    print("$_count");
    //await setLogLabel("end");
    final SendPort send = IsolateNameServer.lookupPortByName(isolateName);
    send?.send(null);
  }

  Future<void> callback(LocationDto locationDto) async {
    //print('$_count location in dart: ${locationDto.toString()} ');
   // print("nkp driver : lat :- ${locationDto.latitude} lng :- ${locationDto.longitude} authToken $_authToken");

    driverTrackingUpdate(locationDto.latitude.toString(), locationDto.longitude.toString());
    //await setLogPosition(_count, locationDto);
    final SendPort send = IsolateNameServer.lookupPortByName(isolateName);
    send?.send(locationDto);
    _count++;
  }

  /*static Future<void> setLogLabel(String label) async {
    final date = DateTime.now();
    await FileManager.writeToLogFile('------------\n$label: ${formatDateLog(date)}\n------------\n');
  }

  static Future<void> setLogPosition(int count, LocationDto data) async {
    final date = DateTime.now();
    await FileManager.writeToLogFile('$count : ${formatDateLog(date)} --> ${formatLog(data)} --- isMocked: ${data.isMocked}\n');
  }*/

  static double dp(double val, int places) {
    double mod = pow(10.0, places);
    return ((val * mod).round().toDouble() / mod);
  }

  static String formatDateLog(DateTime date) {
    return date.hour.toString() +
        ":" +
        date.minute.toString() +
        ":" +
        date.second.toString();
  }

  static String formatLog(LocationDto locationDto) {
    return dp(locationDto.latitude, 4).toString() + ", " + dp(locationDto.longitude, 4).toString();
  }

  Future<void> driverTrackingUpdate(String latitude, String longitude) async {
    //print("_authToken: $_authToken");

    //SharedPreferences.setMockInitialValues({});
    if(_authToken != null){
      NetworkConfig.restClientInstance().driverTrackingUpdate(_authToken, latitude, longitude).then((result) {
        if (result.status == "success") {
          //Navigator.of(_globalKey.currentContext,rootNavigator: true).pop();
          //print("Message " + result.message);
        }else{
          print("Error " + result.message);
        }
        //NetworkConfig.logger.i("result nkp : status - ${result.status} Message - ${result.message}");
      }).catchError((Object obj){
        switch (obj.runtimeType) {
          case DioError:
            final res = (obj as DioError).response;
            // Here's the sample to get the failed response error code and message
            print("res ${res?.data?.toString()}");
            if(res?.data != null) {
              AuthFailedResponse authFailedResponse = AuthFailedResponse.fromJson(res.data);
              if (ResponseHandler.checkStatusCode(authFailedResponse)) {
                ResponseHandler.handleResponse(null, authFailedResponse);
              }
            } else {
              if(res != null)
                NetworkConfig.logger.e("Got error : ${res.statusCode} -> ${res.statusMessage} -> ${res.data}");
            }
            break;
          default:
            NetworkConfig.logger.e("Got error : ${obj.toString()}");
            break;
        }
      });
    }
  }
}