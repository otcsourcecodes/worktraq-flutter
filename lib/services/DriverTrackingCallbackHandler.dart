import 'dart:async';
import 'package:worktraq/sessions/AppSharedPreferences.dart';
import 'package:dio/dio.dart' hide Response;
import 'package:driver_background_locator/location_dto.dart';
import 'package:flutter/material.dart';

import 'DriverLocationServiceRepository.dart';

class DriverTrackingCallbackHandler{

  final BuildContext context;
  static String authToken;

  DriverTrackingCallbackHandler({this.context, authToken});

  static Future<void> initCallback(Map<dynamic, dynamic> params) async {
    DriverLocationServiceRepository driverLocationCallbackRepository = DriverLocationServiceRepository();
    await driverLocationCallbackRepository.init(params);
  }

  static Future<void> disposeCallback() async {
    DriverLocationServiceRepository driverLocationCallbackRepository = DriverLocationServiceRepository();
    await driverLocationCallbackRepository.dispose();
  }

  static Future<void> callback(LocationDto locationDto) async {
    DriverLocationServiceRepository driverLocationCallbackRepository = DriverLocationServiceRepository();
    await driverLocationCallbackRepository.callback(locationDto);
  }

  static Future<void> notificationCallback() async {
    print('*** driver notificationCallback');
  }
}