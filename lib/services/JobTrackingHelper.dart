import 'package:background_locator/background_locator.dart';
import 'package:background_locator/location_dto.dart';
import 'package:background_locator/settings/android_settings.dart';
import 'package:background_locator/settings/ios_settings.dart';
import 'package:background_locator/settings/locator_settings.dart';
import 'package:flutter/material.dart';
import 'package:location_permissions/location_permissions.dart';

import 'LocationCallbackHandler.dart';

class JobTrackingHelper{
  static bool isRunning;
  static LocationDto lastLocation;
  final BuildContext context;
  final String jobId;
  final String authToken;
  final String points;
  final bool isEnterPolygon;

  JobTrackingHelper({this.context, this.jobId, this.authToken, this.points, this.isEnterPolygon});

  Future<void> initPlatformState() async {
    print('Initializing...');
    await BackgroundLocator.initialize();
    print('Initialization done');
    final _isRunning = await BackgroundLocator.isRegisterLocationUpdate();
    isRunning = _isRunning;
    /*setState(() {
      isRunning = _isRunning;
    });*/
    print('Running ${isRunning.toString()}');
  }

  void onStop() async {
    BackgroundLocator.unRegisterLocationUpdate();
    final _isRunning = await BackgroundLocator.isServiceRunning();

    //setState(() {
      isRunning = _isRunning;
//      lastTimeLocation = null;
//      lastLocation = null;
    //});
  }

  void onStart() async {
    if (await _checkLocationPermission()) {
      _startLocator();
      final _isRunning = await BackgroundLocator.isServiceRunning();
      isRunning = _isRunning;
      lastLocation = null;
      /*setState(() {
        isRunning = true;
        //lastTimeLocation = null;
        lastLocation = null;
      });*/
    } else {
      // show error
    }
  }

  Future<bool> _checkLocationPermission() async {
    final access = await LocationPermissions().checkPermissionStatus();
    switch (access) {
      case PermissionStatus.unknown:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
        final permission = await LocationPermissions().requestPermissions(
          permissionLevel: LocationPermissionLevel.locationAlways,
        );
        if (permission == PermissionStatus.granted) {
          return true;
        } else {
          return false;
        }
        break;
      case PermissionStatus.granted:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  void _startLocator() {
    //print("nkp  start service $points");
    //Map<String, dynamic> data = {'countInit': 1, 'jobId': jobId, 'authToken': authToken, 'points' : points};
    Map<String, dynamic> data = {'countInit': 1, 'jobId': jobId, 'authToken': authToken, 'points' : points, 'isEnterPolygon': isEnterPolygon};
    BackgroundLocator.registerLocationUpdate(
      LocationCallbackHandler.callback,
      initCallback: LocationCallbackHandler.initCallback,
      initDataCallback: data,
/*
        Comment initDataCallback, so service not set init variable,
        variable stay with value of last run after unRegisterLocationUpdate
 */
      disposeCallback: LocationCallbackHandler.disposeCallback,
        iosSettings: IOSSettings(
            accuracy: LocationAccuracy.NAVIGATION, distanceFilter: 0),
        autoStop: false,
        androidSettings: AndroidSettings(
            accuracy: LocationAccuracy.NAVIGATION,
            interval: 5,
            distanceFilter: 0,
            androidNotificationSettings: AndroidNotificationSettings(
                notificationChannelName: 'channel_location_job',
                notificationTitle: 'Job Running...',
                notificationMsg: 'Track job location in background.',
                notificationBigMsg: '',
                notificationIcon: 'worktraq_logo',
                notificationIconColor: Colors.grey,
                notificationTapCallback:
                LocationCallbackHandler.notificationCallback)
        )
    );
  }
}