import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:worktraq/models/AfterWork.dart';
import 'package:worktraq/models/BeforeWork.dart';
import 'package:worktraq/models/JobReport.dart';
import 'package:worktraq/models/JobTypeQuestions.dart';
import 'package:worktraq/models/Jobs.dart';
import 'package:worktraq/models/Users.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  // Database Name
  static final String DATABASE_NAME = "worktraq.db";

  // User table name
  static final String TABLE_USER = "user";

  // Jobs table name
  static final String TABLE_JOBS = "jobs";

  // User Table Columns names
  static final String ID = "id";
  static final String USER_ID = "userId";
  static final String FULL_NAME = "fullName";
  static final String EMAIL = "email";
  static final String AUTH_TOKEN = "authToken";
  static final String PROFILE_IMAGE = "profileImage";
  static final String USER_TYPE = "userType";
  static final String USER_ROLE = "userRole";
  static final String FIREBASE_TOKEN = "fireBaseToken";
  static final String OTHER_INFO = "otherInfo";
  static final String LOGOUT = "logout";

  // Job table column name
  static final String JOB_ID = "jobId";
  static final String JOB_NAME = "jobName";
  static final String JOB_TYPE_ID = "jobTypeId";
  static final String JOB_TYPE = "jobType";
  static final String JOB_DRIVER_ID = "driverId";
  static final String JOB_DRIVER_NAME = "driverName";
  static final String JOB_CUSTOMER_ID = "customerId";
  static final String JOB_CUSTOMER_NAME = "customerName";
  static final String JOB_STATUS = "jobStatus";
  static final String JOB_STATUS_SHOW = "statusShow";
  static final String JOB_START_DATE = "startDate";
  static final String JOB_START_TIME = "startTime";
  static final String JOB_ADDRESS = "address";
  static final String JOB_STREET = "street";
  static final String JOB_STREET2 = "street2";
  static final String JOB_CITY = "city";
  static final String JOB_STATE = "state";
  static final String JOB_ZIP = "zip";
  static final String JOB_COUNTRY = "country";
  static final String JOB_LATITUDE = "latitude";
  static final String JOB_LONGITUDE = "longitude";
  // static final String JOB_REPORT = "jobReport";
  static final String JOB_BEFORE_WORK_START_DATETIME = "beforeWork_startDateTime";
  static final String JOB_BEFORE_WORK_COMMENTS = "beforeWork_comments";
  static final String JOB_BEFORE_WORK_DRIVER_SIGNATURE = "beforeWork_driverSignature";
  static final String JOB_BEFORE_WORK_IMAGE1 = "beforeWork_Image1";
  static final String JOB_BEFORE_WORK_IMAGE2 = "beforeWork_Image2";
  static final String JOB_BEFORE_WORK_IMAGE3 = "beforeWork_Image3";
  static final String JOB_AFTER_WORK_END_DATETIME = "afterWork_endDateTime";
  static final String JOB_AFTER_WORK_COMMENTS = "afterWork_comments";
  static final String JOB_AFTER_WORK_CUSTOMER_SIGNATURE = "afterWork_customerSignature";
  static final String JOB_AFTER_WORK_IMAGE1 = "afterWork_image1";
  static final String JOB_AFTER_WORK_IMAGE2 = "afterWork_image2";
  static final String JOB_AFTER_WORK_IMAGE3 = "afterWork_image3";
  static final String JOB_GEO_FENCING = "geoFencing";
  static final String JOB_POINTS = "points";
  static final String JOB_POLYGON_COLOR = "polygon_color";
  static final String JOB_CRD = "crd";
  static final String JOB_UPD = "upd";
  static final String JOB_PRIORITY = "jobPriority";
  static final String JOB_WORK_PRIORITY = "jobWorkPriority";
  static final String JOB_DISTANCE = "jobDistance";
  static final String JOB_TIME_DURATION = "timeDuration";
  static final String JOB_GEO_FENCING_URL = "geoFencingUrl";
  static final String JOB_GENERATE_PDF = "generatePdf";
  static final String JOB_TYPE_QUESTIONS = "jobTypeQuestions";
  static final String JOB_TYPE_QUESTION_ANSWERS = "jobTypeQuestionAnswers";
  static final String LOCAL_UPDATED_STATUS = "updatedStatus";
  static final String LOCAL_IMAGE_UPDATED_STATUS = "imageUpdatedStatus";


  Future<Database> get db async{
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async{
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();

    //Sqflite.devSetDebugModeOn(true);
    String path = join(documentDirectory.path, DATABASE_NAME);
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);

    return theDb;
  }

  void _onCreate(Database db, int version) async {
    String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
        + ID + " INTEGER, "
        + USER_ID + " TEXT, "
        + FULL_NAME + " TEXT, "
        + EMAIL + " TEXT, "
        + PROFILE_IMAGE + " TEXT, "
        + USER_TYPE + " INTEGER, "
        + USER_ROLE + " TEXT, "
        + AUTH_TOKEN + " TEXT, "
        + FIREBASE_TOKEN + " TEXT, "
        + OTHER_INFO + " TEXT, "
        + LOGOUT + " BOOLEAN )";
    // When creating the db, create the table
    await db.execute(CREATE_USER_TABLE);

    String CREATE_JOBS_TABLE = "CREATE TABLE " + TABLE_JOBS + "("
        + ID + " INTEGER, "
        + JOB_ID + " TEXT, "
        + JOB_NAME + " TEXT, "
        + JOB_TYPE_ID + " INTEGER, "
        + JOB_TYPE + " TEXT, "
        + JOB_DRIVER_ID + " INTEGER, "
        + JOB_DRIVER_NAME + " TEXT, "
        + JOB_CUSTOMER_ID + " INTEGER, "
        + JOB_CUSTOMER_NAME + " TEXT, "
        + JOB_STATUS + " INTEGER, "
        + JOB_STATUS_SHOW + " TEXT, "
        + JOB_START_DATE + " TEXT, "
        + JOB_START_TIME + " TEXT, "
        + JOB_ADDRESS + " TEXT, "
        + JOB_STREET + " TEXT, "
        + JOB_STREET2 + " TEXT, "
        + JOB_CITY + " TEXT, "
        + JOB_STATE + " TEXT, "
        + JOB_ZIP + " TEXT, "
        + JOB_COUNTRY + " TEXT, "
        + JOB_LATITUDE + " TEXT, "
        + JOB_LONGITUDE + " TEXT, "
        //+ JOB_REPORT + " TEXT, "
        + JOB_BEFORE_WORK_START_DATETIME + " TEXT, "
        + JOB_BEFORE_WORK_COMMENTS + " TEXT, "
        + JOB_BEFORE_WORK_DRIVER_SIGNATURE + " TEXT, "
        + JOB_BEFORE_WORK_IMAGE1 + " TEXT, "
        + JOB_BEFORE_WORK_IMAGE2 + " TEXT, "
        + JOB_BEFORE_WORK_IMAGE3 + " TEXT, "
        + JOB_AFTER_WORK_END_DATETIME + " TEXT, "
        + JOB_AFTER_WORK_COMMENTS + " TEXT, "
        + JOB_AFTER_WORK_CUSTOMER_SIGNATURE + " TEXT, "
        + JOB_AFTER_WORK_IMAGE1 + " TEXT, "
        + JOB_AFTER_WORK_IMAGE2 + " TEXT, "
        + JOB_AFTER_WORK_IMAGE3 + " TEXT, "
        + JOB_GEO_FENCING + " TEXT, "
        + JOB_POINTS + " TEXT, "
        + JOB_POLYGON_COLOR + " TEXT, "
        + JOB_CRD + " TEXT, "
        + JOB_UPD + " TEXT, "
        + JOB_PRIORITY + " TEXT, "
        + JOB_WORK_PRIORITY + " TEXT, "
        + JOB_DISTANCE + " TEXT, "
        + JOB_TIME_DURATION + " TEXT, "
        + JOB_GEO_FENCING_URL + " TEXT, "
        + JOB_GENERATE_PDF + " TEXT,"
        + JOB_TYPE_QUESTIONS + " TEXT,"
        + JOB_TYPE_QUESTION_ANSWERS + " TEXT,"
        + LOCAL_IMAGE_UPDATED_STATUS + " TEXT,"
        + LOCAL_UPDATED_STATUS + " TEXT)";

    await db.execute(CREATE_JOBS_TABLE);
  }

  // Adding new user
  Future<int> addUser(Users user) async{
    var dbClient = await db;
    Map<String, dynamic> userMap = Map();
    userMap[ID] = user.id;
    userMap[USER_ID] = user.userId;
    userMap[FULL_NAME] = user.fullName;
    userMap[EMAIL] = user.email;
    userMap[PROFILE_IMAGE] = user.profileImage;
    userMap[USER_TYPE] = user.userType;
    userMap[USER_ROLE] = user.userRole;
    userMap[AUTH_TOKEN] = user.authToken;
    userMap[FIREBASE_TOKEN] = user.fireBaseToken;
    userMap[OTHER_INFO] = user.otherInfo.toString();
    userMap[LOGOUT] = user.isLogout;

    int result = await dbClient.insert(TABLE_USER, userMap);

    print("result $result");

    return result;
  }

  /**
   * All Job CRUD(Create, Read, Update, Delete) Operations
   */
  // Adding new job in db
  Future<int> addJob(Jobs job) async{
    var dbClient = await db;
    Map<String, dynamic> jobMap = Map();
    jobMap[ID] = job.jobId;
    jobMap[JOB_ID]  = job.jobId;
    jobMap[JOB_NAME] = job.jobName;
    jobMap[JOB_TYPE_ID] = job.jobTypeId;
    jobMap[JOB_TYPE] = job.jobType;
    jobMap[JOB_DRIVER_ID] = job.driverId;
    jobMap[JOB_DRIVER_NAME] = job.driverName;
    jobMap[JOB_CUSTOMER_ID] = job.customerId;
    jobMap[JOB_CUSTOMER_NAME] = job.customerName;
    jobMap[JOB_STATUS] = job.jobStatus;
    jobMap[JOB_STATUS_SHOW] = job.statusShow;
    jobMap[JOB_START_DATE] = job.startDate;
    jobMap[JOB_START_TIME] = job.startTime;
    jobMap[JOB_ADDRESS] = job.address;
    jobMap[JOB_STREET] = job.street;
    jobMap[JOB_STREET2] =  job.street2;
    jobMap[JOB_CITY] = job.city;
    jobMap[JOB_STATE] = job.state;
    jobMap[JOB_ZIP] = job.zip;
    jobMap[JOB_COUNTRY] = job.country;
    jobMap[JOB_LATITUDE] = job.latitude;
    jobMap[JOB_LONGITUDE] = job.longitude;

    BeforeWork beforeWork = job.jobReport.beforeWork;
    if(beforeWork != null){
      jobMap[JOB_BEFORE_WORK_START_DATETIME] = beforeWork.startDateTime;
      jobMap[JOB_BEFORE_WORK_COMMENTS] = beforeWork.comments;
    }

    AfterWork afterWork = job.jobReport.afterWork;
    if(afterWork != null){
      jobMap[JOB_AFTER_WORK_END_DATETIME] = afterWork.endDateTime;
      jobMap[JOB_AFTER_WORK_COMMENTS] = afterWork.comments;
    }

    jobMap[JOB_GEO_FENCING] = job.geoFencing;
    jobMap[JOB_POINTS] = job.points;
    jobMap[JOB_POLYGON_COLOR] = job.polygonColor;
    jobMap[JOB_CRD] = job.crd;
    jobMap[JOB_UPD] = job.upd;
    jobMap[JOB_PRIORITY] = job.priority;
    jobMap[JOB_WORK_PRIORITY] = job.workPriority;
    jobMap[JOB_DISTANCE] = job.distance;
    jobMap[JOB_TIME_DURATION] = job.timeDuration;
    jobMap[JOB_GENERATE_PDF] = job.generatePdf;
    jobMap[JOB_TYPE_QUESTIONS] = job.jobTypeQuestionsString;
    jobMap[LOCAL_IMAGE_UPDATED_STATUS] = "No";
    jobMap[LOCAL_UPDATED_STATUS] = job.updatedStatus;

    // Inserting Row
    int result = await dbClient.insert(TABLE_JOBS, jobMap);

    //print("job add result $result");
    return result;
  }

  // Updating job in db
  Future<int> updateJob(Jobs job) async{
    var dbClient = await db;
    Map<String, dynamic> jobMap = Map();

    jobMap[JOB_NAME] = job.jobName;
    jobMap[JOB_TYPE_ID] =  job.jobTypeId;
    jobMap[JOB_TYPE] =  job.jobType;
    jobMap[JOB_DRIVER_ID] =  job.driverId;
    jobMap[JOB_DRIVER_NAME] =  job.driverName;
    jobMap[JOB_CUSTOMER_ID] =  job.customerId;
    jobMap[JOB_CUSTOMER_NAME] = job.customerName;
    jobMap[JOB_STATUS] =  job.jobStatus;
    jobMap[JOB_STATUS_SHOW] =  job.statusShow;
    jobMap[JOB_START_DATE] =  job.startDate;
    jobMap[JOB_START_TIME] =  job.startTime;
    jobMap[JOB_ADDRESS] =  job.address;
    jobMap[JOB_STREET] =  job.street;
    jobMap[JOB_STREET2] =  job.street2;
    jobMap[JOB_CITY] =  job.city;
    jobMap[JOB_STATE] =  job.state;
    jobMap[JOB_ZIP] =  job.zip;
    jobMap[JOB_COUNTRY] =  job.country;
    jobMap[JOB_LATITUDE] =  job.latitude;
    jobMap[JOB_LONGITUDE] =  job.longitude;

    BeforeWork beforeWork = job.jobReport.beforeWork;
    if(beforeWork != null){
      jobMap[JOB_BEFORE_WORK_START_DATETIME] =  beforeWork.startDateTime;
      jobMap[JOB_BEFORE_WORK_COMMENTS] =  beforeWork.comments;
    }

    AfterWork afterWork = job.jobReport.afterWork;
    if(afterWork != null){
      jobMap[JOB_AFTER_WORK_END_DATETIME] =  afterWork.endDateTime;
      jobMap[JOB_AFTER_WORK_COMMENTS] =  afterWork.comments;
    }

    jobMap[JOB_GEO_FENCING] =  job.geoFencing;
    jobMap[JOB_POINTS] =  job.points;
    jobMap[JOB_POLYGON_COLOR] =  job.polygonColor;
    jobMap[JOB_UPD] =  job.upd;
    jobMap[JOB_PRIORITY] = job.priority;
    jobMap[JOB_WORK_PRIORITY] = job.workPriority;
    jobMap[JOB_DISTANCE] = job.distance;
    jobMap[JOB_TIME_DURATION] =  job.timeDuration;
    /*jobMap[JOB_GEO_FENCING_URL, job.getGeoFencingUrl());*/
    jobMap[JOB_GENERATE_PDF] =  job.generatePdf;
    jobMap[LOCAL_UPDATED_STATUS] =  job.updatedStatus;

    // Updating Row
    int result = await dbClient.update(TABLE_JOBS, jobMap);

    print("job update result $result");

    return result;

    //Log.d(TAG, "updated row : " + row + "  jobName " + job.getJobName());
  }

  // Updating job activity in db when job start or end
  Future<int> updateJobActivity(Jobs job) async{
    //Log.d(TAG, "job : "+ job.toString());

    var dbClient = await db;
    Map<String, dynamic> jobMap = Map();

    jobMap[JOB_STATUS] = job.jobStatus;
    jobMap[JOB_STATUS_SHOW] =  job.statusShow;

    BeforeWork beforeWork = job.jobReport.beforeWork;
    if(beforeWork != null){
      if(beforeWork.startDateTime != null)
        jobMap[JOB_BEFORE_WORK_START_DATETIME] =  beforeWork.startDateTime;

      if(beforeWork.comments != null)
        jobMap[JOB_BEFORE_WORK_COMMENTS] =  beforeWork.comments;

      if(beforeWork.driverSignature != null)
        jobMap[JOB_BEFORE_WORK_DRIVER_SIGNATURE] =  beforeWork.driverSignature;

      List<String> beforeWorkImage = beforeWork.workImage;
      if(beforeWorkImage != null && beforeWorkImage.length > 0 )
        jobMap[JOB_BEFORE_WORK_IMAGE1] =  beforeWorkImage[0];
      if(beforeWorkImage != null && beforeWorkImage.length > 1 )
        jobMap[JOB_BEFORE_WORK_IMAGE2] =  beforeWorkImage[1];
      if(beforeWorkImage != null && beforeWorkImage.length > 2 )
        jobMap[JOB_BEFORE_WORK_IMAGE3] =  beforeWorkImage[2];
    }

    AfterWork afterWork = job.jobReport.afterWork;
    if(afterWork != null){
      if (afterWork.endDateTime != null)
        jobMap[JOB_AFTER_WORK_END_DATETIME] =  afterWork.endDateTime;

      if (afterWork.comments != null)
        jobMap[JOB_AFTER_WORK_COMMENTS] =  afterWork.comments;

      if (afterWork.customerSignature != null)
        jobMap[JOB_AFTER_WORK_CUSTOMER_SIGNATURE] =  afterWork.customerSignature;

      List<String> afterWorkImage = afterWork.workImage;
      if(afterWorkImage != null && afterWorkImage.length > 0)
        jobMap[JOB_AFTER_WORK_IMAGE1] =  afterWorkImage[0];

      if(afterWorkImage != null && afterWorkImage.length > 1)
        jobMap[JOB_AFTER_WORK_IMAGE2] =  afterWorkImage[1];

      if(afterWorkImage != null && afterWorkImage.length > 2)
        jobMap[JOB_AFTER_WORK_IMAGE3] =  afterWorkImage[2];
    }

    if (job.jobTypeQuestionsString != null && job.jobTypeQuestionsString.length > 0)
      jobMap[JOB_TYPE_QUESTIONS] = job.jobTypeQuestionsString;

    if (job.jobTypeQuestionsString != null && job.jobTypeQuestionsString.length > 0)
      jobMap[JOB_TYPE_QUESTION_ANSWERS] = job.jobTypeQuestionsString;

    jobMap[JOB_UPD] =  job.upd;
    jobMap[LOCAL_UPDATED_STATUS] = job.updatedStatus;
    jobMap[LOCAL_IMAGE_UPDATED_STATUS] = job.imageUpdatedStatus;

    // Updating Row
    int result = await dbClient.update(TABLE_JOBS, jobMap, where: "id = ?", whereArgs: [job.jobId]);
    //int result = await dbClient.update(TABLE_JOBS, jobMap, where: "id="  + job.jobId);

    // Closing database connection

    print("Update job activity result $result");

    return result;
  }

  Future<bool> isJobExist(String jobId) async{
    int count;
    String selectJob = "SELECT * FROM " + TABLE_JOBS + " where " + ID + " = '"+ jobId +"'";

    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();

    //Log.d(TAG, "selectJob "+ selectJob);
    jobMap = await dbClient.rawQuery(selectJob, null);
    // this gets called even if there is an exception somewhere above

    count = jobMap.length;

    return count > 0;
  }

  Future<bool> isUpdateRequired(String jobId, String jobStatus) async{
    int count;
    String selectJob = "SELECT * FROM " + TABLE_JOBS + " where " + ID + " = '"+ jobId +"' and " +  JOB_STATUS + " != '" + jobStatus +"'";
    //Log.d(TAG, "selectJob "+ selectJob);
    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();

    jobMap = await dbClient.rawQuery(selectJob, null);

    count = jobMap.length;
    int localJobStatus = 0;

    jobMap.forEach((firstResult) {
      localJobStatus = firstResult[JOB_STATUS];
      /*firstResult.forEach((key, value) {
        print("key: $key => value: $value");
      });*/
    });

    int serverJobStatus = int.parse(jobStatus);

    //print("serverJobStatus " + serverJobStatus.toString() + " localJobStatus " + localJobStatus.toString() + " count : "+ count.toString());
    return count > 0 && (serverJobStatus > localJobStatus);
    // this gets called even if there is an exception somewhere above
  }

  Future<bool> isUpdateRequiredFilePath(String jobId, String jobStatus, String whereCheck, String imagePath) async{
    int count;
    //Log.d(TAG, "selectJob "+ selectJob);
    String selectJob = "SELECT  * FROM " + TABLE_JOBS + " where " + ID + " = '"+ jobId +"' and " +  JOB_STATUS + " = '" + jobStatus +"' and " + whereCheck + " = '" + imagePath + "'";
    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();

    jobMap = await dbClient.rawQuery(selectJob, null);
    count = jobMap.length;

    // this gets called even if there is an exception somewhere above

    return count > 0;
  }

  Future<int> deleteJobById(String jobId) async{
    var dbClient = await db;
    //database.execSQL("delete from " + TABLE_JOBS + " where " + JOB_ID + " ='" + jobId + "'");

    String where = JOB_ID + " = ?";
    List whereArgs = [
      jobId
    ];

    return dbClient.delete(TABLE_JOBS, where: where, whereArgs: whereArgs);
  }

  /**
   * Get list of Open and In Progress Jobs from SQLite DB as List
   */
  Future<List<Jobs>> getOpenJobsListFromLocal(String userId) async{
    List<Jobs> jobList = List();
    String selectQuery = "SELECT * FROM " + TABLE_JOBS + " where (" + JOB_STATUS + " = '0' or " + JOB_STATUS + " = '1') and (" + JOB_DRIVER_ID + " = '" + userId  + "' or " + JOB_CUSTOMER_ID + " = '" + userId + "')";
    //Log.d(TAG, "selectQuery : " + selectQuery);

    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();

    jobMap = await dbClient.rawQuery(selectQuery, null);
    if(jobMap.length > 0){
      jobMap.forEach((firstResult) {
        /*firstResult.forEach((key, value) {
          print("key: $key => value : $value");
        });*/
        Jobs job = Jobs();
        job.jobId = firstResult[JOB_ID];
        job.jobName = firstResult[JOB_NAME];
        job.jobTypeId = firstResult[JOB_TYPE_ID].toString();
        job.jobType = firstResult[JOB_TYPE];
        job.driverId = firstResult[JOB_DRIVER_ID].toString();
        job.driverName = firstResult[JOB_DRIVER_NAME];
        job.customerId = firstResult[JOB_CUSTOMER_ID].toString();
        job.customerName = firstResult[JOB_CUSTOMER_NAME];
        job.jobStatus = firstResult[JOB_STATUS].toString();
        job.statusShow = firstResult[JOB_STATUS_SHOW];
        job.startDate = firstResult[JOB_START_DATE];
        job.startTime = firstResult[JOB_START_TIME];
        job.address = firstResult[JOB_ADDRESS];
        job.street = firstResult[JOB_STREET];
        job.street2 = firstResult[JOB_STREET2];
        job.city = firstResult[JOB_CITY];
        job.state = firstResult[JOB_STATE];
        job.zip = firstResult[JOB_ZIP];
        job.country = firstResult[JOB_COUNTRY];
        job.latitude = firstResult[JOB_LATITUDE];
        job.longitude = firstResult[JOB_LONGITUDE];

        JobReport jobReport = JobReport();

        BeforeWork beforeWork = BeforeWork();

        beforeWork.startDateTime = firstResult[JOB_BEFORE_WORK_START_DATETIME];
        beforeWork.comments = firstResult[JOB_BEFORE_WORK_COMMENTS];
        beforeWork.driverSignature = firstResult[JOB_BEFORE_WORK_DRIVER_SIGNATURE];

        List<String> workImageList = List();
        if (firstResult[JOB_BEFORE_WORK_IMAGE1] != null)
          workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE1]);

        if (firstResult[JOB_BEFORE_WORK_IMAGE2] != null)
          workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE2]);

        if (firstResult[JOB_BEFORE_WORK_IMAGE3] != null)
          workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE3]);

        beforeWork.workImage = (workImageList != null) ? workImageList : "[]";
        jobReport.beforeWork = beforeWork;

        AfterWork afterWork = new AfterWork();
        afterWork.endDateTime = firstResult[JOB_AFTER_WORK_END_DATETIME];
        afterWork.comments = firstResult[JOB_AFTER_WORK_COMMENTS];
        afterWork.customerSignature = firstResult[JOB_AFTER_WORK_CUSTOMER_SIGNATURE];

        workImageList = List();
        if (firstResult[JOB_AFTER_WORK_IMAGE1] != null)
          workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE1]);

        if (firstResult[JOB_AFTER_WORK_IMAGE2] != null)
          workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE2]);

        if (firstResult[JOB_AFTER_WORK_IMAGE3] != null)
          workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE3]);

        afterWork.workImage =  (workImageList != null) ? workImageList : "[]";
        jobReport.afterWork = afterWork;

        job.jobReport = jobReport;
        job.geoFencing = firstResult[JOB_GEO_FENCING];
        job.points = firstResult[JOB_POINTS];
        job.polygonColor = firstResult[JOB_POLYGON_COLOR];
        job.crd = firstResult[JOB_CRD];
        job.upd = firstResult[JOB_UPD];
        job.priority = firstResult[JOB_PRIORITY];
        job.workPriority = firstResult[JOB_WORK_PRIORITY];
        job.distance = firstResult[JOB_DISTANCE];
        job.timeDuration = firstResult[JOB_TIME_DURATION];
        job.geoFencingUrl = firstResult[JOB_GEO_FENCING_URL];
        job.generatePdf = firstResult[JOB_GENERATE_PDF];
        job.updatedStatus = firstResult[LOCAL_UPDATED_STATUS];
        job.isLocalDb = true;

        jobList.add(job);
      });

      return jobList;
    }
  }

  /**
   * Get list of Complete Jobs from SQLite DB as List
   */
  Future<List<Jobs>> getCompleteJobsListFromLocal(String userId) async{
    List<Jobs> jobList;
    jobList = List();
    String selectQuery = "SELECT  * FROM " + TABLE_JOBS + " where " + JOB_STATUS + " = '2' and (" + JOB_DRIVER_ID + " = '" + userId  + "' or " + JOB_CUSTOMER_ID + " = '" + userId + "') order by "+ JOB_UPD + " desc";
    //Log.d(TAG, "selectQuery : " + selectQuery);
    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();

    jobMap = await dbClient.rawQuery(selectQuery, null);
    if(jobMap.length > 0){
      jobMap.forEach((firstResult) {
        Jobs job = Jobs();

        job.jobId = firstResult[JOB_ID];
        job.jobName = firstResult[JOB_NAME];
        job.jobTypeId = firstResult[JOB_TYPE_ID].toString();
        job.jobType = firstResult[JOB_TYPE];
        job.driverId = firstResult[JOB_DRIVER_ID].toString();
        job.driverName = firstResult[JOB_DRIVER_NAME];
        job.customerId = firstResult[JOB_CUSTOMER_ID].toString();
        job.customerName = firstResult[JOB_CUSTOMER_NAME];
        job.jobStatus = firstResult[JOB_STATUS].toString();
        job.statusShow = firstResult[JOB_STATUS_SHOW];
        job.startDate = firstResult[JOB_START_DATE];
        job.startTime = firstResult[JOB_START_TIME];
        job.address = firstResult[JOB_ADDRESS];
        job.street = firstResult[JOB_STREET];
        job.street2 = firstResult[JOB_STREET2];
        job.city = firstResult[JOB_CITY];
        job.state = firstResult[JOB_STATE];
        job.zip = firstResult[JOB_ZIP];
        job.country = firstResult[JOB_COUNTRY];
        job.latitude = firstResult[JOB_LATITUDE];
        job.longitude = firstResult[JOB_LONGITUDE];

        JobReport jobReport = JobReport();

        BeforeWork beforeWork = BeforeWork();

        beforeWork.startDateTime = firstResult[JOB_BEFORE_WORK_START_DATETIME];
        beforeWork.comments = firstResult[JOB_BEFORE_WORK_COMMENTS];
        beforeWork.driverSignature = firstResult[JOB_BEFORE_WORK_DRIVER_SIGNATURE];

        List<String> workImageList = List();
        if (firstResult[JOB_BEFORE_WORK_IMAGE1] != null)
          workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE1]);

        if (firstResult[JOB_BEFORE_WORK_IMAGE2] != null)
          workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE2]);

        if (firstResult[JOB_BEFORE_WORK_IMAGE3] != null)
          workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE3]);

        beforeWork.workImage = workImageList;
        jobReport.beforeWork = beforeWork;

        AfterWork afterWork = new AfterWork();
        afterWork.endDateTime = firstResult[JOB_AFTER_WORK_END_DATETIME];
        afterWork.comments = firstResult[JOB_AFTER_WORK_COMMENTS];
        afterWork.customerSignature = firstResult[JOB_AFTER_WORK_CUSTOMER_SIGNATURE];

        workImageList = List();
        if (firstResult[JOB_AFTER_WORK_IMAGE1] != null)
          workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE1]);

        if (firstResult[JOB_AFTER_WORK_IMAGE2] != null)
          workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE2]);

        if (firstResult[JOB_AFTER_WORK_IMAGE3] != null)
          workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE3]);

        afterWork.workImage = workImageList;
        jobReport.afterWork = afterWork;

        job.jobReport = jobReport;

        job.geoFencing = firstResult[JOB_GEO_FENCING];
        job.points = firstResult[JOB_POINTS];
        job.polygonColor = firstResult[JOB_POLYGON_COLOR];
        job.crd = firstResult[JOB_CRD];
        job.upd = firstResult[JOB_UPD];
        job.priority = firstResult[JOB_PRIORITY];
        job.workPriority = firstResult[JOB_WORK_PRIORITY];
        job.distance = firstResult[JOB_DISTANCE];
        job.timeDuration = firstResult[JOB_TIME_DURATION];
        job.geoFencingUrl = firstResult[JOB_GEO_FENCING_URL];
        job.generatePdf = firstResult[JOB_GENERATE_PDF];
        job.updatedStatus = firstResult[LOCAL_UPDATED_STATUS];
        job.isLocalDb = true;

        jobList.add(job);
      });

      return jobList;
    }
  }

  /**
   * Get Job Detail By jobId from SQLite DB as List
   */
  Future<Jobs> getJobDetailByJobIdFromLocal(String jobId) async{
    String selectQuery = "SELECT * FROM " + TABLE_JOBS + " where " + JOB_ID + " = " + jobId ;
    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();
    jobMap = await dbClient.rawQuery(selectQuery, null);

    print("Job Detail count : ${jobMap.length}");

    if(jobMap.length > 0){
      Map<String, dynamic> firstResult = jobMap.first;

      Jobs job = Jobs();

      job.jobId = firstResult[JOB_ID];
      job.jobName = firstResult[JOB_NAME];
      job.jobTypeId = firstResult[JOB_TYPE_ID].toString();
      job.jobType = firstResult[JOB_TYPE];
      job.driverId = firstResult[JOB_DRIVER_ID].toString();
      job.driverName = firstResult[JOB_DRIVER_NAME];
      job.customerId = firstResult[JOB_CUSTOMER_ID].toString();
      job.customerName = firstResult[JOB_CUSTOMER_NAME];
      job.jobStatus = firstResult[JOB_STATUS].toString();
      job.statusShow = firstResult[JOB_STATUS_SHOW];
      job.startDate = firstResult[JOB_START_DATE];
      job.startTime = firstResult[JOB_START_TIME];
      job.address = firstResult[JOB_ADDRESS];
      job.street = firstResult[JOB_STREET];
      job.street2 = firstResult[JOB_STREET2];
      job.city = firstResult[JOB_CITY];
      job.state = firstResult[JOB_STATE];
      job.zip = firstResult[JOB_ZIP];
      job.country = firstResult[JOB_COUNTRY];
      job.latitude = firstResult[JOB_LATITUDE];
      job.longitude = firstResult[JOB_LONGITUDE];

      JobReport jobReport = JobReport();

      BeforeWork beforeWork = BeforeWork();

      beforeWork.startDateTime = firstResult[JOB_BEFORE_WORK_START_DATETIME];
      beforeWork.comments = firstResult[JOB_BEFORE_WORK_COMMENTS];
      beforeWork.driverSignature = firstResult[JOB_BEFORE_WORK_DRIVER_SIGNATURE];

      List<String> workImageList = List();
      if (firstResult[JOB_BEFORE_WORK_IMAGE1] != null)
        workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE1]);

      if (firstResult[JOB_BEFORE_WORK_IMAGE2] != null)
        workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE2]);

      if (firstResult[JOB_BEFORE_WORK_IMAGE3] != null)
        workImageList.add(firstResult[JOB_BEFORE_WORK_IMAGE3]);

      beforeWork.workImage = workImageList;
      jobReport.beforeWork = beforeWork;

      AfterWork afterWork = new AfterWork();
      afterWork.endDateTime = firstResult[JOB_AFTER_WORK_END_DATETIME];
      afterWork.comments = firstResult[JOB_AFTER_WORK_COMMENTS];
      afterWork.customerSignature = firstResult[JOB_AFTER_WORK_CUSTOMER_SIGNATURE];

      workImageList = List();
      if (firstResult[JOB_AFTER_WORK_IMAGE1] != null)
        workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE1]);

      if (firstResult[JOB_AFTER_WORK_IMAGE2] != null)
        workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE2]);

      if (firstResult[JOB_AFTER_WORK_IMAGE3] != null)
        workImageList.add(firstResult[JOB_AFTER_WORK_IMAGE3]);

      afterWork.workImage = workImageList;
      jobReport.afterWork = afterWork;

      job.jobReport = jobReport;

      job.geoFencing = firstResult[JOB_GEO_FENCING];
      job.points = firstResult[JOB_POINTS];
      job.polygonColor = firstResult[JOB_POLYGON_COLOR];
      job.crd = firstResult[JOB_CRD];
      job.upd = firstResult[JOB_UPD];

      job.jobTypeQuestions = List();
      if(job.jobStatus == "1"){
        if(firstResult[JOB_TYPE_QUESTIONS] != null){
          jsonDecode(firstResult[JOB_TYPE_QUESTIONS])?.forEach((v) {
            job.jobTypeQuestions.add(JobTypeQuestions.fromJson(v));
          });
        }
      } else if(job.jobStatus == "2"){
        if(firstResult[JOB_TYPE_QUESTION_ANSWERS] != null){
          jsonDecode(firstResult[JOB_TYPE_QUESTION_ANSWERS])?.forEach((v) {
            job.jobTypeQuestions.add(JobTypeQuestions.fromJson(v));
          });
        }
      }

      job.priority = firstResult[JOB_PRIORITY];
      job.workPriority = firstResult[JOB_WORK_PRIORITY];
      job.distance = firstResult[JOB_DISTANCE];
      job.timeDuration = firstResult[JOB_TIME_DURATION];
      job.geoFencingUrl = firstResult[JOB_GEO_FENCING_URL];
      job.generatePdf = firstResult[JOB_GENERATE_PDF];
      job.updatedStatus = firstResult[LOCAL_UPDATED_STATUS];
      job.isLocalDb = true;

      return job;
    }
  }

  /**
   * Get list of local job for uploading on server from SQLite DB as List
   */
  Future<List<Jobs>> getJobListForUploadOnServerFromLocal() async{
    List<Jobs> jobList;
    jobList = List();
    String selectQuery = "SELECT  * FROM " + TABLE_JOBS + " where " + LOCAL_UPDATED_STATUS + " = 'No'" ;

    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();
    jobMap = await dbClient.rawQuery(selectQuery, null);

    if(jobMap.length > 0){
      jobMap.forEach((mapElement) {
        Jobs job =Jobs();

        job.jobId = mapElement[JOB_ID];
        job.jobStatus = mapElement[JOB_STATUS].toString();

        JobReport jobReport = JobReport();

        BeforeWork beforeWork = BeforeWork();
        beforeWork.startDateTime = mapElement[JOB_BEFORE_WORK_START_DATETIME];
        beforeWork.comments = mapElement[JOB_BEFORE_WORK_COMMENTS];
        beforeWork.driverSignature = mapElement[JOB_BEFORE_WORK_DRIVER_SIGNATURE];

        List<String> workImageList = List();
        if (mapElement[JOB_BEFORE_WORK_IMAGE1] != null)
          workImageList.add(mapElement[JOB_BEFORE_WORK_IMAGE1]);

        if (mapElement[JOB_BEFORE_WORK_IMAGE2] != null)
          workImageList.add(mapElement[JOB_BEFORE_WORK_IMAGE2]);

        if (mapElement[JOB_BEFORE_WORK_IMAGE3] != null)
          workImageList.add(mapElement[JOB_BEFORE_WORK_IMAGE3]);

        beforeWork.workImage = workImageList;
        jobReport.beforeWork = beforeWork;

        AfterWork afterWork = new AfterWork();
        afterWork.endDateTime = mapElement[JOB_AFTER_WORK_END_DATETIME];
        afterWork.comments = mapElement[JOB_AFTER_WORK_COMMENTS];
        afterWork.customerSignature = mapElement[JOB_AFTER_WORK_CUSTOMER_SIGNATURE];

        workImageList = List();
        if (mapElement[JOB_AFTER_WORK_IMAGE1] != null)
          workImageList.add(mapElement[JOB_AFTER_WORK_IMAGE1]);

        if (mapElement[JOB_AFTER_WORK_IMAGE2] != null)
          workImageList.add(mapElement[JOB_AFTER_WORK_IMAGE2]);

        if (mapElement[JOB_AFTER_WORK_IMAGE3] != null)
          workImageList.add(mapElement[JOB_AFTER_WORK_IMAGE3]);

        afterWork.workImage = workImageList;
        jobReport.afterWork = afterWork;

        job.jobReport = jobReport;
        job.jobTypeQuestions = List();
        if(mapElement[JOB_TYPE_QUESTIONS] != null){
          jsonDecode(mapElement[JOB_TYPE_QUESTIONS])?.forEach((v) {
            job.jobTypeQuestions.add(JobTypeQuestions.fromJson(v));
          });
        }
        job.jobTypeQuestionAnswers = mapElement[JOB_TYPE_QUESTION_ANSWERS];
        job.updatedStatus = mapElement[LOCAL_UPDATED_STATUS];
        job.isLocalDb = true;

        jobList.add(job);
      });
    }


    return jobList;
  }

  /**
   * Get SQLite records that are yet to be Synced
   */
  Future<int> dbSyncCount() async{
    int count;
    String selectQuery = "SELECT  * FROM " + TABLE_JOBS + " where " + LOCAL_UPDATED_STATUS + " = '"+"No"+"'";
    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();
    jobMap = await dbClient.rawQuery(selectQuery, null);

    count = jobMap.length;

    return count;
  }

  /**
   * Update images path each Job Id
   * @param jobId
   * @param whereImage
   * @param imagePath
   */
  Future<void> updateImagePath(String jobId, String whereImage, String imagePath) async{
    var dbClient = await db;
    String updateQuery = "Update " + TABLE_JOBS + " set " + whereImage + " = '" + imagePath + "', " + LOCAL_IMAGE_UPDATED_STATUS + " = 'Yes' where " + ID + " ="+"'"+ jobId +"'";
    //Log.d(TAG, "Image updated " + whereImage + " = "  + updateQuery);
    dbClient.execute(updateQuery);

  }

  Future<bool> isLocalImageUpdated(String jobId) async{
    int count;
    String selectJob = "SELECT * FROM " + TABLE_JOBS + " where " + ID + " = '"+ jobId +"' and " + LOCAL_IMAGE_UPDATED_STATUS + " = 'No'";
    //Log.d(TAG, "selectJob "+ selectJob);
    var dbClient = await db;
    List<Map<String, dynamic>> jobMap = List();
    jobMap = await dbClient.rawQuery(selectJob, null);

    count = jobMap.length;

    // this gets called even if there is an exception somewhere above

    return count > 0;
  }

  Future<int> clearAllData() async{
    var dbClient = await db;
    int deleteUser = await dbClient?.rawDelete("DELETE FROM $TABLE_USER");
    int deleteJob = await dbClient?.rawDelete("DELETE FROM $TABLE_JOBS");

    print("deleteUser : $deleteUser deleteJob $deleteJob");
    //await dbClient.close();
    return deleteUser + deleteJob;
  }

}