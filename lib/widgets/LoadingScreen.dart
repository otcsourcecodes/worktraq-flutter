import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:worktraq/utils/SizeConfig.dart';

class LoadingScreen extends StatefulWidget {
  final GlobalKey globalKey;
  final String title;

  const LoadingScreen({Key key, this.globalKey, this.title}) : super(key: key);

  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.black.withOpacity(0.70),
        key: widget.globalKey,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  strokeWidth: 5,
                ),
                SizedBox(height: 20),
                Text(
                  ( widget.title != null ? widget.title : "Loading..."),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 2.2 * SizeConfig.textMultiplier,
                    fontWeight: FontWeight.bold
                  ),
                )
              ]
          ),
        ),
      ),
    );
  }
}
