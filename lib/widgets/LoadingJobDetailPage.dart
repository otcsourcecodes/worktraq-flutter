import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:worktraq/utils/AppTheme.dart';
import 'package:worktraq/utils/SizeConfig.dart';

class LoadingJobDetailPage extends StatefulWidget {
  @override
  _LoadingJobDetailPageState createState() => _LoadingJobDetailPageState();
}

class _LoadingJobDetailPageState extends State<LoadingJobDetailPage> {
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Shimmer.fromColors(
        baseColor: Colors.grey[400],
        highlightColor: Colors.grey[100],
        enabled: _enabled,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0, top: 5),
              child: Text(
                "JOB DETAILS",
                style: TextStyle(
                    color: Color(0xFF393939),
                    fontSize: 2.0 * SizeConfig.textMultiplier
                ),
              ),
            ),
            SizedBox(height: 10.0),
            Container(
              height: 95,
              color: Colors.white60,
              margin: EdgeInsets.symmetric(horizontal: 5),
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled: _enabled,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 2 * SizeConfig.heightMultiplier,
                            width: 40 * SizeConfig.widthMultiplier,
                            color: Colors.white,
                          ),
                          SizedBox(height: 10.0),
                          Container(
                            height: 1.5 * SizeConfig.heightMultiplier,
                            width: 30 * SizeConfig.widthMultiplier,
                            color: Colors.white,
                          ),
                          SizedBox(height: 10.0),
                          Container(
                            height: 1.5 * SizeConfig.heightMultiplier,
                            width: 30 * SizeConfig.widthMultiplier,
                            color: Colors.white,
                          ),
                          SizedBox(height: 10.0),
                          Container(
                            height: 1.5 * SizeConfig.heightMultiplier,
                            width: 28 * SizeConfig.widthMultiplier,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 12.0,
                              width: 12.0,
                              color: Colors.white,
                            ),
                            SizedBox(width: 5.0),
                            Container(
                              height: 1.2 * SizeConfig.textMultiplier,
                              width: 25 * SizeConfig.widthMultiplier,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        SizedBox(height: 7.5 * SizeConfig.heightMultiplier),
                        Container(
                          width: 60.0,
                          height: 10.0,
                          color: Colors.white,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 15.0),
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                "ADDRESS",
                style: TextStyle(
                    color: Color(0xFF393939),
                    fontSize: 2.0 * SizeConfig.textMultiplier
                ),
              ),
            ),
            SizedBox(height: 10.0),
            _addressCardContainer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: 15.0),
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Text(
                    "QUESTIONS",
                    style: TextStyle(
                        color: Color(0xFF393939),
                        fontSize: 2.0 * SizeConfig.textMultiplier
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                _noQuestionCardContainer()
              ],
            ),
            SizedBox(height: 15.0),
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                "BEFORE WORK",
                style: TextStyle(
                    color: Color(0xFF393939),
                    fontSize: 2.0 * SizeConfig.textMultiplier
                ),
              ),
            ),
            SizedBox(height: 10.0),
            _beforeCardContainer()
          ],
        ),
      ),
    );
  }

  Widget _addressCardContainer(){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white60,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0, top:5.0, bottom: 15.0),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          enabled: _enabled,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Container(
                  height: 8,
                  width: double.infinity,
                  color: Colors.white,
                ),
              ),
              Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
                          child: Container(
                            width: 35 * SizeConfig.widthMultiplier,
                            height: 8,
                            color: Colors.white,
                          ),
                        ),
                        Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                        Padding(
                          padding: const EdgeInsets.only(top: 22.0, bottom: 10.0),
                          child: Container(
                            width: 35 * SizeConfig.widthMultiplier,
                            height: 8,
                            color: Colors.white,
                          ),
                        ),
                        Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                      ],
                    ),
                  ),
                  SizedBox(width: 20.0),
                  Container(
                    width: 36 * SizeConfig.widthMultiplier,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 22.0, bottom: 10.0),
                          child: Container(
                            width: 30 * SizeConfig.widthMultiplier,
                            height: 8,
                            color: Colors.white,
                          ),
                        ),
                        Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 22.0, bottom: 10.0),
                              child: Container(
                                width: 25 * SizeConfig.widthMultiplier,
                                height: 8,
                                color: Colors.white,
                              ),
                            ),
                            Divider(thickness: 1, height: 0, color: AppTheme.colorDivider),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: 5 * SizeConfig.heightMultiplier,
                  width: 38 * SizeConfig.widthMultiplier,
                  child: GestureDetector(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: AppTheme.primaryColor.withOpacity(0.2), //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                          style: BorderStyle.solid,
                          width: 1.0,
                        ),
                        color: AppTheme.primaryColor, //isLoginButtonEnabled(state) ? Color(0xFF2D6087) : Color(0xFF697680),
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Center(
                            child: Text(
                              "Map View",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 1.8 * SizeConfig.textMultiplier,
                                fontWeight: FontWeight.normal,
                                letterSpacing: 1,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _noQuestionCardContainer(){
    return Card(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
        color: Colors.white60,
        elevation: 5,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.transparent, width: 0),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
          child: Center(
            child: Text(
              "No Questions",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontSize: 2.5 * SizeConfig.textMultiplier
              ),
            ),
          ),
        )
    );
  }

  Widget _beforeCardContainer(){
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      color: Colors.white60,
      elevation: 5,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      child: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top:10.0, bottom: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                "Comment and Driver Signature",
                style: TextStyle(
                    fontSize: 1.8 * SizeConfig.textMultiplier,
                    color: Color(0xFF244764),
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
                Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
                Container(
                  width: 28 * SizeConfig.widthMultiplier,
                  height: 25 * SizeConfig.widthMultiplier,
                  child: Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1, color: Color(0xFFB3B8B7)),
                    ),
                    elevation: 2,
                    //margin: EdgeInsets.all(5),
                  ),
                ),
              ],
            ),
            SizedBox(height: 15.0),
            Row(
              children: [
                Expanded(
                  child: Container(
                    width: 38 * SizeConfig.widthMultiplier,
                    height: 75.0,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFFB3B8B7)),
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(4.0))
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  width: 38 * SizeConfig.widthMultiplier,
                  height: 75.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xFFB3B8B7)),
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    color: Colors.white
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      height: 60,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ) ;
  }
}
