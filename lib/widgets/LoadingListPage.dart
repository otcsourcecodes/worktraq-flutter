import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:worktraq/utils/SizeConfig.dart';

class LoadingListPage extends StatefulWidget {
  @override
  _LoadingListPageState createState() => _LoadingListPageState();
}

class _LoadingListPageState extends State<LoadingListPage> {
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled: _enabled,
                child: ListView.builder(
                  itemBuilder: (_, __) =>
                  Container(
                    height: 17 * SizeConfig.heightMultiplier,
                    padding: EdgeInsets.symmetric(horizontal: 2.43 * SizeConfig.widthMultiplier, vertical: 0.9 * SizeConfig.heightMultiplier),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 16 * SizeConfig.heightMultiplier,
                          width: 3,
                          color: Colors.white,
                        ),
                        SizedBox(width: 2.2 * SizeConfig.widthMultiplier),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 3),
                              Container(
                                height: 2 * SizeConfig.heightMultiplier,
                                width: 40 * SizeConfig.widthMultiplier,
                                color: Colors.white,
                              ),
                              SizedBox(height: 0.9 * SizeConfig.heightMultiplier),
                              Container(
                                height: 1.5 * SizeConfig.heightMultiplier,
                                width: 30 * SizeConfig.widthMultiplier,
                                color: Colors.white,
                              ),
                              SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                            Container(
                              height: 1.5 * SizeConfig.heightMultiplier,
                              width: 28 * SizeConfig.widthMultiplier,
                              color: Colors.white,
                            ),
                              SizedBox(height: 0.65 * SizeConfig.heightMultiplier),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 4 * SizeConfig.imageSizeMultiplier,
                                    width: 4 * SizeConfig.imageSizeMultiplier,
                                    color: Colors.white,
                                  ),
                                  SizedBox(width: 8.0),
                                  Container(
                                    height: 1.5 * SizeConfig.heightMultiplier,
                                    width: 20 * SizeConfig.widthMultiplier,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: 0.4 * SizeConfig.heightMultiplier),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  height: 2.4 * SizeConfig.imageSizeMultiplier,
                                  width: 2.4,
                                  color: Colors.white,
                                ),
                                SizedBox(width: 5.0),
                                Container(
                                  height: 1.2 * SizeConfig.textMultiplier,
                                  width: 22 * SizeConfig.widthMultiplier,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            SizedBox(height: 0.9 * SizeConfig.heightMultiplier),
                            Row(
                              children: [
                                Container(
                                  height: 1.8 * SizeConfig.heightMultiplier,
                                  width: 12 * SizeConfig.widthMultiplier,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 0.8 * SizeConfig.widthMultiplier,
                                ),
                                Container(
                                  height: 1.8 * SizeConfig.heightMultiplier,
                                  width: 15 * SizeConfig.widthMultiplier,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            SizedBox(height: 3.5 * SizeConfig.heightMultiplier),
                            Container(
                              width: 60.0,
                              height: 1.9 * SizeConfig.heightMultiplier,
                              color: Colors.white,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  itemCount: 10,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}