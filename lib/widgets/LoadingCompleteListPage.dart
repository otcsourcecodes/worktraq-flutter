
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:worktraq/utils/SizeConfig.dart';

class LoadingCompleteListPage extends StatefulWidget {
  @override
  _LoadingCompleteListPageState createState() => _LoadingCompleteListPageState();
}

class _LoadingCompleteListPageState extends State<LoadingCompleteListPage> {
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled: _enabled,
                child: ListView.builder(
                  itemBuilder: (_, __) =>
                      Container(
                        height: 85,
                        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 85,
                              width: 3,
                              color: Colors.white,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(height: 5),
                                  Container(
                                    height: 2 * SizeConfig.heightMultiplier,
                                    width: 40 * SizeConfig.widthMultiplier,
                                    color: Colors.white,
                                  ),
                                  SizedBox(height: 12.0),
                                  Container(
                                    height: 1.5 * SizeConfig.heightMultiplier,
                                    width: 30 * SizeConfig.widthMultiplier,
                                    color: Colors.white,
                                  ),
                                  SizedBox(height: 10.0),
                                  Container(
                                    height: 1.5 * SizeConfig.heightMultiplier,
                                    width: 28 * SizeConfig.widthMultiplier,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 12.0,
                                      width: 12.0,
                                      color: Colors.white,
                                    ),
                                    SizedBox(width: 5.0),
                                    Container(
                                      height: 1.2 * SizeConfig.textMultiplier,
                                      width: 22 * SizeConfig.widthMultiplier,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                                SizedBox(height: 3.0),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 12.0,
                                      width: 12.0,
                                      color: Colors.white,
                                    ),
                                    SizedBox(width: 5.0),
                                    Container(
                                      height: 1.2 * SizeConfig.textMultiplier,
                                      width: 22 * SizeConfig.widthMultiplier,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15.0),
                                Container(
                                  width: 60.0,
                                  height: 10.0,
                                  color: Colors.white,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                  itemCount: 7,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
