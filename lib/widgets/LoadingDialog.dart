import 'package:worktraq/utils/SizeConfig.dart';
import 'package:flutter/material.dart';
class LoadingDialog {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key, String loadingText) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
            onWillPop: () async => false,
            child: SimpleDialog(
              key: key,
              backgroundColor: Colors.black54,
              children: <Widget>[
                Center(
                  child: Column(
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text(
                        loadingText,
                        style: TextStyle(color: Colors.white),
                      )
                    ]
                  ),
                )
              ])
          );
        });
  }
}