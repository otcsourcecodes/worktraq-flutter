import 'package:worktraq/models/Users.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppSharedPreferences {
  static SharedPreferences _sharedPreferences;

  static final String PREF_NAME = "worktraqUser";
  static final String IS_USER_LOGGED_IN = "IsLoggedIn";
  static final String EMAIL = "email";
  static final String AUTH_TOKEN = "authToken";
  static final String ID = "id";
  static final String FULLNAME = "fullName";
  static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
  static final String USER_ROLE = "userRole";
  static final String USER_TYPE = "userType";
  static final String PROFILE_IMAGE = "profileImage";
  static final String IN_PROGRESS = "inProgress";
  static final String FIRST_TIME_DATA_SAVE_IN_LOCAL = "firstTimeDataSaveInLocal";
  static final String FIRST_TIME_DATA_SAVE_IN_LOCAL_COMPLETE = "firstTimeDataSaveInLocalComplete";

  Future<SharedPreferences> get prefs async{
    if (_sharedPreferences != null) return _sharedPreferences;
    _sharedPreferences = await SharedPreferences.getInstance();
    return _sharedPreferences;
  }

  ///////////////////////////////////////////////////////////////////////////////
  static Future<void> clearUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  ///////////////////////////////////////////////////////////////////////////////
  static Future<bool> isUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey(IS_USER_LOGGED_IN))
      return false;
    else
      return prefs.getBool(IS_USER_LOGGED_IN);
  }

  static Future<void> setUserLoggedIn(bool isLoggedIn) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(IS_USER_LOGGED_IN, isLoggedIn);
  }

///////////////////////////////////////////////////////////////////////////////

  static Future<void> createLoginSession(Users user) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(IS_USER_LOGGED_IN, true);
    prefs.setString(ID, user.id);
    prefs.setString(AUTH_TOKEN, user.authToken);
    prefs.setString(FULLNAME, user.fullName);
    prefs.setString(EMAIL, user.email);
    prefs.setString(USER_TYPE, user.userType);
    prefs.setString(USER_ROLE, user.userRole);
    prefs.setString(PROFILE_IMAGE, user.profileImage);
  }

  static Future<String> getUserType() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey(USER_TYPE))
      return null;
    else
      return prefs.getString(USER_TYPE);
  }

  static Future<String> getFullName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(FULLNAME);
  }

  static Future<String> getEmail() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(EMAIL);
  }

  static Future<String> getUserRole() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(USER_ROLE);
  }

  static Future<String> getProfileImage() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PROFILE_IMAGE);
  }

  static Future<String> getId() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(ID);
  }

  static Future<String> getAuthToken() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey(AUTH_TOKEN))
      return null;
    else
      return prefs.getString(AUTH_TOKEN);
  }

  static Future<bool> isFirstTimeLaunch() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(IS_FIRST_TIME_LAUNCH);
  }

  static Future<void> setFirstTimeLaunch(bool isFirstTime) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(IS_FIRST_TIME_LAUNCH, isFirstTime);
  }

  static Future<void> setIsInProgress(bool isInProgress) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(IN_PROGRESS, isInProgress);
  }

  static Future<bool> isInProgress() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(IN_PROGRESS);
  }

  static Future<bool> isFirstTimeDataSaveInLocal() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(FIRST_TIME_DATA_SAVE_IN_LOCAL);
  }

  static Future<void> setFirstTimeDataSaveInLocal(bool isFirstTimeDataSaveInLocal) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(FIRST_TIME_DATA_SAVE_IN_LOCAL, isFirstTimeDataSaveInLocal);
  }

  static Future<bool> isFirstTimeDataSaveInLocalComplete() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(FIRST_TIME_DATA_SAVE_IN_LOCAL_COMPLETE);
  }

  static Future<void> setFirstTimeDataSaveInLocalComplete(bool isFirstTimeDataSaveInLocalComplete) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(FIRST_TIME_DATA_SAVE_IN_LOCAL_COMPLETE, isFirstTimeDataSaveInLocalComplete);
  }
}